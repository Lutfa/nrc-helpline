package com.raylabs.nrchelpline;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.raylabs.nrchelpline.Ui.LockableViewPager;
import com.raylabs.nrchelpline.adapter.MyPageAdapter;
import com.raylabs.nrchelpline.fragment.Fragment_Auth1;
import com.raylabs.nrchelpline.fragment.Fragment_Auth2;
import com.raylabs.nrchelpline.fragment.Fragment_Auth4;
import com.google.firebase.FirebaseApp;

import java.util.ArrayList;
import java.util.List;

public class AppSettings_new extends MainActivity {

    public static Button bar1, bar2, bar3, bar4;
    public static LockableViewPager pager;
    MyPageAdapter pageAdapter;
    ImageView settings;
    Fragment_Auth1 fragment_auth1;
    Fragment_Auth2 fragment_auth2;
    Fragment_Auth4 fragment_auth4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_app_settings_new);
            List<Fragment> fragments = getFragments();
            pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
            pager = (LockableViewPager) findViewById(R.id.pager);
            bar1 = (Button) findViewById(R.id.bar1);
            bar2 = (Button) findViewById(R.id.bar2);
            bar3 = (Button) findViewById(R.id.bar3);
            bar4 = (Button) findViewById(R.id.bar4);
            settings = (ImageView) findViewById(R.id.settings);
            settings.setVisibility(View.GONE);
            settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //showDialogForUrlChange();
                }
            });

            FirebaseApp.initializeApp(this);

            pager.setAdapter(pageAdapter);
            pager.setSwipeable(false);

            if (pager.getCurrentItem() == 0) {
                bar1.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                bar2.getBackground().setColorFilter(getResources().getColor(R.color.lightgray), PorterDuff.Mode.SRC_ATOP);
                bar3.getBackground().setColorFilter(getResources().getColor(R.color.lightgray), PorterDuff.Mode.SRC_ATOP);
                bar4.getBackground().setColorFilter(getResources().getColor(R.color.lightgray), PorterDuff.Mode.SRC_ATOP);
            }
            if (pager.getCurrentItem() == 1) {
                bar1.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                bar2.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                bar3.getBackground().setColorFilter(getResources().getColor(R.color.lightgray), PorterDuff.Mode.SRC_ATOP);
                bar4.getBackground().setColorFilter(getResources().getColor(R.color.lightgray), PorterDuff.Mode.SRC_ATOP);
            }
            if (pager.getCurrentItem() == 2) {
                bar1.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                bar2.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                bar3.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                bar4.getBackground().setColorFilter(getResources().getColor(R.color.lightgray), PorterDuff.Mode.SRC_ATOP);
            }
            if (pager.getCurrentItem() == 3) {
                bar1.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                bar2.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                bar3.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                bar4.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /*public void showDialogForUrlChange() {
        try {
            final Dialog dialog = new Dialog(AppSettings_new.this);
            dialog.setContentView(R.layout.dialog_url_change);
            dialog.setTitle("Update url");
            final EditText et_url = (EditText) dialog.findViewById(R.id.urlText);
            et_url.setText(getURL());
            et_url.setSelection(et_url.length());
            final Button submit = (Button) dialog.findViewById(R.id.submit_button);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String updatedUrl = et_url.getText().toString();
                        setURL(updatedUrl);
                        dialog.dismiss();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        //dialog.dismiss();
                    }
                }
            });
            dialog.setCanceledOnTouchOutside(true);
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        try {
            fragment_auth1 = new Fragment_Auth1();
            fragment_auth2 = new Fragment_Auth2();
            fragment_auth4 = new Fragment_Auth4();
            //fList.add(fragment_auth1);
            fList.add(fragment_auth2);
           // fList.add(fragment_auth4);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return fList;
    }


    public Fragment_Auth4 GetFourthFragment() {
        return this.fragment_auth4;
    }

    @Override
    protected void onResume() {
        try {
           /* if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(AppSettings_new.this, Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(AppSettings_new.this, Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(AppSettings_new.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(AppSettings_new.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED
                    ) {
                bar1.setVisibility(View.VISIBLE);
                pager.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        pager.setCurrentItem(0);
                    }
                }, 100);

            } else {*/
                pager.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (pager.getCurrentItem() == 2) {

                        } else {
                            pager.setCurrentItem(1);
                        }

                    }
                }, 100);


                bar1.setVisibility(View.GONE);
                bar2.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);

           // }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        super.onResume();
    }
}
