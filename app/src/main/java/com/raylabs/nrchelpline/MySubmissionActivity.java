package com.raylabs.nrchelpline;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.raylabs.nrchelpline.Ui.RobotoTextView;
import com.raylabs.nrchelpline.adapter.MySubmissionRecyclerViewAdapter;
import com.raylabs.nrchelpline.helper.DbHelper;
import com.raylabs.nrchelpline.utils.DVoterEntity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MySubmissionActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    MySubmissionRecyclerViewAdapter adapter;
    List<DVoterEntity> formList;
    RobotoTextView toolbar_title;
    MainActivity mainActivity;
    DbHelper dbHelper;
    SharedPreferencesActivity sharedPreferencesActivity;
    Gson gson;
    RelativeLayout noSubmissionlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_submission);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        final Drawable upArrow = getResources().getDrawable(R.drawable.vector_back_white_icon);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(getString(R.string.my_submission));

        mainActivity = new MainActivity();
        dbHelper = new DbHelper(MySubmissionActivity.this);
        sharedPreferencesActivity = SharedPreferencesActivity.getInstance(MySubmissionActivity.this);
        gson = new Gson();
        recyclerView = findViewById(R.id.recyclerView);
        noSubmissionlayout = findViewById(R.id.noSubmissionlayout);
        formList = new ArrayList<DVoterEntity>();
        formList=dbHelper.getAllForms();

        Collections.reverse(formList);

        inflateListView();
    }

    private void inflateListView() {
        if(formList.size()>0) {
            noSubmissionlayout.setVisibility(View.GONE);
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MySubmissionActivity.this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

            recyclerView.setLayoutManager(linearLayoutManager);
           // recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
            adapter = new MySubmissionRecyclerViewAdapter(MySubmissionActivity.this, formList);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }else {
            noSubmissionlayout.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MySubmissionActivity.this,HomeScreenActivity.class));
        finish();
    }
}
