package com.raylabs.nrchelpline;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.raylabs.nrchelpline.helper.CircleTransform;
import com.raylabs.nrchelpline.helper.DbHelper;

import org.apache.commons.lang3.StringUtils;

public class MyProfile extends AppCompatActivity {
    CardView other_details_cardView;
    RelativeLayout edit_layout, landmarkaddress_layout, timings_layout, door_layout, landmark_layout, pincode_layout, back_layout;
    LinearLayout phone_number_layout, dob_layout;
    //    MaterialDesignIconsTextView activity_wizard_media_possition;
    TextView name_tv, email_tv, phone_number_tv, userName_tv, landmarkaddress_tv, timings_tv, door_tv, landmark_tv, pincode_tv;
    MainActivity mainActivity;
    ImageView header_navigation_drawer_media_image;

    DbHelper dbHelper;
    ImageView id_proof_image, licence_image;
    private TextView startTime, endTime;

    RelativeLayout relative_main;
    SharedPreferencesActivity preferencesActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        try {
            mainActivity = new MainActivity();
            dbHelper = new DbHelper(getApplicationContext());
            preferencesActivity = SharedPreferencesActivity.getInstance(getApplicationContext());
            edit_layout = findViewById(R.id.edit_layout);
            header_navigation_drawer_media_image = findViewById(R.id.header_navigation_drawer_media_image);
//            activity_wizard_media_possition = (MaterialDesignIconsTextView) findViewById(R.id.activity_wizard_media_possition);
            other_details_cardView = findViewById(R.id.other_details_cardView);
            landmarkaddress_layout = findViewById(R.id.landmarkaddress_layout);
            relative_main = findViewById(R.id.relative_main);
            timings_layout = findViewById(R.id.timings_layout);
            door_layout = findViewById(R.id.door_layout);
            landmark_layout = findViewById(R.id.landmark_layout);
            pincode_layout = findViewById(R.id.pincode_layout);
            back_layout = findViewById(R.id.back_layout);
            phone_number_layout = findViewById(R.id.phone_number_layout);
            dob_layout = findViewById(R.id.dob_layout);
            name_tv = findViewById(R.id.name_tv);
            email_tv = findViewById(R.id.email_tv);
            phone_number_tv = findViewById(R.id.phone_number_tv);
            landmarkaddress_tv = findViewById(R.id.address_tv);
            userName_tv = findViewById(R.id.userName_tv);
            timings_tv = findViewById(R.id.timings_tv);
            door_tv = findViewById(R.id.door_tv);
            landmark_tv = findViewById(R.id.landmark_tv);
            pincode_tv = findViewById(R.id.pincode_tv);
            id_proof_image = findViewById(R.id.id_proof_image);
            licence_image = findViewById(R.id.licence_image);


            name_tv.setText(preferencesActivity.getUserName());
            phone_number_tv.setText(preferencesActivity.getPhoneNumber());
            if (StringUtils.isBlank(preferencesActivity.getUserEmail())) {
                email_tv.setVisibility(View.GONE);
            } else {
                email_tv.setText(preferencesActivity.getUserEmail() + "");
            }
            if (preferencesActivity.getDob()==null && StringUtils.isBlank(preferencesActivity.getDob())) {
                dob_layout.setVisibility(View.GONE);
            } else {
                userName_tv.setText(preferencesActivity.getDob() + "");
            }


            if (StringUtils.isBlank(preferencesActivity.getUserAddress())) {
                landmarkaddress_tv.setHint("Enter Address");
            } else {
                landmarkaddress_tv.setText(preferencesActivity.getUserAddress() + "");
            }


            if (StringUtils.isBlank(preferencesActivity.getUserArea())) {
                landmark_tv.setHint("Enter Area");
            } else {
                landmark_tv.setText(preferencesActivity.getUserArea() + "");
            }

            /*if (StringUtils.isBlank(pharmacyDetailsEntity.Pincode)) {
                pincode_tv.setHint("Enter Pincode");
            } else {
                pincode_tv.setText(pharmacyDetailsEntity.Pincode + "");
            }*/

            if (preferencesActivity.getImagePath() != null && !preferencesActivity.getImagePath().equalsIgnoreCase("")) {

              /*  Glide.with(getApplicationContext())
                        .load(pharmacyDetailsEntity.Image)
                        .error(R.drawable.default_image)
                        .transform(new CircleTransformGlide(getApplicationContext()))
                        .diskCacheStrategy(DiskCacheStrategy.ALL).into(header_navigation_drawer_media_image);*/


                byte[] decodedString = Base64.decode(preferencesActivity.getImagePath(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                header_navigation_drawer_media_image.setImageBitmap(new CircleTransform().transform(decodedByte));

            }


            back_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });


            edit_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(getApplicationContext(), ProfileEditActivity.class);
                        startActivity(intent);
                        finish();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
            startActivity(intent);
            finish();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
