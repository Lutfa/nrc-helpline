package com.raylabs.nrchelpline.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by PCCS on 5/22/2017.
 */

public class UserPreferences {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;
    public static final String PREF_NAME = "app_flags";


    public static final String ACCESS_TOKEN = "access_taken";
    public static final String USER_GID = "user_guid";
    public static final String DEVICE_UNIQUE_ID = "device_unique_id";
    public static final String SERVER_TOKEN = "server_token";

    public static final String RESET_DEVICE ="false";


    public UserPreferences(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setServerToken(String value) {
        value = value.replace("\"", "");
        editor.putString(SERVER_TOKEN, value);
        editor.commit();
    }

    public String getServerToken() {
        return pref.getString(SERVER_TOKEN, "");
    }

    public void setAccessToken(String value) {
        editor.putString(ACCESS_TOKEN, value);
        editor.commit();
    }

    public String getAccessToken() {
        return pref.getString(ACCESS_TOKEN, "");
    }

    public void setUserGid(String userid) {
        editor.putString(USER_GID, userid);
        editor.commit();
    }

    public String getUserGid() {
        return pref.getString(USER_GID, "");
    }

    public void setDeviceUniqueID(String deviceUniqueId) {
        editor.putString(DEVICE_UNIQUE_ID, deviceUniqueId);
        editor.commit();
    }

    public String getDeviceUniqueId() {
        return pref.getString(DEVICE_UNIQUE_ID, "");
    }


    public void setResetDevice(String resetDevice) {
        editor.putString(RESET_DEVICE, resetDevice);
        editor.commit();
    }

    public String getResetDevice() {
        return pref.getString(RESET_DEVICE, "");
    }


    public void clear() {
        editor.clear().commit();
    }

}