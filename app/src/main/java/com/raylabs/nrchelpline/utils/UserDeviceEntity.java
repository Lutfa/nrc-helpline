package com.raylabs.nrchelpline.utils;


public class UserDeviceEntity {

    public String name;
    public String client_name;
    public String client_mobile_number;

    public String ImageURL;

    public String state;

    public String CompanyName;

    public String OrganizationName;

    public String profile_id;

    public String email;

    public String status;

    public String avatar;

    public String type;

    public String description;
    public String phone_number;
    public UserDeviceEntity data;
}
