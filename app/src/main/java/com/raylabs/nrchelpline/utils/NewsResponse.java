package com.raylabs.nrchelpline.utils;

import java.util.List;

/**
 * Created by ahmed on 9/19/2018.
 */

public class NewsResponse {
    public List<NewsEntity> data;

    public class NewsEntity {
        public String title;
        public String description;
        public String image;
    }
}
