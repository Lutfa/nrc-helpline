package com.raylabs.nrchelpline.utils;

import java.util.List;

/**
 * Created by lutfa on 15-09-2018.
 */

public class DVoterEntity  {
    public String id;
    public String user_id;
    public String submission_id_gen;
    public String name;
    public String so;
    public String occupation;
    public String temporary_address;
    public String permanent_address;
    public String family_tree_details;
    public String cell_no;
    public String case_no;
    public String case_status;
    public String disposed_case;
    public String advocate_details;
    public String arn_no;
    public String no;
    public String age;
    public String nsk_no;
    public String date_detail;
    public String why_the_application_rejected;
    public String reason_1;
    public String reason_2;
    public String reason_3;
    public String request_for;
    public String contact_no;
    public String arrested_on;
    public String name_of_the_dc;
    public String name_of_the_advocate;
    public String form_type;
    public String image;
    public String reason;
    public String ft_no;
    public String main_no;
    public String district;
    public String petioner;
    public String petioner_advocate;
    public String case_category;
    public String respondent;
    public String filing_date;
    public String listing_date;
    public String purpose;
    public String honorable_judge;
    public String category;
    public String prayer;
    public String petentioners_1;
    public String petentioners_2;
    public String petentioners_3;
    public String petentioners_4;
    public String petentioners_5;
    public String document;
    public List<MemberEntity> members;
    public List<String> docs;
    public List<ImageEntity> files;


    public String email;

    public String area_visited;
    public String exclusion_parcentage;
    public String distance;
    public String count_people;
    public String comments;
    public String date;

    public String location;
    public String rejection_reason;
    public String time;
    public String document_1;
    public String document_2;
    public String problem;
    public String name_of_caller;



    public static class MemberEntity {
        public String name;
        public String status;
        public String Image;
    }

    public static class ImageEntity {
        public  String image;
    }
}
