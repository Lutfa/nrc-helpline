package com.raylabs.nrchelpline.utils;

public class UserRegistrationEntity {

    public String PhoneNumber;

    public String UserGuid;

    public String NickName;

    public String DeviceGuid;

    public String DeviceUniqueID;

    public String EmailAddress;

    public String ProfilePicture;

    public String OrganizationName;

    public String State;

    public String Status;

    public String UserId;

    public String Landmark;
    public String DetailAddress;
    public String Pincode;
    public double Latitude;
    public double Longitude;
}
