package com.raylabs.nrchelpline.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raylabs.nrchelpline.DailyReportActivity;
import com.raylabs.nrchelpline.Form1Activity;
import com.raylabs.nrchelpline.Form2Activity;
import com.raylabs.nrchelpline.Form3Activity;
import com.raylabs.nrchelpline.Form4Activity;
import com.raylabs.nrchelpline.LogSheetActivity;
import com.raylabs.nrchelpline.MainActivity;
import com.raylabs.nrchelpline.R;
import com.raylabs.nrchelpline.Ui.RobotoTextView;
import com.raylabs.nrchelpline.utils.DVoterEntity;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed on 9/7/2018.
 */

public class MySubmissionRecyclerViewAdapter extends RecyclerView.Adapter<MySubmissionRecyclerViewAdapter.MyViewHolder> {
    List<DVoterEntity> customerList = new ArrayList<>();
    List<DVoterEntity> arraylist = new ArrayList<>();
    Activity activity;


    public MySubmissionRecyclerViewAdapter(Activity applicationContext, List<DVoterEntity> customerList) {
        this.customerList = customerList;
        this.activity = applicationContext;
        this.arraylist.addAll(customerList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        try {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.submission_list_row, parent, false);
            MyViewHolder rcv = new MyViewHolder(itemView);
            return rcv;
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DVoterEntity entity = customerList.get(position);
        holder.applicant_name.setText(entity.name);
        holder.formId.setText(entity.id);
        holder.application_type.setText(entity.form_type);
        if(!StringUtils.isBlank(entity.submission_id_gen)) {
            holder.form_number.setText(entity.submission_id_gen);
            holder.form_number.setVisibility(View.VISIBLE);

        }else{
            holder.form_number.setVisibility(View.GONE);

        }

    }


    @Override
    public int getItemCount() {
        return customerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RobotoTextView formId, applicant_name, application_type, form_number;


        public MyViewHolder(View itemView) {
            super(itemView);
            try {

                formId = itemView.findViewById(R.id.formId);
                applicant_name = itemView.findViewById(R.id.applicant_name);
                application_type = itemView.findViewById(R.id.application_name);
                form_number = itemView.findViewById(R.id.form_number);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = null;
                        if (application_type.getText().toString().equalsIgnoreCase(MainActivity.D_VOTER)) {
                            intent = new Intent(activity, Form1Activity.class);
                        } else if (application_type.getText().toString().equalsIgnoreCase(MainActivity.NRC_REAPPLICATION)) {
                            intent = new Intent(activity, Form2Activity.class);
                        } else if (application_type.getText().toString().equalsIgnoreCase(MainActivity.COURT_CASE)) {
                            intent = new Intent(activity, Form3Activity.class);
                        } else if (application_type.getText().toString().equalsIgnoreCase(MainActivity.DETENTION_CAMP)) {
                            intent = new Intent(activity, Form4Activity.class);
                        } else if (application_type.getText().toString().equalsIgnoreCase(MainActivity.DAILY_REPORT)) {
                            intent = new Intent(activity, DailyReportActivity.class);
                        }else if (application_type.getText().toString().equalsIgnoreCase(MainActivity.LOG_SHEET)) {
                            intent = new Intent(activity, LogSheetActivity.class);
                        }
                        intent.putExtra(MainActivity.FORMID, formId.getText().toString());

                        activity.startActivity(intent);

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void filter(String charText) {
      /*  try {
            charText = charText.toLowerCase(Locale.getDefault());
            customerList.clear();
            if (charText.length() == 0) {
                customerList.addAll(arraylist);
            } else {
                for (CustomerEntity wp : arraylist) {
                    if (wp.CustomerName.toLowerCase(Locale.getDefault())
                            .contains(charText)||wp.PhoneNumber.toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        customerList.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
*/
    }
}
