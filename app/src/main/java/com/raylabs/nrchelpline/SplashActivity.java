package com.raylabs.nrchelpline;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import org.apache.commons.lang3.StringUtils;

public class SplashActivity extends Activity {
    Animation spinin;
    SharedPreferencesActivity preferencesActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        MainActivity.updateResources(SplashActivity.this);
        preferencesActivity = SharedPreferencesActivity.getInstance(SplashActivity.this);
        spinin = AnimationUtils.loadAnimation(this,
                R.anim.fade_in_welcomesplash);
        // spinin.setDuration(1000);
        LayoutAnimationController controller = new LayoutAnimationController(
                spinin);
        RelativeLayout table = (RelativeLayout) findViewById(R.id.Splashlayout);
        table.setLayoutAnimation(controller);
        // Transition to Main Menu when bottom title finishes animating
        spinin.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation animation) {

                if (!StringUtils.isBlank(preferencesActivity.getUserId())) {
                    if (!StringUtils.isBlank(preferencesActivity.getUserName())) {
                        Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(getApplicationContext(), RegistrationDetailsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Intent intent = new Intent(getApplicationContext(), AppSettings_new.class);
                    startActivity(intent);
                    finish();
                }


            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Toast.makeText(WelcomeSplashActivity.this, "Animation repeat", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAnimationStart(Animation animation) {
                // Toast.makeText(WelcomeSplashActivity.this, "Animation start", Toast.LENGTH_LONG).show();
            }
        });
    }
}
