package com.raylabs.nrchelpline;

import android.content.Context;
import android.content.SharedPreferences;

import com.raylabs.nrchelpline.utils.AppConstant;
import com.raylabs.nrchelpline.utils.Sliders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by ahmed on 11/14/2017.
 */

public class SharedPreferencesActivity {
    private static final String TAG = "SharedPreferences";
    SharedPreferences sharedPreferences;
    public static SharedPreferencesActivity instance;

    public static SharedPreferencesActivity getInstance(Context context)

    {
        if (instance == null)
            instance = new SharedPreferencesActivity(context);
        return instance;
    }

    public SharedPreferencesActivity(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.shared_prefs), Context.MODE_PRIVATE);
    }

    public void setImagePath(String imagePath) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("imagePathSave", imagePath);
        editor.apply();
    }

    public String getImagePath() {
        String imagePath = sharedPreferences.getString("imagePathSave", "");
        return imagePath;
    }

    public void setLike(String id, boolean imagePath) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("Like" + id, imagePath);
        editor.apply();
    }

    public boolean getLike(String id) {
        boolean imagePath = sharedPreferences.getBoolean("Like" + id, false);
        return imagePath;
    }

    public void setSliderImages(ArrayList<String> sliderImages) {

        Gson gson = new Gson();
        String jsonImages = gson.toJson(sliderImages);


        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("sliderImages", jsonImages);
        editor.apply();
    }

    public ArrayList<String> getsliderImages() {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Sliders>>() {
        }.getType();

        String sliderImage = sharedPreferences.getString("sliderImages", "");

        ArrayList<String> sliderImages = gson.fromJson(sliderImage, type);
        return sliderImages;
    }

    public void setActivityCount(String activityName, int count) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(activityName + "count", count);
        editor.apply();
    }

    public int getActivityCount(String activityName) {
        int imagePath = sharedPreferences.getInt(activityName + "count", 0);
        return imagePath;
    }

    public String getNickname() {

        return sharedPreferences.getString(AppConstant.NickName, "");
    }

    public void setUserId(String userid) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("UserId", userid);
        editor.apply();
    }

    public void setPhoneNumber(String PhoneNumber) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("PhoneNumber", PhoneNumber);
        editor.apply();
    }
    public String getPhoneNumber() {
       return sharedPreferences.getString("PhoneNumber", "");
    }
    public String getUserId() {
        return sharedPreferences.getString("UserId", "");
    }

    public void setIsLogIn(boolean login) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isLogIn", login);
        editor.apply();
    }

    public boolean getIsLogIn() {
        return sharedPreferences.getBoolean("isLogIn", false);
    }

    public void setlanguage(String language) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("language", language);
        editor.apply();


    }

    public String getlanguage() {
        return sharedPreferences.getString("language", "");
    }

    public void setUserName(String name) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_name", name);
        editor.apply();

    }

    public String getUserName() {
        return sharedPreferences.getString("user_name", "");
    }

    public void setUserEmail(String email) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_email", email);
        editor.apply();
    }

    public String getUserEmail() {
        return sharedPreferences.getString("user_email", "");
    }

    public void setUserAddress(String address) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_address", address);
        editor.apply();
    }

    public String getUserAddress() {
        return sharedPreferences.getString("user_address", "");
    }

    public void setUserArea(String address) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_area", address);
        editor.apply();
    }

    public String getUserArea() {
        return sharedPreferences.getString("user_area", "");
    }


    public void setDob(String dob) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_dob", dob);
        editor.apply();
    }
    public String getDob() {
        return sharedPreferences.getString("user_dob", "");
    }
}
