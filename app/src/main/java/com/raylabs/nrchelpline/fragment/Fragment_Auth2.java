package com.raylabs.nrchelpline.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.raylabs.nrchelpline.AppSettings_new;
import com.raylabs.nrchelpline.CodeInput.CodeInput;
import com.raylabs.nrchelpline.HomeScreenActivity;
import com.raylabs.nrchelpline.MainActivity;
import com.raylabs.nrchelpline.R;
import com.raylabs.nrchelpline.RegistrationDetailsActivity;
import com.raylabs.nrchelpline.SharedPreferencesActivity;
import com.raylabs.nrchelpline.adapter.CustomAdapter;
import com.raylabs.nrchelpline.utils.UserPreferences;
import com.raylabs.nrchelpline.utils.UserRegistrationRequest;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class Fragment_Auth2 extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final String TAG = "PhoneAuthActivity";
    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";
    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;
    public static TextView phone_number_tv, otp_tv;
    public EditText enter_phone_number;
    public String otp_number;
    public String mVerificationId;
    ProgressBar progress_loading, progress_loading_otp;
    String[] countryNames;
    RelativeLayout right_image;
    ImageView right_image_icon;
    TextView bottom_bar_textview;
    TextView bottom_bar_textview2;
    int spinner_position;
    String country_code, userID, email, name, workplace;
    //    EditText number1, number2, number3, number4, number5, number6;
    ImageView edit_phone_number;
    RelativeLayout bottom_bar;
    String _phoneNumber = "", profile_picture;
    Gson gson = new Gson();
    String[] code1;
    Spinner spin;

    //firebase
    LinearLayout fragment3_layout, fragment2_layout;
    CodeInput cInput;
    boolean verify = false;
    private String CountryZipCode;
    private ProgressDialog pdialogue;
    private String deviceID = "";
    private String deviceUniqueID = "";
    private GoogleCloudMessaging gcm;
    private String regId;
    private UserPreferences userPreferences;
    private MainActivity mainActivity;
    // [END declare_auth]
    private RelativeLayout relative_main_header;
    // [START declare_auth]
    private FirebaseAuth mAuth;
    private boolean mVerificationInProgress = false;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private int phoneLength;
    private Bitmap image, bitmap;
    private String filename;
    private URL url;
    private String stringNoQuotes, UserGuid = "";

    SharedPreferencesActivity sharedPreferencesActivity;

    public Fragment_Auth2() {
        // Required empty public constructor
    }

    public static String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment__auth2, container, false);

        spin = (Spinner) view.findViewById(R.id.simpleSpinner);
        enter_phone_number = (EditText) view.findViewById(R.id.enter_phone_number);
        right_image = (RelativeLayout) view.findViewById(R.id.right_image);
        right_image_icon = (ImageView) view.findViewById(R.id.right_image_icon);
        bottom_bar_textview = (TextView) view.findViewById(R.id.bottom_bar_textview);
        bottom_bar_textview2 = (TextView) view.findViewById(R.id.bottom_bar_textview2);
        fragment3_layout = (LinearLayout) view.findViewById(R.id.fragment3_layout);
        fragment2_layout = (LinearLayout) view.findViewById(R.id.fragment2_layout);
        progress_loading = (ProgressBar) view.findViewById(R.id.progress_loading);
        progress_loading_otp = (ProgressBar) view.findViewById(R.id.progress_loading_otp);
        progress_loading_otp.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        progress_loading.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        phone_number_tv = (TextView) view.findViewById(R.id.phone_number_tv);
        edit_phone_number = (ImageView) view.findViewById(R.id.edit_phone_number);
        bottom_bar = (RelativeLayout) view.findViewById(R.id.bottom_bar);
        relative_main_header = (RelativeLayout) view.findViewById(R.id.relative_main_header);
        cInput = (CodeInput) view.findViewById(R.id.code_input);
        otp_tv = (TextView) view.findViewById(R.id.otp_tv);
        cInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        userPreferences = new UserPreferences(getActivity());
        mainActivity = new MainActivity();
        sharedPreferencesActivity = SharedPreferencesActivity.getInstance(getActivity());
        sharedPreferencesActivity.setIsLogIn(true);
        cInput.setCodeReadyListener(new CodeInput.codeReadyListener() {
            @Override
            public void onCodeReady(Character[] code) {
                if (mainActivity.isInternetConnected(getActivity())) {
                    hideKeyboard(getActivity());
                    cInput.hideKeyBoard();
                    progress_loading_otp.setVisibility(View.VISIBLE);
                    bottom_bar_textview2.setClickable(false);
                    bottom_bar_textview2.setEnabled(false);
                    onNext();
                } else {
                    Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                }
            }
        });
//        showSoftKeyboard(number1);
//        number1.requestFocus();
//        number1.setFocusable(true);

//        phone_number_tv.setText(enter_phone_number.getText().toString());


        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(enter_phone_number.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        enter_phone_number.requestFocus();

        final StringBuilder sb = new StringBuilder();
        try {
            TelephonyManager tMgr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            String mPhoneNumber = tMgr.getLine1Number();

            if (mPhoneNumber != null) {
                enter_phone_number.setText(mPhoneNumber);
                enter_phone_number.setSelection(mPhoneNumber.length());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        edit_phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                right_image.setVisibility(View.VISIBLE);
                enter_phone_number.setEnabled(true);
                enter_phone_number.setClickable(true);

                spin.setEnabled(true);
                spin.setClickable(true);

                cInput.setCode("");

                progress_loading.setVisibility(View.GONE);
                fragment2_layout.setVisibility(View.VISIBLE);
                fragment3_layout.setVisibility(View.GONE);
                bottom_bar_textview.setVisibility(View.VISIBLE);
                bottom_bar_textview.setClickable(true);
                bottom_bar_textview.setEnabled(true);

                bottom_bar_textview2.setVisibility(View.GONE);
                progress_loading_otp.setVisibility(View.GONE);

//                AppSettings_new.pager.setCurrentItem(AppSettings_new.pager.getCurrentItem() - 1, true);
                AppSettings_new.bar3.getBackground().setColorFilter(getResources().getColor(R.color.lightgray), PorterDuff.Mode.SRC_ATOP);
            }
        });


        otp_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainActivity.isInternetConnected(getActivity())) {
                    resendVerificationCode(phone_number_tv.getText().toString(), mResendToken);
                } else {
                    Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                }

            }
        });

        try {
            spin.getBackground().setColorFilter(getResources().getColor(R.color.black_54), PorterDuff.Mode.SRC_ATOP);
            spin.setOnItemSelectedListener(this);

            countryNames = getActivity().getResources().getStringArray(R.array.country_code);

            CustomAdapter customAdapter = new CustomAdapter(getActivity(), countryNames);
            spin.setAdapter(customAdapter);

            String zipcode = getUserCountry(getActivity());
            userPreferences = new UserPreferences(getActivity());
            String name = "";
            Locale loc;
            if (!StringUtils.isBlank(zipcode)) {
                loc = new Locale("", zipcode);
                name = loc.getCountry();
            }


           /* try {
                String[] rl = this.getResources().getStringArray(R.array.country_code);
                for (int i = 0; i < rl.length; i++) {
                    String[] g = rl[i].split("\\(");
                    if (g[0].trim().equals(name)) {
                        CountryZipCode = g[0];
                        country_code = g[1];
                        spinner_position = i;
                        break;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            String code = country_code;
            code1 = code.split("\\)");
            System.out.println("CountryZipCode " + CountryZipCode);
*/

            spin.setSelection(94);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        if (enter_phone_number.getText().toString().length() > 3) {
            right_image_icon.setBackgroundResource(R.drawable.vector_check_right_white_icon);
            right_image_icon.getBackground().setColorFilter(Color.parseColor("#1f8005"), PorterDuff.Mode.SRC_ATOP);
            right_image.setClickable(true);
            right_image.setEnabled(true);
            bottom_bar_textview.setEnabled(true);
            bottom_bar_textview.setTextColor(getResources().getColor(R.color.white));
            bottom_bar_textview.setClickable(true);
        } else {
            right_image_icon.setBackgroundResource(R.drawable.vector_check_right_white_icon);
            right_image_icon.getBackground().setColorFilter(Color.parseColor("#33000000"), PorterDuff.Mode.SRC_ATOP);
            right_image.setClickable(false);
            bottom_bar_textview.setClickable(false);
            right_image.setEnabled(false);
            bottom_bar_textview.setEnabled(false);
            bottom_bar_textview.setTextColor(getResources().getColor(R.color.white_75));
        }

        enter_phone_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() > 3) {
                    right_image_icon.setBackgroundResource(R.drawable.vector_check_right_white_icon);
                    right_image_icon.getBackground().setColorFilter(Color.parseColor("#1f8005"), PorterDuff.Mode.SRC_ATOP);
                    right_image.setClickable(true);
                    right_image.setEnabled(true);
                    bottom_bar_textview.setEnabled(true);
                    bottom_bar_textview.setTextColor(getResources().getColor(R.color.white));
                    bottom_bar_textview.setClickable(true);
                } else {
                    right_image_icon.setBackgroundResource(R.drawable.vector_check_right_white_icon);
                    right_image_icon.getBackground().setColorFilter(Color.parseColor("#33000000"), PorterDuff.Mode.SRC_ATOP);
                    right_image.setClickable(false);
                    bottom_bar_textview.setClickable(false);
                    right_image.setEnabled(false);
                    bottom_bar_textview.setEnabled(false);
                    bottom_bar_textview.setTextColor(getResources().getColor(R.color.white_75));
                }

            }
        });
        right_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainActivity.isInternetConnected(getActivity())) {
                    onNextFrag();
                } else {
                    Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                }
            }
        });
        bottom_bar_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainActivity.isInternetConnected(getActivity())) {
                    onNextFrag();
                } else {
                    Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                }

            }
        });
        bottom_bar_textview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (String.valueOf(cInput.getCode()) != null) {
                    if (mainActivity.isInternetConnected(getActivity())) {
                        progress_loading_otp.setVisibility(View.VISIBLE);
                        bottom_bar_textview2.setClickable(false);
                        bottom_bar_textview2.setEnabled(false);
                        onNext();
                    } else {
                        Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please enter OTP", Toast.LENGTH_SHORT).show();
                }


            }
        });

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);
                Log.d(TAG, "onVerificationCompleted string:" + credential.toString());
                Log.d(TAG, "onVerificationCompleted sms code:" + credential.getSmsCode());
                Log.d(TAG, "onVerificationCompleted get provider:" + credential.getProvider());

                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                updateUI(STATE_VERIFY_SUCCESS, credential);
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential);
                // startActivity(new Intent(getActivity(), HomeScreenActivity.class));
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    enter_phone_number.setError("Invalid phone number");
                    progress_loading.setVisibility(View.GONE);
                    right_image.setVisibility(View.VISIBLE);
                    bottom_bar_textview.setClickable(true);
                    bottom_bar_textview.setEnabled(true);
                    enter_phone_number.setEnabled(true);
                    enter_phone_number.setClickable(true);

                    spin.setEnabled(true);
                    spin.setClickable(true);
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    progress_loading.setVisibility(View.GONE);
                    right_image.setVisibility(View.VISIBLE);
                    bottom_bar_textview.setClickable(true);
                    bottom_bar_textview.setEnabled(true);
                    enter_phone_number.setEnabled(true);
                    enter_phone_number.setClickable(true);

                    spin.setEnabled(true);
                    spin.setClickable(true);
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Quota exceeded.", Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseNetworkException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    progress_loading.setVisibility(View.GONE);
                    right_image.setVisibility(View.VISIBLE);
                    bottom_bar_textview.setClickable(true);
                    bottom_bar_textview.setEnabled(true);
                    enter_phone_number.setEnabled(true);
                    enter_phone_number.setClickable(true);

                    spin.setEnabled(true);
                    spin.setClickable(true);
                    Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    progress_loading.setVisibility(View.GONE);
                    right_image.setVisibility(View.VISIBLE);
                    bottom_bar_textview.setClickable(true);
                    bottom_bar_textview.setEnabled(true);
                    enter_phone_number.setEnabled(true);
                    enter_phone_number.setClickable(true);

                    spin.setEnabled(true);
                    spin.setClickable(true);
                    Toast.makeText(getActivity(), "Please try again Later", Toast.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }

                // Show a message and update the UI
                // [START_EXCLUDE]
                updateUI(STATE_VERIFY_FAILED);
                // [END_EXCLUDE]
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                Log.d(TAG, "onVerificationToken : " + token);
                Log.d(TAG, "onVerificationId : " + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;


                // [START_EXCLUDE]
                // Update UI
                updateUI(STATE_CODE_SENT);
                // [END_EXCLUDE]
            }
        };


        return view;


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser == true) {
            //showKeyboard(getActivity());
            try {
                TelephonyManager tMgr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
                String mPhoneNumber = tMgr.getLine1Number();

                if (mPhoneNumber != null) {
                    enter_phone_number.setText(mPhoneNumber);
                    enter_phone_number.setSelection(mPhoneNumber.length());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void onNext() {

       /* try {
            String code = String.valueOf(Arrays.toString(cInput.getCode()));
            verifyPhoneNumberWithCode(mVerificationId, code);
        } catch (Exception ex) {
            ex.printStackTrace();
        }*/
        try {
            Character[] code1 = cInput.getCode();
            String code = Character.valueOf(code1[0]).toString() + Character.valueOf(code1[1]).toString() + Character.valueOf(code1[2]).toString() + Character.valueOf(code1[3]).toString() + Character.valueOf(code1[4]).toString() + Character.valueOf(code1[5]).toString();
            verifyPhoneNumberWithCode(mVerificationId, code);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

       /* // [START_EXCLUDE]
        if (mVerificationInProgress && validatePhoneNumber()) {
            startPhoneNumberVerification(enter_phone_number.getText().toString());
        }*/
        // [END_EXCLUDE]
    }

    public void onNextFrag() {
        AppSettings_new.bar3.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
        String final_phone_number;

        enter_phone_number.setEnabled(false);
        enter_phone_number.setClickable(false);

        spin.setEnabled(false);
        spin.setClickable(false);

        bottom_bar_textview.setClickable(false);
        bottom_bar_textview.setEnabled(false);


        progress_loading.setVisibility(View.VISIBLE);
        right_image.setVisibility(View.GONE);


        if (enter_phone_number.getText().toString().startsWith("+")) {
            final_phone_number = enter_phone_number.getText().toString();
            phone_number_tv.setText(final_phone_number);

            if (isValidPhoneNumber(final_phone_number)) {
                enter_phone_number.setError(null);
                startPhoneNumberVerification(final_phone_number);
            } else {
                enter_phone_number.setError("Invalid phone number");
                progress_loading.setVisibility(View.GONE);
                right_image.setVisibility(View.VISIBLE);
                bottom_bar_textview.setClickable(true);
                bottom_bar_textview.setEnabled(true);
                enter_phone_number.setEnabled(true);
                enter_phone_number.setClickable(true);

                spin.setEnabled(true);
                spin.setClickable(true);
            }
        } else {
            final_phone_number = code1[0].concat(enter_phone_number.getText().toString());
            phone_number_tv.setText(final_phone_number);

            if (isValidPhoneNumber(final_phone_number)) {
                boolean status = validateUsing_libphonenumber(code1[0], enter_phone_number.getText().toString());
                if (status) {
                    enter_phone_number.setError(null);
                    startPhoneNumberVerification(final_phone_number);
                } else {
                    enter_phone_number.setError("Invalid phone number");
                    progress_loading.setVisibility(View.GONE);
                    right_image.setVisibility(View.VISIBLE);
                    bottom_bar_textview.setClickable(true);
                    bottom_bar_textview.setEnabled(true);
                    enter_phone_number.setEnabled(true);
                    enter_phone_number.setClickable(true);

                    spin.setEnabled(true);
                    spin.setClickable(true);
                }
            } else {
                enter_phone_number.setError("Invalid phone number");
                progress_loading.setVisibility(View.GONE);
                right_image.setVisibility(View.VISIBLE);
                bottom_bar_textview.setClickable(true);
                bottom_bar_textview.setEnabled(true);
                enter_phone_number.setEnabled(true);
                enter_phone_number.setClickable(true);

                spin.setEnabled(true);
                spin.setClickable(true);
            }
        }


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        String[] g = countryNames[position].split("\\(");
        code1 = g[1].split("\\)");
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        try {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    getActivity(),               // Activity (for callback binding)
                    mCallbacks);        // OnVerificationStateChangedCallbacks
            // [END start_phone_auth]

            mVerificationInProgress = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void verifyPhoneNumberWithCode(String verificationId, String code) {
        otp_tv.setEnabled(false);
        otp_tv.setClickable(false);
        cInput.setClickable(false);
        cInput.setEnabled(false);
        edit_phone_number.setEnabled(false);
        edit_phone_number.setClickable(false);
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                getActivity(),               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    // [END resend_verification]

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser user = task.getResult().getUser();
                            verify = true;
                            user.getToken(true)
                                    .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                        public void onComplete(@NonNull Task<GetTokenResult> task) {
                                            if (task.isSuccessful()) {
                                                String idToken = task.getResult().getToken();
                                                // Send token to your backend via HTTPS
                                                userPreferences.setAccessToken(idToken);
                                                _phoneNumber = phone_number_tv.getText().toString();
                                               /* TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
                                                deviceUniqueID = telephonyManager.getDeviceId();*/
                                                registerInBackground();
                                            } else {
                                            }
                                        }
                                    });

                            // [START_EXCLUDE]
                            updateUI(STATE_SIGNIN_SUCCESS, user);
                            // [END_EXCLUDE]
                        } else {

                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
//                                number6.setError("Invalid code");
//
                                bottom_bar_textview2.setClickable(true);
                                bottom_bar_textview2.setEnabled(true);
                                otp_tv.setEnabled(true);
                                otp_tv.setClickable(true);

                                cInput.setEnabled(true);
                                cInput.setClickable(true);
                                if (verify == true) {

                                } else {
                                    progress_loading_otp.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Check the code", Toast.LENGTH_SHORT).show();
                                }

                                edit_phone_number.setEnabled(true);
                                edit_phone_number.setClickable(true);
                            }
                            // [START_EXCLUDE silent]
                            // Update UI
                            updateUI(STATE_SIGNIN_FAILED);
                            // [END_EXCLUDE]
                        }
                    }
                });
    }
    // [END sign_in_with_phone]

    private void signOut() {
        mAuth.signOut();
        updateUI(STATE_INITIALIZED);
    }

    private void updateUI(int uiState) {
        updateUI(uiState, mAuth.getCurrentUser(), null);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }

    private void updateUI(int uiState, FirebaseUser user) {
        updateUI(uiState, user, null);
    }

    private void updateUI(int uiState, PhoneAuthCredential cred) {
        updateUI(uiState, null, cred);
    }

    private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
                break;
            case STATE_CODE_SENT:
                fragment3_layout.setVisibility(View.VISIBLE);
                fragment2_layout.setVisibility(View.GONE);
                bottom_bar_textview.setVisibility(View.GONE);
                bottom_bar_textview2.setVisibility(View.VISIBLE);
                progress_loading.setVisibility(View.GONE);
                hideKeyboard(getActivity());


                AppSettings_new.bar3.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                Toast.makeText(getActivity(), "Code sent", Toast.LENGTH_SHORT).show();
                break;
            case STATE_VERIFY_FAILED:
                break;
            case STATE_VERIFY_SUCCESS:

                if (cred.getSmsCode() != null) {
                    otp_number = cred.getSmsCode();
                }
                if (otp_number != null) {
                    try {
                        cInput.setCode(otp_number);
                        progress_loading_otp.setVisibility(View.VISIBLE);
                        bottom_bar_textview2.setClickable(false);
                        bottom_bar_textview2.setEnabled(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                break;
            case STATE_SIGNIN_FAILED:
                // No-op, handled by sign-in check
                break;
            case STATE_SIGNIN_SUCCESS:
                // Np-op, handled by sign-in check
                break;
        }
    }

    private boolean validatePhoneNumber() {
        String phoneNumber = enter_phone_number.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            enter_phone_number.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    private void addToken(String deviceuniqueid, String userid, String phonenumber,
                          String nickName, String email, String profile_pic, String location, String age) {


        sharedPreferencesActivity.setUserId(userid);
        sharedPreferencesActivity.setPhoneNumber(phonenumber);
        sharedPreferencesActivity.setUserEmail(email);
        sharedPreferencesActivity.setUserName(name);
        sharedPreferencesActivity.setImagePath(profile_pic);
        sharedPreferencesActivity.setUserAddress(location);
        sharedPreferencesActivity.setDob(age);


    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    private boolean validateUsing_libphonenumber(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }

        boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);
        if (isValid) {
            String internationalFormat = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            return true;
        } else {
            return false;
        }
    }

    private void registerInBackground() {
        gcm = GoogleCloudMessaging.getInstance(getActivity());
        new AsyncTask<Void, Void, String>() {
            ProgressDialog dialogGCM;

            @Override
            protected void onPreExecute() {
                try {
                    dialogGCM = new ProgressDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    dialogGCM.setMessage(getString(R.string.dialogPleaseWait));
                    dialogGCM.setIndeterminate(false);
                    dialogGCM.setCancelable(false);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getActivity());
                    }

                    //Civilciti account staging gcm/fcm project
                    regId = gcm.register("895207482070");

                    deviceID = regId;

                    msg = "Device registered, registration ID=" + regId;

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();

                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                try {
                    dialogGCM.dismiss();
                    if (msg.contains("SERVICE_NOT_AVAILABLE")) {
                    } else
                        LongOperation();
                    //new LongRunningGetIO().execute();
                } catch (Exception ex) {
                    if (progress_loading.getVisibility() == View.VISIBLE) {
                        progress_loading.setVisibility(View.GONE);
                        right_image.setVisibility(View.VISIBLE);
                        bottom_bar_textview.setClickable(true);
                        bottom_bar_textview.setEnabled(true);
                        enter_phone_number.setEnabled(true);
                        enter_phone_number.setClickable(true);
                        spin.setClickable(true);
                        spin.setEnabled(true);
                    }
                    if (progress_loading_otp.getVisibility() == View.VISIBLE) {
                        progress_loading_otp.setVisibility(View.GONE);
                        edit_phone_number.setClickable(true);
                        edit_phone_number.setEnabled(true);

                        bottom_bar_textview2.setClickable(true);
                        bottom_bar_textview2.setEnabled(true);
                        cInput.setEnabled(true);
                        cInput.setClickable(true);
                    }

                    ex.printStackTrace();
                }

            }
        }.execute(null, null, null);
    }

    private void LongOperation() {
        String json;
        UserRegistrationRequest request = new UserRegistrationRequest();
        request.phone = _phoneNumber;
        request.device_token = deviceID;
        request.device_id = deviceUniqueID;
        request.device_type = "Android";


        json = gson.toJson(request);
        try {
            Rx2AndroidNetworking.post(MainActivity.SITE_URL + MainActivity.URL_SIGNUP)
                    .addJSONObjectBody(new JSONObject(json))
                    .addHeaders("Content-type", "application/json")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {

                        @Override
                        public void onError(Throwable e) {
                            Log.i("errror here", "edokati");
                            if (progress_loading.getVisibility() == View.VISIBLE) {
                                progress_loading.setVisibility(View.GONE);
                                right_image.setVisibility(View.VISIBLE);
                                bottom_bar_textview.setClickable(true);
                                bottom_bar_textview.setEnabled(true);
                                enter_phone_number.setEnabled(true);
                                enter_phone_number.setClickable(true);
                                spin.setClickable(true);
                                spin.setEnabled(true);
                                Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
                            }
                            if (progress_loading_otp.getVisibility() == View.VISIBLE) {
                                progress_loading_otp.setVisibility(View.GONE);
                                edit_phone_number.setClickable(true);
                                edit_phone_number.setEnabled(true);

                                bottom_bar_textview2.setClickable(true);
                                bottom_bar_textview2.setEnabled(true);
                                cInput.setEnabled(true);
                                cInput.setClickable(true);
                                Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onComplete() {

                        }

                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {
                            //do anything with response
                            URL url;
                            if (response != null) {
                                try {
                                    if (response.get("status").toString().equalsIgnoreCase("Registration Success full")) {

                                        userID = "" + response.get("id");
                                        userPreferences.setUserGid(userID);

                                        _phoneNumber = "" + response.get("phone");
                                 /*       name = "" + response.get("name");
                                        email = "" + response.get("email");*/


                                        addToken("", userID, _phoneNumber, "", "", profile_picture,"","");
                                       /* final FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                                        Fragment_Auth4 newFragment = new Fragment_Auth4("","","");
                                        t.replace(R.id.relative_main, newFragment);
                                        t.addToBackStack(TAG);
                                        t.commit();
                                        AppSettings_new.pager.setCurrentItem(AppSettings_new.pager.getCurrentItem() + 1, true);
                                        AppSettings_new.bar4.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
*/
                                        startActivity(new Intent(getActivity(), RegistrationDetailsActivity.class));

                                    } else if (response.get("status").toString().equalsIgnoreCase("Your already existed")) {

                                        String avatar, location = "", age = "";

                                        //JSONObject jsonobject = response.getJSONObject("data");
                                        userID = "" + response.get("id");

                                        userPreferences.setUserGid(userID);

                                        _phoneNumber = "" + response.get("phone");
                                        name = "" + response.get("name");
                                        email = "" + response.get("email");
                                        if (response.has("avatar")) {
                                            profile_picture = response.get("avatar") + "";
                                        }
                                        if (response.has("location")) {
                                            location = response.get("location") + "";
                                        }
                                        if (response.has("age")) {
                                            age = response.get("age") + "";
                                        }


                                        addToken(null, userID, _phoneNumber, name, email, profile_picture, location, age);
                                       /* Bundle bundle = new Bundle();
                                        if (!StringUtils.isBlank(name)) {
                                            bundle.putString("$nick_name", name);
                                        }
                                        if (!StringUtils.isBlank(email)) {
                                            bundle.putString("$emailAddress", email);
                                        }*/
                                       /* final FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                                        Fragment_Auth4 newFragment = new Fragment_Auth4(email,name,"");
                                        newFragment.setArguments(bundle);
                                        t.replace(R.id.relative_main, newFragment);
                                        t.addToBackStack(TAG);
                                        t.commit();
                                        AppSettings_new.pager.setCurrentItem(AppSettings_new.pager.getCurrentItem() + 1, true);
                                        AppSettings_new.bar4.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
                                 */

                                        // startActivity(new Intent(getActivity(),HomeScreenActivity.class));
                                        if (!StringUtils.isBlank(name)) {
                                            startActivity(new Intent(getActivity(), HomeScreenActivity.class));
                                        } else {
                                            startActivity(new Intent(getActivity(), RegistrationDetailsActivity.class));
                                        }

                                    }

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                } catch (Exception e) {

                                    if (progress_loading.getVisibility() == View.VISIBLE) {
                                        progress_loading.setVisibility(View.GONE);
                                        right_image.setVisibility(View.VISIBLE);
                                        bottom_bar_textview.setClickable(true);
                                        bottom_bar_textview.setEnabled(true);
                                        enter_phone_number.setEnabled(true);
                                        enter_phone_number.setClickable(true);
                                        spin.setClickable(true);
                                        spin.setEnabled(true);
                                    }
                                    if (progress_loading_otp.getVisibility() == View.VISIBLE) {
                                        progress_loading_otp.setVisibility(View.GONE);
                                        edit_phone_number.setClickable(true);
                                        edit_phone_number.setEnabled(true);

                                        bottom_bar_textview2.setClickable(true);
                                        bottom_bar_textview2.setEnabled(true);
                                        cInput.setEnabled(true);
                                        cInput.setClickable(true);
                                    }
                                    spin.setEnabled(true);
                                    spin.setClickable(true);
                                    Log.d("PostSignIn", "Error: " + e.getMessage());
                                    e.printStackTrace();
                                }

                            }
                            Log.i("tag", "response is" + response);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void DeviceUpdateApi(UserRegistrationRequest userRegistrationRequest) {/*
       String json = gson.toJson(userRegistrationRequest);
        try {
            Rx2AndroidNetworking.patch(MainActivity.SITE_URL + getString(R.string.api_device_update) + userID)
                    .addJSONObjectBody(new JSONObject(json))
                    .addHeaders("Content-type", "application/json")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {

                        @Override
                        public void onError(Throwable e) {
                            Log.i("errror here", "edokati");
                        }

                        @Override
                        public void onComplete() {

                        }

                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {
                            //do anything with response
                            UserDeviceResponse userDeviceResponse = new UserDeviceResponse();
                            UserDeviceEntity result = null;
                            if (response != null) {
                                try {
                                    if (response.get("status").toString().equalsIgnoreCase("you have old device")) {

                                    }else
                                    {

                                    }
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                            Log.i("tag", "response is" + response);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }

    public void SaveImage(Bitmap bitmap2, byte[] getByteArray, String imgActualPath) {
        File file = null, profileDirectory;
        FileOutputStream fos;
        String root = Environment.getExternalStorageDirectory().toString();
        File directory = new File(root + "/CivilCiti");
        if (!directory.exists())
            directory.mkdirs();
        profileDirectory = directory.getParentFile();

        profileDirectory = new File(directory + "/ProfileImages");

        if (!profileDirectory.exists())
            profileDirectory.mkdirs();

        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);

        //String fname = "/" + filename;
        filename = "profileImage.jpg";

        file = new File(profileDirectory, filename);

        if (file.exists())
            file.delete();
        try {

            if (imgActualPath != null) {
                bitmap = BitmapFactory.decodeFile(imgActualPath);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                getByteArray = stream.toByteArray();
                fos = new FileOutputStream(file, false);
                fos.write(getByteArray);
                fos.flush();
                fos.close();
            } else if (getByteArray != null) {
                // bitmap = BitmapFactory.decodeFile(filpath);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                // getByteArray = stream.toByteArray();
                fos = new FileOutputStream(file, false);
                fos.write(getByteArray);
                fos.flush();
                fos.close();
            } else if (bitmap2 != null) {
                FileOutputStream out = new FileOutputStream(file, false);
                bitmap2.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
