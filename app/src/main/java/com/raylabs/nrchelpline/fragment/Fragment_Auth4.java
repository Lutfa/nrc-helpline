package com.raylabs.nrchelpline.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.raylabs.nrchelpline.MainActivity;
import com.raylabs.nrchelpline.R;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


public class Fragment_Auth4 extends Fragment {

    public static final String MY_PREFS_NAME = "MY_PREFS_NAME";
    private static final String MyPREFERENCES = null;
    public static SharedPreferences.Editor editor;
    public static String nick_name, Organization_name, email_id;
    public EditText enter_name;
    public EditText enter_email_id;
    public EditText enter_organization;
    String userGuid = null;
    String deviceGuid = null;
    String deviceUniqueID = null;
    String phoneNumber = null, ImageURL = null,userID;
    TextView percentage_text, welcome_text;
    int globalcount = 0;

    ImageView enter_image, right_image, right_image2;
    View view_name, view_email, view_organization;
    int intuserGuid = 0;
    RelativeLayout progressbar_layout;
    ProgressBar progressbar;
    private ProgressDialog pdialogue;
    private String companyname, nickName, emailid, personalStatus;
    private MainActivity mainActivity = new MainActivity();
    private SharedPreferences app_preference;
    private Gson gson = new Gson();
    private RelativeLayout relative_main_header, bottom_bar;
    private String TAG = "PhoneAuthActivity";
    private ArrayList<String> activityGuids;

    @SuppressLint("ValidFragment")
    public Fragment_Auth4(String email, String nickName, String companyname) {
        nick_name = nickName;
        email_id = email;
        Organization_name = companyname;
    }

    public Fragment_Auth4() {
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_auth4, container, false);
        relative_main_header = (RelativeLayout) view.findViewById(R.id.relative_main_header);
        bottom_bar = (RelativeLayout) view.findViewById(R.id.bottom_bar);
        enter_name = (EditText) view.findViewById(R.id.enter_name);
        enter_email_id = (EditText) view.findViewById(R.id.enter_email_id);
        enter_organization = (EditText) view.findViewById(R.id.enter_organization);
        percentage_text = (TextView) view.findViewById(R.id.percentage_text);
        enter_image = (ImageView) view.findViewById(R.id.enter_image);
        right_image = (ImageView) view.findViewById(R.id.right_image);
        right_image2 = (ImageView) view.findViewById(R.id.right_image2);
        view_name = (View) view.findViewById(R.id.view_name);
        view_email = (View) view.findViewById(R.id.view_email);
        view_organization = (View) view.findViewById(R.id.view_organization);
        welcome_text = (TextView) view.findViewById(R.id.welcome_text);
        progressbar_layout = (RelativeLayout) view.findViewById(R.id.progressbar_layout);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
     //   userPreferences = new UserPreferences(getActivity());


        editor = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        app_preference = getActivity().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

        bottom_bar.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                try {
                   /* userKeyDetails = mainActivity.getTokenFromDb(getActivity());
                    userGuid = userKeyDetails.getUserid();
                    deviceGuid = userKeyDetails.getDeviceid();
                    deviceUniqueID = userKeyDetails.getDeviceuniqueid();
                    phoneNumber = userKeyDetails.getPhoneNumber();*/
                    onNext();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });
        enter_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                try {
                    view_name.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    view_email.setBackgroundColor(getResources().getColor(R.color.black_34));
                    view_organization.setBackgroundColor(getResources().getColor(R.color.black_34));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        });

        enter_organization.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                try {
                    view_name.setBackgroundColor(getResources().getColor(R.color.black_34));
                    view_email.setBackgroundColor(getResources().getColor(R.color.black_34));
                    view_organization.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        enter_email_id.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                try {
                    view_name.setBackgroundColor(getResources().getColor(R.color.black_34));
                    view_email.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    view_organization.setBackgroundColor(getResources().getColor(R.color.black_34));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        });
        enter_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                enter_email_id.requestFocus();
            }
        });
        right_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enter_organization.requestFocus();
            }
        });
        right_image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(getActivity());
                right_image2.setClickable(false);
                try {
                   /* userKeyDetails = mainActivity.getTokenFromDb(getActivity());
                    userGuid = userKeyDetails.getUserid();
                    deviceGuid = userKeyDetails.getDeviceid();
                    deviceUniqueID = userKeyDetails.getDeviceuniqueid();
                    phoneNumber = userKeyDetails.getPhoneNumber();
                    onNext();*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        return view;


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser == true) {
            try {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                if (StringUtils.isBlank(nick_name)) {
                    welcome_text.setText("Hello!");
                    enter_name.setText("");
                } else {
                    welcome_text.setText("Welcome Back!");
                    enter_name.setText(nick_name);
                    enter_name.setSelection(0);
                    app_preference.edit().putBoolean("first_time_install", true).commit();
                }
                if (StringUtils.isBlank(email_id)) {
                    enter_email_id.setText("");
                } else {
                    enter_email_id.setText(email_id);
                }
                if (StringUtils.isBlank(Organization_name)) {
                    enter_organization.setText("");
                } else {
                    enter_organization.setText(Organization_name);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    public void onNext() {
        try {
            if (!mainActivity.isInternetConnected(getActivity())) {
                mainActivity.show_snakbar(getString(R.string.check_internet), relative_main_header);
            } else {
                if (enter_name.getText().toString().trim().length() > 0) {

                    if (enter_email_id.getText().toString().length() > 0) {
                        if (isValidEmail(enter_email_id.getText().toString())) {
                            //NavigateToNext(getActivity());
                            enter_organization.setEnabled(false);
                            enter_organization.setClickable(false);
                            enter_name.setEnabled(false);
                            enter_name.setClickable(false);
                            enter_email_id.setEnabled(false);
                            enter_email_id.setClickable(false);

                            progressbar_layout.setVisibility(View.VISIBLE);
                            bottom_bar.setVisibility(View.VISIBLE);
                            ConfirmUserRegistration();
                        } else {
                            // NavigateToNext(getActivity());

                            enter_organization.setEnabled(true);
                            enter_organization.setClickable(true);
                            enter_name.setEnabled(true);
                            enter_name.setClickable(true);
                            enter_email_id.setEnabled(true);
                            enter_email_id.setClickable(true);

                            progressbar_layout.setVisibility(View.GONE);
                            bottom_bar.setVisibility(View.VISIBLE);
                            mainActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), "Please enter valid email id", Toast.LENGTH_SHORT).show();
                                }
                            });

//                        ConfirmUserRegistration();
                        }

                    } else {
                        //mainActivity.show_snakbar(getString(R.string.msg_nickname), relative_main_header);
                        mainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Enter MailId", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }else
                {
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "Enter Nickname", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private void ConfirmUserRegistration() {

    }


   /* private void ConfirmUserRegistration() {
        String json;
        PackageInfo info = null;
        UserDeviceEntity request = new UserDeviceEntity();
        nickName = enter_name.getText().toString();
        companyname = enter_organization.getText().toString();
        emailid = enter_email_id.getText().toString();

        intuserGuid = Integer.parseInt(userPreferences.getUserGid());

        request.name = nickName;
        request.OrganizationName = companyname;
        request.email = emailid;
        request.ImageURL = ImageURL;
        json = gson.toJson(request);
        try {
            Rx2AndroidNetworking.put(MainActivity.SITE_URL + getString(R.string.api_user_confirmation) + intuserGuid)
                    .addJSONObjectBody(new JSONObject(json))
                    .addHeaders("Content-type", "application/json")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {

                        @Override
                        public void onError(Throwable e) {
                            Log.i("errror here", "edokati");
                        }

                        @Override
                        public void onComplete() {

                        }

                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {
                            //do anything with response
                            UserDeviceResponse userDeviceResponse = new UserDeviceResponse();
                            UserDeviceEntity result = null;
                             if (response != null) {
                                try {
                                    if (response.get("status").toString().equalsIgnoreCase("Profile Updated")) {
                                        JSONObject jsonobject = response.getJSONObject("data");
                                       *//* userDeviceResponse = gson.fromJson(String.valueOf(jsonobject), UserDeviceResponse.class);
                                        result = userDeviceResponse.Response;*//*
                                        userID = "" + jsonobject.get("id");
                                        phoneNumber = "" + jsonobject.get("phone");
                                        email_id = "" + jsonobject.get("email");
                                        nick_name = "" + jsonobject.get("name");
                                        addToken(userID,phoneNumber,email_id,nick_name);
                                        Intent intent = new Intent(getActivity(), HomeScreenActivity.class);
                                        startActivity(intent);
                                    }
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                    enter_organization.setEnabled(true);
                                    enter_organization.setClickable(true);
                                    enter_name.setEnabled(true);
                                    enter_name.setClickable(true);
                                    enter_email_id.setEnabled(true);
                                    enter_email_id.setClickable(true);

                                    progressbar_layout.setVisibility(View.GONE);
                                    bottom_bar.setVisibility(View.VISIBLE);
                                    mainActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    enter_organization.setEnabled(true);
                                    enter_organization.setClickable(true);
                                    enter_name.setEnabled(true);
                                    enter_name.setClickable(true);
                                    enter_email_id.setEnabled(true);
                                    enter_email_id.setClickable(true);

                                    progressbar_layout.setVisibility(View.GONE);
                                    bottom_bar.setVisibility(View.VISIBLE);
                                    mainActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                            }else
                             {
                                 enter_organization.setEnabled(true);
                                 enter_organization.setClickable(true);
                                 enter_name.setEnabled(true);
                                 enter_name.setClickable(true);
                                 enter_email_id.setEnabled(true);
                                 enter_email_id.setClickable(true);

                                 progressbar_layout.setVisibility(View.GONE);
                                 bottom_bar.setVisibility(View.VISIBLE);
                                 mainActivity.runOnUiThread(new Runnable() {
                                     @Override
                                     public void run() {
                                         Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
                                     }
                                 });
                             }
                            Log.i("tag", "response is" + response);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/

    private void addToken(String userID, String phoneNumber, String email_id, String nick_name) {
      /*  db = new CivilCitiDBHelper(getActivity());
        UserKeyDetails tkn = new UserKeyDetails();
        tkn.setUserid(userID);
        tkn.setPhoneNumber(phoneNumber);
        tkn.setNickname(nick_name);
        tkn.setProfileEmail(email_id);
        db.addUpdateUserDetails(tkn);*/
    }
}
