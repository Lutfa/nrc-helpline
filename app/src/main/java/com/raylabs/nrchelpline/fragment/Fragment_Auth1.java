package com.raylabs.nrchelpline.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.raylabs.nrchelpline.AppSettings_new;
import com.raylabs.nrchelpline.R;
import com.raylabs.nrchelpline.Ui.RobotoTextView;


public class Fragment_Auth1 extends Fragment {
    private static final int PERMISSION_REQUEST_CODE = 1;
    RelativeLayout askPermissionFullLayout, relative_digitsbutton;
    RobotoTextView settingTextV, nextTextV, auth_button;
    String[] PERMISSIONS = {"android.permission.READ_PHONE_STATE", "android.permission.READ_CONTACTS", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
    boolean isPermissionDeniedNever = false, isRationaleDialogShown = false;

    public Fragment_Auth1() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //deleteExistingDB();
        //CivilCitiDBHandler dbm = new CivilCitiDBHandler(getActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for getActivity() fragment
        View view = inflater.inflate(R.layout.fragment_fragment__auth1, container, false);
        askPermissionFullLayout = (RelativeLayout) view.findViewById(R.id.permissionlayout);
        relative_digitsbutton = (RelativeLayout) view.findViewById(R.id.digits_bottomLayout);
        nextTextV = (RobotoTextView) view.findViewById(R.id.nextTV);
        auth_button = (RobotoTextView) view.findViewById(R.id.auth_button);
        settingTextV = (RobotoTextView) view.findViewById(R.id.settingTV);
        nextTextV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                marshmallowDialog();
            }
        });
        settingTextV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                marshmallowSetting();
            }
        });
        auth_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent = new Intent(getActivity(), HomeScreenActivity.class);
                startActivity(intent);*/

                AppSettings_new.pager.setCurrentItem(AppSettings_new.pager.getCurrentItem() + 1, true);
                AppSettings_new.bar2.getBackground().setColorFilter(getResources().getColor(R.color.bottom_bar_color), PorterDuff.Mode.SRC_ATOP);
            }
        });

        return view;
    }

    private void marshmallowSetting() {
        try {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getActivity().getPackageName()));
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getActivity().startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void marshmallowDialog() {
        try {
            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED
                    ) {
                requestPermission();
            } else {
                askPermissionFullLayout.setVisibility(View.GONE);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void requestPermission() {
        try {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_PHONE_STATE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                isRationaleDialogShown = true;
                requestPermissions(PERMISSIONS, PERMISSION_REQUEST_CODE);
            } else {
                isPermissionDeniedNever = true;
                requestPermissions(PERMISSIONS, PERMISSION_REQUEST_CODE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                try {
                    boolean isPhoneStateDone = false,
                            isReadSMS = false, isAccessCoarseDone = false,
                            isShowRationalePhoneState = false,
                            isShowRationaleReadSMS = false,isWriteExtStorageDone = false, isReadExtStorageDone = false, isShowRationaleWriteExtStorage = false, isShowRationaleReadExtStorage = false;
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];
                        if (permission.equals(Manifest.permission.READ_PHONE_STATE)) {
                            isShowRationalePhoneState = ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isPhoneStateDone = true;
                            } else {
                                isPhoneStateDone = false;
                            }
                        }
                        else if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            isShowRationaleWriteExtStorage = ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isWriteExtStorageDone = true;
                            } else {
                                isWriteExtStorageDone = false;
                            }
                        } else if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            isShowRationaleReadExtStorage = ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isReadExtStorageDone = true;
                            } else {
                                isReadExtStorageDone = false;
                            }
                        }
                    }
                    boolean isRationale = (isShowRationalePhoneState || isShowRationaleReadSMS || isShowRationaleWriteExtStorage ||
                            isShowRationaleReadExtStorage);
                    if (isRationale) {
                        nextTextV.setVisibility(View.VISIBLE);
                        settingTextV.setVisibility(View.GONE);
                    } else if (!isRationale && (isPhoneStateDone && isReadSMS && isWriteExtStorageDone &&
                            isReadExtStorageDone)) {
                        nextTextV.setVisibility(View.VISIBLE);
                        settingTextV.setVisibility(View.GONE);
                    } else if (!isRationale && (!isPhoneStateDone || !isReadSMS || !isWriteExtStorageDone || !isReadExtStorageDone)) {
                        nextTextV.setVisibility(View.GONE);
                        settingTextV.setVisibility(View.VISIBLE);
                    } else {
                        nextTextV.setVisibility(View.GONE);
                        settingTextV.setVisibility(View.VISIBLE);
                    }
                    break;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED|| Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                relative_digitsbutton.setVisibility(View.GONE);
                askPermissionFullLayout.setVisibility(View.VISIBLE);
            } else {
                askPermissionFullLayout.setVisibility(View.GONE);
                relative_digitsbutton.setVisibility(View.VISIBLE);
//                getSupportActionBar().setTitle(getString(R.string.app_name));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
