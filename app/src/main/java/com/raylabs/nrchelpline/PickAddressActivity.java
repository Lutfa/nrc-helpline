package com.raylabs.nrchelpline;/*
package com.assam.amcr;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.zibew.distributor.Ui.SearchMedicineResponse;
import com.zibew.distributor.adapter.GooglePlacesSearchAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PickAddressActivity extends Baseactivity implements OnMapReadyCallback {
    List<String> array_list;
    GoogleMap googleMap;
    Geocoder geocoder;
    MarkerOptions markerOptions;
    LatLng coordinate;
    List<Address> addresses;
    MainActivity mainActivity;
    private String TAG = "GoogleMapBottomSheet";
    SharedPreferences app_preference = null;
    public static final String MyPREFERENCES = null;
    EditText searchPlaceET;
    TextView searchIcon, LocationTitle, LocationDetails, picked_text, next_text;
    SearchMedicineResponse locationModel;
    private static final String LOG_TAG = "AutoCompleteSearch";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    String previousScreen;
    private static final String API_KEY = "AIzaSyDiynKssSfW2Cdg9UhnY8hLfXGffe1woFw";
    String currentLocationPincode = null, currentLocation = null, gpsFullAddress, countryCode = null;
    RelativeLayout searchListLayout, mapFullLayout;
    RelativeLayout backLayout;
    ListView listView;
    Toolbar toolbar;
    boolean NoSavedAddress = false, isAddressEditClicked = false, addressDisplayed = false;
    String loginType;
    CardView searchHeaderLayout;
    Gson gson;
    Toolbar loginToolbar;
    String json ;
    private View view;

    //    private LoginCredentials loginCredentials;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReference();
        setHeader(loginToolbar);
      

    }

    @Override
    public void setReference() {
        view = LayoutInflater.from(this).inflate(R.layout.activity_pick_address, container);
        try {
            loginToolbar = (Toolbar) view.findViewById(R.id.toolbar);
            setSupportActionBar(loginToolbar);
            getSupportActionBar().setTitle(getString(R.string.pick_address));
            mainActivity = new MainActivity();
            locationModel = new SearchMedicineResponse();
            gson = new Gson();
            app_preference = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            mapFullLayout = (RelativeLayout) view.findViewById(R.id.mapRelLayout);
            searchHeaderLayout = (CardView) view.findViewById(R.id.searchHeaderLayout);
       */
/* next_text = (TextView) findViewById(R.id.next_text);
        picked_text = (TextView) findViewById(R.id.picked_text);*//*

            FloatingActionButton gpslocation = (FloatingActionButton) view.findViewById(R.id.myLocationButton);
            gpslocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowGoogleMapWithCurrentLocation();
                }
            });
            searchHeaderLayout.setVisibility(View.VISIBLE);
            searchPlaceET = (EditText) view.findViewById(R.id.search_field_editT);
            searchIcon = (TextView) view.findViewById(R.id.searchicon);
            listView = (ListView) view.findViewById(R.id.lisiview);
            backLayout = (RelativeLayout) view.findViewById(R.id.backLayout);
            LocationTitle = (TextView) view.findViewById(R.id.LocationTitle);
            LocationDetails = (TextView) view.findViewById(R.id.LocationDetails);
            searchListLayout = (RelativeLayout) view.findViewById(R.id.searchListLayout);
            app_preference = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            Intent intent = getIntent();
            MapFragment supportMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.googleMap);
            supportMapFragment.getMapAsync(PickAddressActivity.this);


            searchIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (searchPlaceET.getText().toString() != null &&
                            !searchPlaceET.getText().toString().equalsIgnoreCase("")) {
                        if(searchIcon.getText().toString().equalsIgnoreCase(getString(R.string.fontello_x_mark_masked)))
                        {
                            mapFullLayout.setVisibility(View.VISIBLE);
                            searchPlaceET.setText(null);
                            hideKeyboard(PickAddressActivity.this);
                        }

                    }
                }
            });
            searchPlaceET.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence input, int start, int before,
                                          int count) {
                    try {
                        boolean isLive = mainActivity.isInternetConnected(PickAddressActivity.this);
                        if (!isLive) {
                            Toast.makeText(PickAddressActivity.this, getResources().getString(R.string.checkInternetCon), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (input.length() >= 2) {
                            DemoTask getPlaces = new DemoTask(input.toString());
                            ArrayList<String> resultList = getPlaces.execute().get();
                            searchIcon.setText(R.string.fontello_x_mark_masked);
                            if (resultList != null) {
                                mapFullLayout.setVisibility(View.GONE);
                                listView.setVisibility(View.VISIBLE);
                                searchListLayout.setVisibility(View.VISIBLE);
                                listView.setAdapter(new GooglePlacesSearchAdapter(PickAddressActivity.this, resultList));
                            }
                        } else {
                            listView.setAdapter(null);
                            listView.setVisibility(View.GONE);
                            searchListLayout.setVisibility(View.GONE);
                            searchIcon.setText(R.string.fontello_search);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                }

                @SuppressLint("DefaultLocale")
                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    String selectedPlace = null;
                    selectedPlace = (String) parent.getItemAtPosition(position);
                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(listView.getWindowToken(), 0);

                        String[] placeArray = selectedPlace.split("\\,");
                        String firstName = placeArray[0];
                        SharedPreferences.Editor edit = app_preference.edit();
                        edit.putString("LocalName", firstName);
                        edit.commit();
                        searchPlaceET.setText(firstName);
                        searchListLayout.setVisibility(View.GONE);
                        mapFullLayout.setVisibility(View.VISIBLE);
                        if (Geocoder.isPresent()) {
                            GetCoordinatesFromPlace(selectedPlace);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void ShowGoogleMapWithCurrentLocation() {
        double latitude = 0.0, longitude = 0.0;
        try {



            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.setMyLocationEnabled(true);
            geocoder = new Geocoder(PickAddressActivity.this, Locale.getDefault());

            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location location = null;
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                onLocationChanged(new LatLng(location.getLatitude(), location.getLongitude()));
            } else {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    onLocationChanged(new LatLng(location.getLatitude(), location.getLongitude()));
                }
            }
            if (!isAddressEditClicked) {
                if (location != null) {
                    onLocationChanged(new LatLng(location.getLatitude(), location.getLongitude()));
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
            }
            coordinate = new LatLng(latitude, longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(coordinate));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 1));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            markerOptions.position(coordinate);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onLocationChanged(LatLng latLng) {
        String detailAddressShowing = null;
        double latitude = latLng.latitude;
        double longitude = latLng.longitude;
        coordinate = new LatLng(latitude, longitude);
        googleMap.clear();
        String postalcode;
        markerOptions = new MarkerOptions();
        SharedPreferences.Editor edit;
        geocoder = new Geocoder(PickAddressActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Log.v("log_tag", "addresses+)_+++" + addresses);

            locationModel.Landmark = addresses.get(0).getSubLocality();
            locationModel.City = addresses.get(0).getLocality();
            locationModel.State = addresses.get(0).getAdminArea();
            locationModel.Country = addresses.get(0).getCountryName();
            locationModel.DetailAddress = addresses.get(0).getAddressLine(0);
            postalcode = addresses.get(0).getPostalCode();
            locationModel.Pincode = postalcode;
            locationModel.Latitude = addresses.get(0).getLatitude();
            locationModel.Longitude = addresses.get(0).getLongitude();
            countryCode = addresses.get(0).getCountryCode();
            try {

                edit = app_preference.edit();
                edit.putString("CountryCode", countryCode);
                edit.putString("Longitude",String.valueOf(locationModel.Longitude));
                edit.putString("Latitude", String.valueOf(locationModel.Latitude));
                edit.commit();

                if (postalcode != null) {
                    edit.putString("NewPincode", postalcode);
                    edit.commit();
                } else {
                    for (int i = 0; i < 10; i++) {
                        String addressDetails = addresses.get(0).getAddressLine(i);
                        if (addressDetails != null) {
                            postalcode = mainActivity.containsPinCode(addressDetails);
                            if (postalcode != null && !postalcode.equalsIgnoreCase("")) {
                                edit = app_preference.edit();
                                edit.putString("NewPincode", postalcode);
                                edit.commit();
                                locationModel.Pincode = postalcode;
                                break;
                            }

                        } else
                            break;
                    }
                }
                if (!addressDisplayed) {
                    String localArea = addresses.get(0).getAddressLine(0) ;
                    LocationTitle.setText(locationModel.Landmark);
                    LocationDetails.setText(localArea);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        markerOptions.position(coordinate);
        googleMap.addMarker(markerOptions);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(coordinate));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 1));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    public ArrayList<String> getPlacesList(String input) {
        ArrayList<String> resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:" + countryCode);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }
        return resultList;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        
    }


    class DemoTask extends AsyncTask<String, String, ArrayList<String>> {
        ArrayList<String> resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        String input;

        public DemoTask(String inputtext) {
            input = inputtext;
        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {

            try {
                StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
                sb.append("?key=" + API_KEY);
                sb.append("&components=country:" + countryCode);
                sb.append("&input=" + URLEncoder.encode(input, "utf8"));

                URL url = new URL(sb.toString());

                System.out.println("URL: " + url);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(conn.getInputStream());

                // Load the results into a StringBuilder
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
            } catch (MalformedURLException e) {
                Log.e(LOG_TAG, "Error processing Places API URL", e);
                return resultList;
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error connecting to Places API", e);
                return resultList;
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }

            try {

                // Create a JSON object hierarchy from the results
                JSONObject jsonObj = new JSONObject(jsonResults.toString());
                JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

                // Extract the Place descriptions from the results
                resultList = new ArrayList<String>(predsJsonArray.length());
                for (int i = 0; i < predsJsonArray.length(); i++) {
                    System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                    System.out.println("============================================================");
                    resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                }
            } catch (JSONException e) {
                Log.e(TAG, "Cannot process JSON results", e);
            }
            return resultList;
        }

        protected void onPostExecute(Void result) {
            // TODO: do something with the feed
        }


    }

    public void GetCoordinatesFromPlace(String location) {
        try {
            SharedPreferences.Editor edit = app_preference.edit();
            Geocoder gc = new Geocoder(this);
            List<Address> addresses = gc.getFromLocationName(location, 5); // get the found Address Objects
            List<LatLng> ll = new ArrayList<LatLng>(addresses.size()); // A list to save the coordinates if they are available
            for (Address addr : addresses) {
                if (addr.hasLatitude() && addr.hasLongitude()) {
                    edit.putString("Longitude", String.valueOf(addr.getLongitude()));
                    edit.putString("Latitude", String.valueOf(addr.getLatitude()));
                    edit.commit();
                    onLocationChanged(new LatLng(addr.getLatitude(), addr.getLongitude()));
                }
            }
        } catch (IOException e) {
        }
    }

    public void onMapReady(GoogleMap Map) {
        googleMap = Map;
        ShowGoogleMapWithCurrentLocation();

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.d(TAG, "Map clicked");
                onLocationChanged(latLng);
            }
        });
    }

    private void initGoogleMap() {
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);
        supportMapFragment.getMapAsync(this);
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap Map) {
                googleMap = Map;
                ShowGoogleMapWithCurrentLocation();
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.google_map_next, menu);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.next:

//                loginCredentials.setCountryName(locationModel.Country);
                json = gson.toJson(locationModel);
                Intent intent = new Intent(PickAddressActivity.this, FullRegistrationDetailsActivity.class);
                intent.putExtra("LocationJson", json);
                Bundle bndlanimation = ActivityOptions
                        .makeCustomAnimation(getApplicationContext(),
                                R.anim.back_swipe2, R.anim.back_swipe1).toBundle();
                startActivity(intent, bndlanimation);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
*/
