package com.raylabs.nrchelpline;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.raylabs.nrchelpline.Ui.RobotoButton;
import com.raylabs.nrchelpline.Ui.RobotoEdittext;
import com.raylabs.nrchelpline.Ui.RobotoTextView;
import com.raylabs.nrchelpline.helper.DbHelper;
import com.raylabs.nrchelpline.helper.GraphicsUtil;
import com.raylabs.nrchelpline.utils.DVoterEntity;
import com.google.gson.Gson;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.raylabs.nrchelpline.MainActivity.MyPREFERENCES;

public class Form4Activity extends AppCompatActivity {
    RobotoTextView toolbar_title;
    RobotoEdittext applicant_name, son_of, occupation, temp_add, perm_add, famity_details,
            arrested_year, name_of_dc, contact_no, case_status, name_of_advocate, cell_no;
    RobotoButton btn_submit;
ProgressDialog progressBar;
    SharedPreferencesActivity sharedPreferencesActivity;
    Gson gson;
    DbHelper dbHelper;
    String formId;
    private DVoterEntity entity;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private SharedPreferences app_preference;
    private static int SELECT_FILE_CAMERA = 1;
    private static int SELECT_FILE_GALLERY = 2;
    String[] PERMISSIONS = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.CAMERA"};
    boolean isPermissionDeniedNever = false, isRationaleDialogShown = false;
    ProgressDialog progressDialogCommon;
    MainActivity mainActivity = null;
    private String filename = null;
    private Uri selectedImageUri, currentImageUri;
    private String imagePath = null;
    private AlertDialog alert;
    private boolean setBitmap, isRemoved;
    private String mCurrentPhotoPath;
    private byte[] imageInByte;
    String imageBase64String;
    Bitmap bitmap;
    LinearLayout imageLayout;
    RobotoButton btn_add_document;
    List<DVoterEntity.ImageEntity> imagelist;
    List<String> imageUrllist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form4);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        final Drawable upArrow = getResources().getDrawable(R.drawable.vector_back_white_icon);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(getString(R.string.detention_camp));
        mainActivity = new MainActivity();
        dbHelper = new DbHelper(Form4Activity.this);
        sharedPreferencesActivity = SharedPreferencesActivity.getInstance(Form4Activity.this);
        gson = new Gson();
        imagelist = new ArrayList<>();
        imageUrllist = new ArrayList<>();

        applicant_name = findViewById(R.id.applicant_name);
        son_of = findViewById(R.id.son_of);
        occupation = findViewById(R.id.occupation);
        temp_add = findViewById(R.id.temp_add);
        perm_add = findViewById(R.id.perm_add);
        famity_details = findViewById(R.id.famity_details);
        arrested_year = findViewById(R.id.year);
        name_of_dc = findViewById(R.id.name_of_dc);
        contact_no = findViewById(R.id.contact_no);
        case_status = findViewById(R.id.case_status);
        name_of_advocate = findViewById(R.id.advocate_details);
        cell_no = findViewById(R.id.cell_no);
        btn_submit = findViewById(R.id.btn_submit);
        btn_add_document = findViewById(R.id.btn_add_document);
        imageLayout = findViewById(R.id.imageLayout);
        Intent intent = getIntent();
        formId = intent.getStringExtra(MainActivity.FORMID);
        if (!StringUtils.isBlank(formId)) {
            inflateLayout();
        }
        btn_add_document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                marshmallowDialog();

            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String Name_of_dc, name, Son_of, Occupation, Temp_add, Perm_add, Contact_no, Famity_details, Cell_no, Case_status, Advocate_details, Arrested_on;

                name = applicant_name.getText().toString();
                Son_of = son_of.getText().toString();
                Occupation = occupation.getText().toString();
                Temp_add = temp_add.getText().toString();
                Perm_add = perm_add.getText().toString();
                Famity_details = famity_details.getText().toString();
                Cell_no = cell_no.getText().toString();
                Contact_no = contact_no.getText().toString();
                Case_status = case_status.getText().toString();
                Advocate_details = name_of_advocate.getText().toString();
                Arrested_on = arrested_year.getText().toString();
                Name_of_dc = name_of_dc.getText().toString();

                if (!StringUtils.isBlank(name) && !StringUtils.isBlank(Name_of_dc) && !StringUtils.isBlank(Arrested_on) && !StringUtils.isBlank(Contact_no) && !StringUtils.isBlank(Son_of) && !StringUtils.isBlank(Occupation) && !StringUtils.isBlank(Temp_add) && !StringUtils.isBlank(Perm_add) && !StringUtils.isBlank(Famity_details) && !StringUtils.isBlank(Cell_no) &&
                        !StringUtils.isBlank(Case_status) && !StringUtils.isBlank(Advocate_details)) {


                    DVoterEntity dVoterEntity = new DVoterEntity();
                    dVoterEntity.user_id = sharedPreferencesActivity.getUserId();
                    dVoterEntity.name = name;
                    dVoterEntity.so = Son_of;
                    dVoterEntity.occupation = Occupation;
                    dVoterEntity.temporary_address = Temp_add;
                    dVoterEntity.permanent_address = Perm_add;
                    dVoterEntity.family_tree_details = Famity_details;
                    dVoterEntity.arrested_on = Arrested_on;

                    dVoterEntity.cell_no = Cell_no;
                    dVoterEntity.contact_no = Contact_no;
                    dVoterEntity.case_status = Case_status;
                    dVoterEntity.name_of_the_dc = Name_of_dc;
                    dVoterEntity.name_of_the_advocate = Advocate_details;
                    dVoterEntity.docs = imageUrllist;
                    dVoterEntity.form_type = MainActivity.DETENTION_CAMP;

                    if (mainActivity.isInternetConnected(Form4Activity.this)) {
                        progressBar = new ProgressDialog(Form4Activity.this);
                        progressBar.setTitle("Submitting form...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                        progressBar.show();
                        progressBar.setCancelable(false);
                        if (!StringUtils.isBlank(formId)) {
                            longOperationUpdate(dVoterEntity);
                        } else {
                            longOperation(dVoterEntity);
                        }


                    }else{
                        mainActivity.showToast(Form4Activity.this,getString(R.string.check_internet));
                    }
                }else{
                    mainActivity.showToast(Form4Activity.this,getString(R.string.fill_all_fields));
                }
            }
        });

    }

    private void inflateLayout() {
        try {


        entity = dbHelper.getFormDetails(formId, MainActivity.DETENTION_CAMP);

        applicant_name.setText(entity.name);
        son_of.setText(entity.so);
        occupation.setText(entity.occupation);
        temp_add.setText(entity.temporary_address);
        perm_add.setText(entity.permanent_address);
        famity_details.setText(entity.family_tree_details);
        arrested_year.setText(entity.arrested_on);
        name_of_dc.setText(entity.name_of_the_dc);
        contact_no.setText(entity.contact_no);
        case_status.setText(entity.case_status);
        name_of_advocate.setText(entity.name_of_the_advocate);
        cell_no.setText(entity.cell_no);
        if (entity.docs != null) {
            imageUrllist=entity.docs;
            for (int i = 0; i < entity.docs.size(); i++) {


                LayoutInflater inflater = LayoutInflater.from(Form4Activity.this);
                View inflatedLayout = inflater.inflate(R.layout.image_row_layout, null, false);
                ImageView imageView = inflatedLayout.findViewById(R.id.imageView);
                Picasso.with(Form4Activity.this)
                        .load(entity.docs.get(i))

                        .into(imageView);
              //  Picasso.with(getApplicationContext()).load(entity.docs.get(i)).into(imageView);
                imageLayout.addView(inflatedLayout);
            }
        }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void longOperationUpdate(final DVoterEntity dVoterEntity) {
        String json = gson.toJson(dVoterEntity);

        try {
            Rx2AndroidNetworking.patch(MainActivity.SITE_URL + "detentioncamp/" + formId)
                    .addJSONObjectBody(new JSONObject(json))
                    .addHeaders("Content-type", "application/json")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {
                                    progressBar.cancel();
                                    String Id = null;
                                    if (response.get("status").toString().equalsIgnoreCase("Submitted Success full")) {

                                        Id = "" + response.get("id");
                                        dVoterEntity.id = Id;
                                        dbHelper.addUpdateFormTable(dVoterEntity);
                                        startActivity(new Intent(Form4Activity.this, HomeScreenActivity.class));

                                    } else if (response.get("status").toString().equalsIgnoreCase("Updated Success full")) {
                                        dVoterEntity.id = formId;
                                        dbHelper.addUpdateFormTable(dVoterEntity);
                                        startActivity(new Intent(Form4Activity.this, HomeScreenActivity.class));

                                    }


                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                } catch (Exception e) {

                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                            progressBar.cancel();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void longOperation(final DVoterEntity dVoterEntity) {
        String json = gson.toJson(dVoterEntity);
        try {
            Rx2AndroidNetworking.post(MainActivity.SITE_URL + "detentioncamp")
                    .addJSONObjectBody(new JSONObject(json))
                    .addHeaders("Content-type", "application/json")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {
                                    progressBar.cancel();
                                    String Id = null,formnumber=null;
                                    if (response.get("status").toString().equalsIgnoreCase("Submitted Success full")) {

                                        Id = "" + response.get("id");
                                        if(response.has("submission_id_gen")) {
                                            formnumber = "" + response.get("submission_id_gen");
                                            dVoterEntity.submission_id_gen=formnumber;
                                        }
                                    }
                                    dVoterEntity.id = Id;
                                    dbHelper.addUpdateFormTable(dVoterEntity);
                                    startActivity(new Intent(Form4Activity.this, HomeScreenActivity.class));

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                    progressBar.cancel();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressBar.cancel();
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                            progressBar.cancel();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Form4Activity.this,FormListActivity.class));

    }

    private void marshmallowDialog() {
        try {
            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                ShowOptions();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void requestPermission() {
        try {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CAMERA)
                    ) {
                isRationaleDialogShown = true;
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);
            } else {
                isPermissionDeniedNever = true;
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                try {
                    boolean isPhoneStateDone = false,
                            isCamera = false,
                            isShowRationaleReadExternal = false,
                            isShowRationaleReadCamera = false;
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];
                        if (permission.equals(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            isShowRationaleReadExternal = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isPhoneStateDone = true;
                            } else {
                                isPhoneStateDone = false;
                            }
                        } else if (permission.equals(android.Manifest.permission.CAMERA)) {
                            isShowRationaleReadCamera = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isCamera = true;
                            } else {
                                isCamera = false;
                            }
                        }
                    }
                    if (isPhoneStateDone && isCamera) {
                        ShowOptions();
                    }
                    boolean isRationale = (isShowRationaleReadExternal || isShowRationaleReadCamera);
                    if (isRationale) {
//                        nextTextV.setVisibility(View.VISIBLE);
//                        settingTextV.setVisibility(View.GONE);
                    } else if (!isRationale && (isPhoneStateDone)) {
//                        nextTextV.setVisibility(View.VISIBLE);
//                        settingTextV.setVisibility(View.GONE);
                    } else if (!isRationale && (!isPhoneStateDone || !isCamera)) {
//                        nextTextV.setVisibility(View.GONE);
                        marshmallowSetting();
//                        settingTextV.setVisibility(View.VISIBLE);
                    } else {
                        marshmallowSetting();
                    }
                    break;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void marshmallowSetting() {
        try {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            /*Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void ShowOptions() {

        final CharSequence[] items = {getResources().getString(R.string.takeNewPhoto), getResources().getString(R.string.chooseFromGallery),
                getResources().getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                Boolean isSDPresent = Environment
                        .getExternalStorageState().equals(
                                Environment.MEDIA_MOUNTED);
                if (item == 0) {
                    if (isSDPresent)
                        getPhotoFromCamera();
                    else
                        Toast.makeText(
                                Form4Activity.this,
                                "Please turn off USB storage or insert your SD card and try again",
                                Toast.LENGTH_SHORT).show();

                    return;
                } else if (item == 1) {
                    if (isSDPresent)
                        getPhotoFromGallery();
                    else
                        Toast.makeText(
                                Form4Activity.this,
                                "Please turn off USB storage or insert your SD card and try again",
                                Toast.LENGTH_SHORT).show();
                    return;

                } /*else if (item == 2) {
                    imagePath = null;
                    setBitmap = false;
                    isRemoved = true;
                    return;

                }*/ else
                    alert.cancel();
            }

        });

        alert = builder.create();
        alert.show();

//        TextView textView = (TextView) alert.getWindow().findViewById(android.R.id.message);
//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat_Regular.otf");
//        textView.setTypeface(font);

    }

    protected void getPhotoFromCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                currentImageUri = createImageFile();
                SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();
                editor.putString("currentImageUri", String.valueOf(currentImageUri));
                editor.commit();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
            } else {
                File file = new File(currentImageUri.getPath());
                Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            }
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                startActivityForResult(intent, SELECT_FILE_CAMERA);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Uri createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        if (storageDir.exists()) {
            image = File.createTempFile(timeStamp, /* prefix */
                    ".jpg", /* suffix */
                    storageDir /* directory */
            );
        } else {
            storageDir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            if (storageDir.exists()) {
                image = File.createTempFile(timeStamp, /* prefix */
                        ".jpg", /* suffix */
                        storageDir /* directory */
                );
            }
        }
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return Uri.fromFile(image);
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(currentImageUri);
        this.sendBroadcast(mediaScanIntent);
    }

    protected void getPhotoFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select picture to upload "), SELECT_FILE_GALLERY);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_FILE_GALLERY) {
                try {
                    selectedImageUri = data.getData();
                  /*  if (isProfilePic) {
                        performCrop(selectedImageUri);
                    } else {*/
                    imagePath = getRealPathFromURI(selectedImageUri);
                    app_preference = getSharedPreferences(MyPREFERENCES,
                            Context.MODE_PRIVATE);
                    if (imagePath.equalsIgnoreCase("")) {
                        imagePath = ((String) selectedImageUri.toString()).substring(7, selectedImageUri.toString().length());
                    }

                    bitmap = decodeFile1(imagePath, selectedImageUri);
                    // }
                } catch (Exception ex) {
                    Toast.makeText(Form4Activity.this, "Internal error",
                            Toast.LENGTH_LONG).show();
                } catch (OutOfMemoryError ex) {
                    Toast.makeText(Form4Activity.this, "Out of memory",
                            Toast.LENGTH_LONG).show();
                }
            } else if (requestCode == SELECT_FILE_CAMERA) {
                try {
                    app_preference = getSharedPreferences(MyPREFERENCES,
                            Context.MODE_PRIVATE);
                    String currentUri = app_preference.getString("currentImageUri", "");

                    currentImageUri = Uri.parse(currentUri);
                    galleryAddPic();
                  /*  if (isProfilePic) {
                        performCrop(selectedImageUri);
                    } else {*/
                    if (mCurrentPhotoPath == null) {
                        imagePath = currentImageUri.getPath();
                    } else
                        imagePath = mCurrentPhotoPath;


                    bitmap = decodeFile1(imagePath, selectedImageUri);
                    // }
                } catch (Exception e) {
                    Toast.makeText(Form4Activity.this, getResources().getString(R.string.internal_error),
                            Toast.LENGTH_LONG).show();
                } catch (OutOfMemoryError ex) {
                    Toast.makeText(Form4Activity.this, "Out of memory",
                            Toast.LENGTH_LONG).show();
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    GraphicsUtil graphicUtil = null;
                    Uri resultUri = result.getUri();
                    try {
                        Bitmap thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);

                        String filename = "profileImage.jpg";
                        // imagePath = Environment.getExternalStorageDirectory().toString() + "/Xampr/ProfileImages/" + filename;
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thePic.compress(Bitmap.CompressFormat.JPEG, 95, stream);
                        imageInByte = stream.toByteArray();
                        imageBase64String = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                        // mainActivity.SaveImageGallery(thePic, null, filename, null);
                        //SaveImage(thePic, imageInByte, null);
                        imagePath = Environment.getExternalStorageDirectory().toString() + "/NRC/ProfileImages/" + filename;
                        File file = new File(imagePath);
                        if (file.exists()) {
                            bitmap = BitmapFactory.decodeFile(imagePath);
                            // profileImage.setImageBitmap(bitmap);
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }

            }
        }
    }

    private Bitmap decodeFile1(String filePath, Uri selectedImageUri) {
        String imgOrient = "0";
        int height = 0, width = 0;
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        try {
            Bitmap upBitmap = BitmapFactory.decodeFile(filePath, o);
            // height = upBitmap.getHeight();
            // width = upBitmap.getWidth();
            final int REQUIRED_SIZE = 400;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            upBitmap = BitmapFactory.decodeFile(filePath, o2);
            if (upBitmap == null) {
                Toast.makeText(Form4Activity.this,
                        "Please select valid image file!", Toast.LENGTH_LONG)
                        .show();


            }
            ExifInterface exif = null;

            try {
                exif = new ExifInterface(filePath);
                imgOrient = exif.getAttribute(ExifInterface.TAG_ORIENTATION);

            } catch (Exception e) {
                e.printStackTrace();

            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if ((width > 1500 || height > 1500) && (height > width)) {
                upBitmap = Bitmap.createScaledBitmap(upBitmap, 840, 1200, true);
                upBitmap.compress(Bitmap.CompressFormat.JPEG, 45, baos);
            } else
                upBitmap.compress(Bitmap.CompressFormat.PNG, 60, baos);
            byte[] data = baos.toByteArray();
            if (upBitmap == null) {
                Toast.makeText(Form4Activity.this,
                        "Error during image decoding.", Toast.LENGTH_LONG)
                        .show();


            }

            //  thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
            LayoutInflater inflater = LayoutInflater.from(Form4Activity.this);
            View inflatedLayout = inflater.inflate(R.layout.image_row_layout, null, false);
            ImageView imageView = inflatedLayout.findViewById(R.id.imageView);
            imageView.setImageBitmap(rotateImage(Integer.parseInt(imgOrient),
                    upBitmap));
            imageLayout.addView(inflatedLayout);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            upBitmap.compress(Bitmap.CompressFormat.JPEG, 95, stream);
            byte[] imageInByte = stream.toByteArray();
            String imageBase64String = Base64.encodeToString(imageInByte, Base64.DEFAULT);
            DVoterEntity.ImageEntity imageEntity = new DVoterEntity.ImageEntity();
            imageEntity.image = imageBase64String;
            if (mainActivity.isInternetConnected(Form4Activity.this)) {

                sendImageToBackend(imageEntity);

            }else{
                mainActivity.showToast(Form4Activity.this,getString(R.string.check_internet));
            }
        } catch (OutOfMemoryError e) {

            Toast.makeText(Form4Activity.this,
                    "Job closed as memory is low!", Toast.LENGTH_LONG).show();
        }


        return bitmap;
    }

    private Bitmap rotateImage(int orint, Bitmap bitmap) {
        Bitmap bit = bitmap;
        Bitmap result = null;
        try {
            Matrix matrix = new Matrix();

            switch (orint) {
                case 1:
                    result = bitmap;
                    break;
                case 6:
                    matrix.postRotate(90);
                    break;
                case 3:
                    matrix.postRotate(180);
                    break;
                case 8:
                    matrix.postRotate(270);
                    break;
                default:
                    break;
            }
            result = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
        } catch (Exception e) {
            return bit;
        }
        return result;
    }


    private String getRealPathFromURI(Uri contentURI) {
        String filePath = "";
        Cursor cursor = null;
        try {
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
            String[] column = {MediaStore.Images.Media.DATA};
            if (isKitKat
                    && DocumentsContract.isDocumentUri(
                    Form4Activity.this, contentURI)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(contentURI)) {
                    final String docId = DocumentsContract
                            .getDocumentId(contentURI);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        filePath = Environment.getExternalStorageDirectory()
                                + "/" + split[1];
                    } else {
                        String fileExtSDPath = System
                                .getenv("SECONDARY_STORAGE");
                        if ((null == fileExtSDPath)
                                || (fileExtSDPath.length() == 0)) {
                            fileExtSDPath = System
                                    .getenv("EXTERNAL_SDCARD_STORAGE");
                        }
                        filePath = fileExtSDPath + "/" + split[1];
                    }
                }
                // if (Build.VERSION.SDK_INT >= 19)
                else if (isDownloadsDocument(contentURI)) {
                    String wholeID = DocumentsContract
                            .getDocumentId(contentURI);
                    String[] id = wholeID.split(":");
                    final String type = id[0];
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"),
                            Long.valueOf(wholeID));
                    cursor = getContentResolver().query(contentUri, column,
                            null, null, null);
                    int columnIndex = cursor.getColumnIndex(column[0]);
                    if (cursor.moveToFirst()) {
                        filePath = cursor.getString(columnIndex);
                    }
                    cursor.close();
                } else if (isMediaDocument(contentURI)) {
                    final String docId = DocumentsContract
                            .getDocumentId(contentURI);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    String id = docId.split(":")[1];
                    String sel = MediaStore.Images.Media._ID + "=?";
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        cursor = getContentResolver().query(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                column, sel, new String[]{id}, null);
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        cursor = getContentResolver().query(contentUri, column,
                                null, null, null);
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        cursor = getContentResolver().query(contentUri, column,
                                null, null, null);
                    }
                    int columnIndex = cursor.getColumnIndex(column[0]);
                    if (cursor.moveToFirst()) {
                        filePath = cursor.getString(columnIndex);
                    }
                    cursor.close();
                }
            } else {
                cursor = getContentResolver().query(contentURI, column, null,
                        null, null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return filePath;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    private void performCrop(Uri tempUri) {
        try {
            CropImage.activity(tempUri)
                    .setAspectRatio(1, 1)
                    .start(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendImageToBackend(DVoterEntity.ImageEntity imageEntity) {
        String json = gson.toJson(imageEntity);
        try {
            Rx2AndroidNetworking.post(MainActivity.SITE_URL + "filebox")
                    .addJSONObjectBody(new JSONObject(json))
                    .addHeaders("Content-type", "application/json")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {

                                    String path = null;
                                    if (response.get("status").toString().equalsIgnoreCase("Submitted Success full")) {

                                        path = "" + response.get("path");
                                    }
                                    imageUrllist.add(path);

                                    //  startActivity(new Intent(Form3Activity.this, HomeScreenActivity.class));

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
