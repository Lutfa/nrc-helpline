package com.raylabs.nrchelpline;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.raylabs.nrchelpline.Ui.RobotoTextView;
import com.raylabs.nrchelpline.helper.DbHelper;
import com.raylabs.nrchelpline.utils.NewsResponse;
import com.google.gson.Gson;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NewsActivity extends AppCompatActivity {
    MainActivity mainActivity;
    SharedPreferencesActivity sharedPreferencesActivity;
    Gson gson;
    DbHelper dbHelper;
    RobotoTextView toolbar_title, title;
    WebView webView;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //  getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        final Drawable upArrow = getResources().getDrawable(R.drawable.vector_back_white_icon);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        mainActivity = new MainActivity();
        dbHelper = new DbHelper(NewsActivity.this);
        sharedPreferencesActivity = SharedPreferencesActivity.getInstance(NewsActivity.this);
        gson = new Gson();
        webView = (WebView) findViewById(R.id.webView);
        toolbar_title = findViewById(R.id.toolbar_title);
        image = findViewById(R.id.image);
        title = findViewById(R.id.title);
        toolbar_title.setText(getString(R.string.news_announcement));

        gson = new Gson();
        showUi(null);
        //longOperation();
    }

    private void longOperation() {

        try {
            Rx2AndroidNetworking.get(MainActivity.SITE_URL + "news")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {
                                    NewsResponse formResponse = gson.fromJson(response.toString(), NewsResponse.class);
                                    for (int i = 0; i < formResponse.data.size(); i++) {

                                        NewsResponse.NewsEntity entity = formResponse.data.get(i);
                                        showUi(entity);
                                    }


                                } catch (Exception e) {

                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void showUi(NewsResponse.NewsEntity entity) {
       // title.setText(entity.title);

      //  webView.loadDataWithBaseURL(null, entity.description, "text/html", "utf-8", null);
       // webView.loadDataWithBaseURL(null, "https://cjp.org.in/assam/", "text/html", "utf-8", null);
       // webView.loadUrl("https://cjp.org.in/assam/");
       /* if (!StringUtils.isBlank(entity.image)) {
            Picasso.with(NewsActivity.this)
                    .load(entity.image)
                    .error(R.drawable.image2)

                    .into(image);
        }*/

   final   ProgressDialog  progressBar = new ProgressDialog(NewsActivity.this);
        progressBar.setTitle("Loading news...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressBar.show();
        progressBar.setCancelable(false);


        webView.getSettings().setJavaScriptEnabled(true); // enable javascript

        final Activity activity = this;

      /*  webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.cancel();
            }
        });

        webView .loadUrl("https://cjp.org.in/Assam/");*/
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setUserAgentString("Android WebView");
        webView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progressBar.show();
                view.loadUrl(url);

                return true;
            }
            @Override
            public void onPageFinished(WebView view, final String url) {
                progressBar.dismiss();
            }
        });

        webView.loadUrl("https://cjp.org.in/");




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(NewsActivity.this,HomeScreenActivity.class));
        finish();
    }
}
