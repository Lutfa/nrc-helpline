package com.raylabs.nrchelpline;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.raylabs.nrchelpline.Ui.RobotoTextView;

public class ContactUsActivity extends AppCompatActivity {
RobotoTextView toolbar_title;
    public static String[] PERMISSIONS_PHONESTATE = {"android.permission.READ_PHONE_STATE"};
    public static final int PERMISSIONS_PHONESTATE_REQUEST_CODE = 5;
    private AlertDialog alert11;
    RelativeLayout call_layout;
    RobotoTextView tv_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //  getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        final Drawable upArrow = getResources().getDrawable(R.drawable.vector_back_white_icon);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar_title = findViewById(R.id.toolbar_title);
        call_layout = findViewById(R.id.call_layout);
        tv_email = findViewById(R.id.tv_email);
        toolbar_title.setText(getString(R.string.contact));

        call_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_callDialog();
            }
        });
        tv_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "cjphelpline@gmail.com"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "your_subject");
                    startActivity(intent);
                }catch(ActivityNotFoundException e){
                    //TODO smth
                }
            }
        });


    }
    public void show_callDialog() {


        AlertDialog.Builder builder1 = new AlertDialog.Builder(ContactUsActivity.this);
        builder1.setMessage(getResources().getString(R.string.callcustomercare));

        builder1.setPositiveButton(getResources().getString(R.string.call), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(ContactUsActivity.this, Manifest.permission.READ_PHONE_STATE)
                        != PackageManager.PERMISSION_GRANTED) {

                    requestPhonStatePermission();
                } else {


                    Intent callIntent = new Intent(Intent.ACTION_CALL);

                    callIntent.setData(Uri.parse("tel:1800 300 250 78"));
                    if (ActivityCompat.checkSelfPermission(ContactUsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }


                alert11.dismiss();

            }
        });
        builder1.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alert11.dismiss();

            }
        });
        alert11 = builder1.create();
        alert11.show();
        alert11.getButton(alert11.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.AppThemeColor));
        alert11.getButton(alert11.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AppThemeColor));




    }
    private void requestPhonStatePermission() {
        try {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_PHONE_STATE)) {
                //	Toast.makeText(IntroductionScreen.this,getResources().getString(R.string.permissionStatement), Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this, PERMISSIONS_PHONESTATE, PERMISSIONS_PHONESTATE_REQUEST_CODE);
            } else {
                // Contact permissions have not been granted yet. Request them directly.
                ActivityCompat.requestPermissions(this, PERMISSIONS_PHONESTATE, PERMISSIONS_PHONESTATE_REQUEST_CODE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ContactUsActivity.this,HomeScreenActivity.class));

        finish();
    }
}
