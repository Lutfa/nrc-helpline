package com.raylabs.nrchelpline.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.raylabs.nrchelpline.utils.DVoterEntity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lutfa on 15-09-2018.
 */

public class DbHelper extends DbHandler {
    public DbHelper(Context context) {
        super(context);
    }

    public void addUpdateFormTable(DVoterEntity dVoterEntity) {
        SQLiteDatabase db = super.getWritableDatabase();
        Cursor cursor = null;
        Gson gson = new Gson();
        try {
            db.beginTransaction();
            ContentValues values = new ContentValues();

            if (!StringUtils.isBlank(dVoterEntity.id)) {
                values.put(KEY_FORM_ID, dVoterEntity.id);
            }
            if (!StringUtils.isBlank(dVoterEntity.submission_id_gen)) {
                values.put(KEY_FORM_NUMBER, dVoterEntity.submission_id_gen);
            }

            if (!StringUtils.isBlank(dVoterEntity.form_type)) {
                values.put(KEY_FORM_TYPE, dVoterEntity.form_type);
            }
            if (!StringUtils.isBlank(dVoterEntity.name)) {
                values.put(KEY_FORM_APPLICANT_NAME, dVoterEntity.name);
            }
            if (!StringUtils.isBlank(dVoterEntity.so)) {
                values.put(KEY_SON_OF, dVoterEntity.so);
            }
            if (!StringUtils.isBlank(dVoterEntity.occupation)) {
                values.put(KEY_OCCUPATION, dVoterEntity.occupation);
            }
            if (!StringUtils.isBlank(dVoterEntity.temporary_address)) {
                values.put(KEY_TEMP_ADDRESS, dVoterEntity.temporary_address);
            }
            if (!StringUtils.isBlank(dVoterEntity.permanent_address)) {
                values.put(KEY_PERM_ADDRESS, dVoterEntity.permanent_address);
            }
            if (!StringUtils.isBlank(dVoterEntity.family_tree_details)) {
                values.put(KEY_FAMILY_TREE_DETAILS, dVoterEntity.family_tree_details);
            }
            if (!StringUtils.isBlank(dVoterEntity.cell_no)) {
                values.put(KEY_CELL_NO, dVoterEntity.cell_no);
            }
            if (!StringUtils.isBlank(dVoterEntity.case_no)) {
                values.put(KEY_CASE_NO, dVoterEntity.case_no);
            }
            if (!StringUtils.isBlank(dVoterEntity.case_status)) {
                values.put(KEY_CASE_STATUS, dVoterEntity.case_status);
            }
            if (!StringUtils.isBlank(dVoterEntity.disposed_case)) {
                values.put(KEY_DISPOSED_CASE, dVoterEntity.disposed_case);
            }
            if (!StringUtils.isBlank(dVoterEntity.advocate_details)) {
                values.put(KEY_ADVOCATE_DETAILS, dVoterEntity.advocate_details);
            }


            if (!StringUtils.isBlank(dVoterEntity.arn_no)) {
                values.put(KEY_ARN_NO, dVoterEntity.arn_no);
            }
            if (!StringUtils.isBlank(dVoterEntity.no)) {
                values.put(KEY_NO, dVoterEntity.no);
            }
            if (!StringUtils.isBlank(dVoterEntity.age)) {
                values.put(KEY_AGE, dVoterEntity.age);
            }
            if (!StringUtils.isBlank(dVoterEntity.nsk_no)) {
                values.put(KEY_NSK_NO, dVoterEntity.nsk_no);
            }
            if (!StringUtils.isBlank(dVoterEntity.date_detail)) {
                values.put(KEY_DATE_DETAILS, dVoterEntity.date_detail);
            }
            if (!StringUtils.isBlank(dVoterEntity.why_the_application_rejected)) {
                values.put(KEY_WHY_APPLICATION_REJECTED, dVoterEntity.why_the_application_rejected);
            }
            if (!StringUtils.isBlank(dVoterEntity.reason_1)) {
                values.put(KEY_REASON_1, dVoterEntity.reason_1);
            }
            if (!StringUtils.isBlank(dVoterEntity.reason_2)) {
                values.put(KEY_REASON_2, dVoterEntity.reason_2);
            }
            if (!StringUtils.isBlank(dVoterEntity.reason_3)) {
                values.put(KEY_REASON_3, dVoterEntity.reason_3);
            }
            if (!StringUtils.isBlank(dVoterEntity.request_for)) {
                values.put(KEY_REQUEST_FOR, dVoterEntity.request_for);
            }


            if (!StringUtils.isBlank(dVoterEntity.contact_no)) {
                values.put(KEY_CONTACT_NO, dVoterEntity.contact_no);
            }
            if (!StringUtils.isBlank(dVoterEntity.arrested_on)) {
                values.put(KEY_ARRESTED_ON, dVoterEntity.arrested_on);
            }
            if (!StringUtils.isBlank(dVoterEntity.name_of_the_dc)) {
                values.put(KEY_NAME_OF_THE_DC, dVoterEntity.name_of_the_dc);
            }
            if (!StringUtils.isBlank(dVoterEntity.name_of_the_advocate)) {
                values.put(KEY_NAME_OF_THE_ADVOCATE, dVoterEntity.name_of_the_advocate);
            }

            if (!StringUtils.isBlank(dVoterEntity.image)) {
                values.put(KEY_IMAGE, dVoterEntity.image);
            }
            if (!StringUtils.isBlank(dVoterEntity.reason)) {
                values.put(KEY_REASON, dVoterEntity.reason);
            }
            if (!StringUtils.isBlank(dVoterEntity.ft_no)) {
                values.put(KEY_FT_NO, dVoterEntity.ft_no);
            }
            if (!StringUtils.isBlank(dVoterEntity.main_no)) {
                values.put(KEY_MAIN_NO, dVoterEntity.main_no);
            }
            if (!StringUtils.isBlank(dVoterEntity.district)) {
                values.put(KEY_DISTRICT, dVoterEntity.district);
            }
            if (!StringUtils.isBlank(dVoterEntity.petioner)) {
                values.put(KEY_PETIONER, dVoterEntity.petioner);
            }
            if (!StringUtils.isBlank(dVoterEntity.petioner_advocate)) {
                values.put(KEY_PETIONER_ADVOCATE, dVoterEntity.petioner_advocate);
            }
            if (!StringUtils.isBlank(dVoterEntity.case_category)) {
                values.put(KEY_CASE_CATEGORY, dVoterEntity.case_category);
            }
            if (!StringUtils.isBlank(dVoterEntity.respondent)) {
                values.put(KEY_RESPONDENT, dVoterEntity.respondent);
            }
            if (!StringUtils.isBlank(dVoterEntity.filing_date)) {
                values.put(KEY_FILING_DATE, dVoterEntity.filing_date);
            }


            if (!StringUtils.isBlank(dVoterEntity.listing_date)) {
                values.put(KEY_LISTING_DATE, dVoterEntity.listing_date);
            }
            if (!StringUtils.isBlank(dVoterEntity.purpose)) {
                values.put(KEY_PURPOSE, dVoterEntity.purpose);
            }


            if (!StringUtils.isBlank(dVoterEntity.honorable_judge)) {
                values.put(KEY_HONORABLE_JUDGE, dVoterEntity.honorable_judge);
            }
            if (!StringUtils.isBlank(dVoterEntity.category)) {
                values.put(KEY_CATEGORY, dVoterEntity.category);
            }

            if (!StringUtils.isBlank(dVoterEntity.prayer)) {
                values.put(KEY_PRAYER, dVoterEntity.prayer);
            }
            if (!StringUtils.isBlank(dVoterEntity.petentioners_1)) {
                values.put(KEY_PETENTIONERS1, dVoterEntity.petentioners_1);
            }
            if (!StringUtils.isBlank(dVoterEntity.petentioners_2)) {
                values.put(KEY_PETENTIONERS2, dVoterEntity.petentioners_2);
            }
            if (!StringUtils.isBlank(dVoterEntity.petentioners_3)) {
                values.put(KEY_PETENTIONERS3, dVoterEntity.petentioners_3);
            }
            if (!StringUtils.isBlank(dVoterEntity.petentioners_4)) {
                values.put(KEY_PETENTIONERS4, dVoterEntity.petentioners_4);
            }
            if (!StringUtils.isBlank(dVoterEntity.petentioners_5)) {
                values.put(KEY_PETENTIONERS5, dVoterEntity.petentioners_5);
            }


            if (!StringUtils.isBlank(dVoterEntity.email)) {
                values.put(KEY_EMAIL, dVoterEntity.email);
            }
            if (!StringUtils.isBlank(dVoterEntity.date)) {
                values.put(KEY_DATE, dVoterEntity.date);
            }
            if (!StringUtils.isBlank(dVoterEntity.area_visited)) {
                values.put(KEY_AREA_VISITED, dVoterEntity.area_visited);
            }
            if (!StringUtils.isBlank(dVoterEntity.exclusion_parcentage)) {
                values.put(KEY_EXCLUSION_PERCENTAGE, dVoterEntity.exclusion_parcentage);
            }
            if (!StringUtils.isBlank(dVoterEntity.distance)) {
                values.put(KEY_DISTANCE_FROM_HOME, dVoterEntity.distance);
            }
            if (!StringUtils.isBlank(dVoterEntity.count_people)) {
                values.put(KEY_NUMBER_PEOPLE, dVoterEntity.count_people);
            }
            if (!StringUtils.isBlank(dVoterEntity.comments)) {
                values.put(KEY_GENERAL_REMARK, dVoterEntity.comments);
            }
            if (!StringUtils.isBlank(dVoterEntity.location)) {
                values.put(KEY_LOCATION, dVoterEntity.location);
            }

            if (!StringUtils.isBlank(dVoterEntity.rejection_reason)) {
                values.put(KEY_REASON_FOR_REJECTION, dVoterEntity.rejection_reason);
            }
            if (!StringUtils.isBlank(dVoterEntity.time)) {
                values.put(KEY_TIME, dVoterEntity.time);
            }
            if (!StringUtils.isBlank(dVoterEntity.document_1)) {
                values.put(KEY_LINKAGE_DOC, dVoterEntity.document_1);
            }
            if (!StringUtils.isBlank(dVoterEntity.document_2)) {
                values.put(KEY_LEGACY_DOC, dVoterEntity.document_2);
            }
            if (!StringUtils.isBlank(dVoterEntity.problem)) {
                values.put(KEY_PROBLEM, dVoterEntity.problem);
            }
            if (!StringUtils.isBlank(dVoterEntity.name_of_caller)) {
                values.put(KEY_NAME_OF_CALLER, dVoterEntity.name_of_caller);
            }

            String members = gson.toJson(dVoterEntity.members);
            if (!StringUtils.isBlank(members) && !members.equalsIgnoreCase("null") && !members.equalsIgnoreCase("[]")) {
                values.put(KEY_MEMBER, members);
            }
            String doc = gson.toJson(dVoterEntity.docs);
            if (!StringUtils.isBlank(doc) && !doc.equalsIgnoreCase("null") && !doc.equalsIgnoreCase("[]")) {
                values.put(KEY_DOCUMENT, doc);
            }
            String query = SELECT_ALL + TABLE_FORM + " WHERE " + KEY_FORM_ID + " = '" + dVoterEntity.id + "' AND " + KEY_FORM_TYPE + " = '" + dVoterEntity.form_type + "' ";

            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst())
                db.update(TABLE_FORM, values, KEY_FORM_ID + " = '"
                        + dVoterEntity.id + "' AND " + KEY_FORM_TYPE + " = '" + dVoterEntity.form_type + "' ", null);
            else
                db.insertOrThrow(TABLE_FORM, null, values);

            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.endTransaction();
        db.close();
    }


    public List<DVoterEntity> getAllForms() {
        List<DVoterEntity> formList = new ArrayList<DVoterEntity>();
        SQLiteDatabase db = super.getWritableDatabase();
        Cursor c = null;
        try {
            // String query = "Select * from TableForm";
            String query = SELECT_ALL + TABLE_FORM;
            c = db.rawQuery(query, null);
            c.getCount();

            if (c.moveToFirst()) {
                while (c.isAfterLast() == false) {
                    DVoterEntity dVoterEntity = new DVoterEntity();
                    dVoterEntity.submission_id_gen = c.getString(c
                            .getColumnIndex(KEY_FORM_NUMBER));
                    dVoterEntity.id = c.getString(c
                            .getColumnIndex(KEY_FORM_ID));
                    dVoterEntity.form_type = c.getString(c
                            .getColumnIndex(KEY_FORM_TYPE));
                    dVoterEntity.name = c.getString(c
                            .getColumnIndex(KEY_FORM_APPLICANT_NAME));
                    dVoterEntity.so = c.getString(c
                            .getColumnIndex(KEY_SON_OF));
                    dVoterEntity.occupation = c.getString(c
                            .getColumnIndex(KEY_OCCUPATION));
                    dVoterEntity.temporary_address = c.getString(c
                            .getColumnIndex(KEY_TEMP_ADDRESS));

                    dVoterEntity.permanent_address = c.getString(c
                            .getColumnIndex(KEY_PERM_ADDRESS));
                    dVoterEntity.family_tree_details = c.getString(c
                            .getColumnIndex(KEY_FAMILY_TREE_DETAILS));
                    dVoterEntity.cell_no = c.getString(c
                            .getColumnIndex(KEY_CELL_NO));
                    dVoterEntity.case_no = c.getString(c
                            .getColumnIndex(KEY_CASE_NO));
                    dVoterEntity.case_status = c.getString(c
                            .getColumnIndex(KEY_CASE_STATUS));
                    dVoterEntity.disposed_case = c.getString(c
                            .getColumnIndex(KEY_DISPOSED_CASE));

                    dVoterEntity.advocate_details = c.getString(c
                            .getColumnIndex(KEY_ADVOCATE_DETAILS));
                    dVoterEntity.arn_no = c.getString(c
                            .getColumnIndex(KEY_ARN_NO));
                    dVoterEntity.no = c.getString(c
                            .getColumnIndex(KEY_NO));
                    dVoterEntity.age = c.getString(c
                            .getColumnIndex(KEY_AGE));
                    dVoterEntity.nsk_no = c.getString(c
                            .getColumnIndex(KEY_NSK_NO));
                    dVoterEntity.date_detail = c.getString(c
                            .getColumnIndex(KEY_DATE_DETAILS));


                    dVoterEntity.why_the_application_rejected = c.getString(c
                            .getColumnIndex(KEY_WHY_APPLICATION_REJECTED));
                    dVoterEntity.reason_1 = c.getString(c
                            .getColumnIndex(KEY_REASON_1));
                    dVoterEntity.reason_2 = c.getString(c
                            .getColumnIndex(KEY_REASON_2));
                    dVoterEntity.reason_3 = c.getString(c
                            .getColumnIndex(KEY_REASON_3));
                    dVoterEntity.request_for = c.getString(c
                            .getColumnIndex(KEY_REQUEST_FOR));

                    dVoterEntity.contact_no = c.getString(c
                            .getColumnIndex(KEY_CONTACT_NO));
                    dVoterEntity.arrested_on = c.getString(c
                            .getColumnIndex(KEY_ARRESTED_ON));
                    dVoterEntity.name_of_the_dc = c.getString(c
                            .getColumnIndex(KEY_NAME_OF_THE_DC));
                    dVoterEntity.name_of_the_advocate = c.getString(c
                            .getColumnIndex(KEY_NAME_OF_THE_ADVOCATE));


                    dVoterEntity.image = c.getString(c
                            .getColumnIndex(KEY_IMAGE));
                    dVoterEntity.reason = c.getString(c
                            .getColumnIndex(KEY_REASON));
                    dVoterEntity.ft_no = c.getString(c
                            .getColumnIndex(KEY_FT_NO));
                    dVoterEntity.main_no = c.getString(c
                            .getColumnIndex(KEY_MAIN_NO));
                    dVoterEntity.district = c.getString(c
                            .getColumnIndex(KEY_DISTRICT));
                    dVoterEntity.petioner = c.getString(c
                            .getColumnIndex(KEY_PETIONER));


                    dVoterEntity.petioner_advocate = c.getString(c
                            .getColumnIndex(KEY_PETIONER_ADVOCATE));
                    dVoterEntity.case_category = c.getString(c
                            .getColumnIndex(KEY_CASE_CATEGORY));
                    dVoterEntity.respondent = c.getString(c
                            .getColumnIndex(KEY_RESPONDENT));
                    dVoterEntity.filing_date = c.getString(c
                            .getColumnIndex(KEY_FILING_DATE));
                    dVoterEntity.listing_date = c.getString(c
                            .getColumnIndex(KEY_LISTING_DATE));

                    dVoterEntity.purpose = c.getString(c
                            .getColumnIndex(KEY_PURPOSE));

                    dVoterEntity.honorable_judge = c.getString(c
                            .getColumnIndex(KEY_HONORABLE_JUDGE));
                    dVoterEntity.category = c.getString(c
                            .getColumnIndex(KEY_CATEGORY));
                    dVoterEntity.prayer = c.getString(c
                            .getColumnIndex(KEY_PRAYER));

                    dVoterEntity.petentioners_1 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS1));
                    dVoterEntity.petentioners_2 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS2));
                    dVoterEntity.petentioners_3 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS3));
                    dVoterEntity.petentioners_4 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS4));
                    dVoterEntity.petentioners_5 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS5));
                    dVoterEntity.document = c.getString(c
                            .getColumnIndex(KEY_DOCUMENT));
                    String members = c.getString(c
                            .getColumnIndex(KEY_MEMBER));
                    Gson gson = new Gson();
                    if (!StringUtils.isBlank(members)) {

                        Type type = new TypeToken<ArrayList<DVoterEntity.MemberEntity>>() {
                        }.getType();

                        List<DVoterEntity.MemberEntity> memberlist = gson.fromJson(members, type);
                        dVoterEntity.members = memberlist;

                    }
                    if (!StringUtils.isBlank(dVoterEntity.document)) {

                        Type type = new TypeToken<List<String>>() {
                        }.getType();

                        ArrayList<String> doclist = gson.fromJson(dVoterEntity.document, type);
                        dVoterEntity.docs = doclist;

                    }

                    formList.add(dVoterEntity);
                    c.moveToNext();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        c.close();
        db.close();
        return formList;
    }

    public DVoterEntity getFormDetails(String formId, String formType) {
        DVoterEntity dVoterEntity = new DVoterEntity();
        SQLiteDatabase db = super.getWritableDatabase();
        Cursor c = null;
        try {
            // String query = "Select * from TableForm";
            String query = SELECT_ALL + TABLE_FORM + " WHERE " + KEY_FORM_ID + " = '" + formId + "' AND " + KEY_FORM_TYPE + " = '" + formType + "' ";
            c = db.rawQuery(query, null);
            c.getCount();

            if (c.moveToFirst()) {
                while (c.isAfterLast() == false) {
                    dVoterEntity.id = c.getString(c
                            .getColumnIndex(KEY_FORM_ID));
                    dVoterEntity.submission_id_gen = c.getString(c
                            .getColumnIndex(KEY_FORM_NUMBER));
                    dVoterEntity.form_type = c.getString(c
                            .getColumnIndex(KEY_FORM_TYPE));
                    dVoterEntity.name = c.getString(c
                            .getColumnIndex(KEY_FORM_APPLICANT_NAME));
                    dVoterEntity.so = c.getString(c
                            .getColumnIndex(KEY_SON_OF));
                    dVoterEntity.occupation = c.getString(c
                            .getColumnIndex(KEY_OCCUPATION));
                    dVoterEntity.temporary_address = c.getString(c
                            .getColumnIndex(KEY_TEMP_ADDRESS));

                    dVoterEntity.permanent_address = c.getString(c
                            .getColumnIndex(KEY_PERM_ADDRESS));
                    dVoterEntity.family_tree_details = c.getString(c
                            .getColumnIndex(KEY_FAMILY_TREE_DETAILS));
                    dVoterEntity.cell_no = c.getString(c
                            .getColumnIndex(KEY_CELL_NO));
                    dVoterEntity.case_no = c.getString(c
                            .getColumnIndex(KEY_CASE_NO));
                    dVoterEntity.case_status = c.getString(c
                            .getColumnIndex(KEY_CASE_STATUS));
                    dVoterEntity.disposed_case = c.getString(c
                            .getColumnIndex(KEY_DISPOSED_CASE));

                    dVoterEntity.advocate_details = c.getString(c
                            .getColumnIndex(KEY_ADVOCATE_DETAILS));
                    dVoterEntity.arn_no = c.getString(c
                            .getColumnIndex(KEY_ARN_NO));
                    dVoterEntity.no = c.getString(c
                            .getColumnIndex(KEY_NO));
                    dVoterEntity.age = c.getString(c
                            .getColumnIndex(KEY_AGE));
                    dVoterEntity.nsk_no = c.getString(c
                            .getColumnIndex(KEY_NSK_NO));
                    dVoterEntity.date_detail = c.getString(c
                            .getColumnIndex(KEY_DATE_DETAILS));


                    dVoterEntity.why_the_application_rejected = c.getString(c
                            .getColumnIndex(KEY_WHY_APPLICATION_REJECTED));
                    dVoterEntity.reason_1 = c.getString(c
                            .getColumnIndex(KEY_REASON_1));
                    dVoterEntity.reason_2 = c.getString(c
                            .getColumnIndex(KEY_REASON_2));
                    dVoterEntity.reason_3 = c.getString(c
                            .getColumnIndex(KEY_REASON_3));
                    dVoterEntity.request_for = c.getString(c
                            .getColumnIndex(KEY_REQUEST_FOR));

                    dVoterEntity.contact_no = c.getString(c
                            .getColumnIndex(KEY_CONTACT_NO));
                    dVoterEntity.arrested_on = c.getString(c
                            .getColumnIndex(KEY_ARRESTED_ON));
                    dVoterEntity.name_of_the_dc = c.getString(c
                            .getColumnIndex(KEY_NAME_OF_THE_DC));
                    dVoterEntity.name_of_the_advocate = c.getString(c
                            .getColumnIndex(KEY_NAME_OF_THE_ADVOCATE));


                    dVoterEntity.image = c.getString(c
                            .getColumnIndex(KEY_IMAGE));
                    dVoterEntity.reason = c.getString(c
                            .getColumnIndex(KEY_REASON));
                    dVoterEntity.ft_no = c.getString(c
                            .getColumnIndex(KEY_FT_NO));
                    dVoterEntity.main_no = c.getString(c
                            .getColumnIndex(KEY_MAIN_NO));
                    dVoterEntity.district = c.getString(c
                            .getColumnIndex(KEY_DISTRICT));
                    dVoterEntity.petioner = c.getString(c
                            .getColumnIndex(KEY_PETIONER));


                    dVoterEntity.petioner_advocate = c.getString(c
                            .getColumnIndex(KEY_PETIONER_ADVOCATE));
                    dVoterEntity.case_category = c.getString(c
                            .getColumnIndex(KEY_CASE_CATEGORY));
                    dVoterEntity.respondent = c.getString(c
                            .getColumnIndex(KEY_RESPONDENT));
                    dVoterEntity.filing_date = c.getString(c
                            .getColumnIndex(KEY_FILING_DATE));
                    dVoterEntity.listing_date = c.getString(c
                            .getColumnIndex(KEY_LISTING_DATE));

                    dVoterEntity.purpose = c.getString(c
                            .getColumnIndex(KEY_PURPOSE));

                    dVoterEntity.honorable_judge = c.getString(c
                            .getColumnIndex(KEY_HONORABLE_JUDGE));
                    dVoterEntity.category = c.getString(c
                            .getColumnIndex(KEY_CATEGORY));
                    dVoterEntity.prayer = c.getString(c
                            .getColumnIndex(KEY_PRAYER));

                    dVoterEntity.petentioners_1 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS1));
                    dVoterEntity.petentioners_2 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS2));
                    dVoterEntity.petentioners_3 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS3));
                    dVoterEntity.petentioners_4 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS4));
                    dVoterEntity.petentioners_5 = c.getString(c
                            .getColumnIndex(KEY_PETENTIONERS5));
                    dVoterEntity.document = c.getString(c
                            .getColumnIndex(KEY_DOCUMENT));


                    dVoterEntity.email = c.getString(c
                            .getColumnIndex(KEY_EMAIL));
                    dVoterEntity.date = c.getString(c
                            .getColumnIndex(KEY_DATE));
                    dVoterEntity.area_visited = c.getString(c
                            .getColumnIndex(KEY_AREA_VISITED));
                    dVoterEntity.exclusion_parcentage = c.getString(c
                            .getColumnIndex(KEY_EXCLUSION_PERCENTAGE));

                    dVoterEntity.distance = c.getString(c
                            .getColumnIndex(KEY_DISTANCE_FROM_HOME));
                    dVoterEntity.count_people = c.getString(c
                            .getColumnIndex(KEY_NUMBER_PEOPLE));
                    dVoterEntity.comments = c.getString(c
                            .getColumnIndex(KEY_GENERAL_REMARK));
                    dVoterEntity.location = c.getString(c
                            .getColumnIndex(KEY_LOCATION));


                    dVoterEntity.rejection_reason = c.getString(c
                            .getColumnIndex(KEY_REASON_FOR_REJECTION));
                    dVoterEntity.time = c.getString(c
                            .getColumnIndex(KEY_TIME));
                    dVoterEntity.document_1 = c.getString(c
                            .getColumnIndex(KEY_LINKAGE_DOC));
                    dVoterEntity.document_2 = c.getString(c
                            .getColumnIndex(KEY_LEGACY_DOC));
                    dVoterEntity.problem = c.getString(c
                            .getColumnIndex(KEY_PROBLEM));
                    dVoterEntity.name_of_caller = c.getString(c
                            .getColumnIndex(KEY_NAME_OF_CALLER));

                    String members = c.getString(c
                            .getColumnIndex(KEY_MEMBER));
                    Gson gson = new Gson();
                    if (!StringUtils.isBlank(members)) {

                        Type type = new TypeToken<ArrayList<DVoterEntity.MemberEntity>>() {
                        }.getType();

                        List<DVoterEntity.MemberEntity> memberlist = gson.fromJson(members, type);
                        dVoterEntity.members = memberlist;

                    }
                    if (!StringUtils.isBlank(dVoterEntity.document)) {

                        Type type = new TypeToken<List<String>>() {
                        }.getType();

                        ArrayList<String> doclist = gson.fromJson(dVoterEntity.document, type);
                        dVoterEntity.docs = doclist;

                    }


                    c.moveToNext();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        c.close();
        db.close();
        return dVoterEntity;
    }
}
