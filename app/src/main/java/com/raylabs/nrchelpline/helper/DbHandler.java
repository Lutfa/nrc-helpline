package com.raylabs.nrchelpline.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lutfa on 15-09-2018.
 */

public class DbHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "AmcrDB";
    protected static Context con;
    private static String TAG = "AmcrDBHandler";
    ////query
    public static final String SELECT_ALL = "select * from ";

    // Table name
    public static final String TABLE_FORM = "TableForm";

    /// column name
    protected static final String KEY_FORM_ID = "id";
    protected static final String KEY_FORM_NUMBER = "form_number";
    protected static final String KEY_FORM_TYPE = "form_type";
    protected static final String KEY_FORM_APPLICANT_NAME = "applicant_name";
    protected static final String KEY_SON_OF = "so";
    protected static final String KEY_OCCUPATION = "occupation";
    protected static final String KEY_TEMP_ADDRESS = "temporary_address";
    protected static final String KEY_PERM_ADDRESS = "permanent_address";
    protected static final String KEY_FAMILY_TREE_DETAILS = "family_tree_details";
    protected static final String KEY_CELL_NO = "cell_no";
    protected static final String KEY_CASE_NO = "case_no";
    protected static final String KEY_CASE_STATUS = "case_status";
    protected static final String KEY_DISPOSED_CASE = "disposed_case";
    protected static final String KEY_ADVOCATE_DETAILS = "advocate_details";
    protected static final String KEY_ARN_NO = "arn_no";
    protected static final String KEY_NO = "no";
    protected static final String KEY_AGE = "age";
    protected static final String KEY_NSK_NO = "nsk_no";
    protected static final String KEY_DATE_DETAILS = "date_detail";
    protected static final String KEY_WHY_APPLICATION_REJECTED = "why_the_application_rejected";
    protected static final String KEY_REASON_1 = "reason_1";
    protected static final String KEY_REASON_2 = "reason_2";
    protected static final String KEY_REASON_3 = "reason_3";
    protected static final String KEY_REQUEST_FOR = "request_for";
    protected static final String KEY_CONTACT_NO = "contact_no";
    protected static final String KEY_ARRESTED_ON = "arrested_on";
    protected static final String KEY_NAME_OF_THE_DC = "name_of_the_dc";
    protected static final String KEY_NAME_OF_THE_ADVOCATE = "name_of_the_advocate";
    protected static final String KEY_IMAGE = "image";


    protected static final String KEY_REASON = "reason";
    protected static final String KEY_FT_NO = "ft_no";
    protected static final String KEY_MAIN_NO = "main_no";
    protected static final String KEY_DISTRICT = "district";
    protected static final String KEY_PETIONER = "petioner";
    protected static final String KEY_PETIONER_ADVOCATE = "petioner_advocate";
    protected static final String KEY_CASE_CATEGORY = "case_category";
    protected static final String KEY_RESPONDENT = "respondent";
    protected static final String KEY_FILING_DATE = "filing_date";
    protected static final String KEY_LISTING_DATE = "listing_date";
    protected static final String KEY_PURPOSE = "purpose";
    protected static final String KEY_HONORABLE_JUDGE = "honorable_judge";
    protected static final String KEY_CATEGORY = "category";
    protected static final String KEY_PRAYER = "prayer";
    protected static final String KEY_PETENTIONERS1 = "petentioners_1";
    protected static final String KEY_PETENTIONERS2 = "petentioners_2";
    protected static final String KEY_PETENTIONERS3 = "petentioners_3";
    protected static final String KEY_PETENTIONERS4 = "petentioners_4";
    protected static final String KEY_PETENTIONERS5 = "petentioners_5";
    protected static final String KEY_DOCUMENT = "document";
    protected static final String KEY_MEMBER = "members";
    protected static final String KEY_OPTION1 = "option1";
    protected static final String KEY_OPTION2 = "option2";

    //daily report

    protected static final String KEY_EMAIL= "email";
    protected static final String KEY_DATE= "date";
    protected static final String KEY_AREA_VISITED= "area_visited";
    protected static final String KEY_EXCLUSION_PERCENTAGE= "exclusion_parcentage";
    protected static final String KEY_DISTANCE_FROM_HOME= "distance_from_home";
    protected static final String KEY_NUMBER_PEOPLE= "number_of_people";
    protected static final String KEY_GENERAL_REMARK= "general_remark";

////Log Sheet



    protected static final String KEY_LOCATION = "si_no";
    protected static final String KEY_REASON_FOR_REJECTION= "reason_for_rejection";
    protected static final String KEY_TIME= "time";
    protected static final String KEY_LINKAGE_DOC= "linkage_doc";
    protected static final String KEY_LEGACY_DOC= "legacy_doc";
    protected static final String KEY_PROBLEM= "what_is_problem";
    protected static final String KEY_NAME_OF_CALLER= "name_of_caller";

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createFormTable());
    }

    private String createFormTable() {
        return "CREATE TABLE "
                + TABLE_FORM + "(" + KEY_FORM_ID
                + " TEXT ,"
                + KEY_FORM_TYPE + " TEXT , "
                + KEY_FORM_NUMBER + " TEXT , "
                + KEY_FORM_APPLICANT_NAME + " TEXT , "
                + KEY_SON_OF + " TEXT , "
                + KEY_OCCUPATION + " TEXT , "
                + KEY_TEMP_ADDRESS + " TEXT , "
                + KEY_PERM_ADDRESS + " TEXT , "
                + KEY_FAMILY_TREE_DETAILS + " TEXT , "
                + KEY_CELL_NO + " TEXT , "
                + KEY_CASE_NO + " TEXT , "
                + KEY_CASE_STATUS + " TEXT , "
                + KEY_DISPOSED_CASE + " TEXT , "
                + KEY_ARN_NO + " TEXT , "
                + KEY_NO + " TEXT , "
                + KEY_AGE + " TEXT , "
                + KEY_NSK_NO + " TEXT , "
                + KEY_DATE_DETAILS + " TEXT , "
                + KEY_WHY_APPLICATION_REJECTED + " TEXT , "
                + KEY_REASON_1 + " TEXT , "
                + KEY_REASON_2 + " TEXT , "
                + KEY_REASON_3 + " TEXT , "
                + KEY_REQUEST_FOR + " TEXT , "
                + KEY_CONTACT_NO + " TEXT , "
                + KEY_ARRESTED_ON + " TEXT , "
                + KEY_NAME_OF_THE_DC + " TEXT , "
                + KEY_NAME_OF_THE_ADVOCATE + " TEXT , "
                + KEY_IMAGE + " TEXT , "
                + KEY_REASON + " TEXT , "
                + KEY_FT_NO + " TEXT , "
                + KEY_MAIN_NO + " TEXT , "
                + KEY_DISTRICT + " TEXT , "
                + KEY_PETIONER + " TEXT , "
                + KEY_PETIONER_ADVOCATE + " TEXT , "
                + KEY_CASE_CATEGORY + " TEXT , "
                + KEY_RESPONDENT + " TEXT , "
                + KEY_FILING_DATE + " TEXT , "
                + KEY_LISTING_DATE + " TEXT , "
                + KEY_PURPOSE + " TEXT , "
                + KEY_HONORABLE_JUDGE + " TEXT , "
                + KEY_CATEGORY + " TEXT , " + KEY_PRAYER + " TEXT , "
                + KEY_PETENTIONERS1 + " TEXT , "
                + KEY_PETENTIONERS2 + " TEXT , "
                + KEY_PETENTIONERS3 + " TEXT , "
                + KEY_PETENTIONERS4 + " TEXT , "
                + KEY_PETENTIONERS5 + " TEXT , "
                + KEY_DOCUMENT + " TEXT , "
                + KEY_MEMBER + " TEXT , "
                + KEY_OPTION1 + " TEXT , "
                + KEY_OPTION2 + " TEXT , "
                + KEY_EMAIL + " TEXT , "
                + KEY_DATE + " TEXT , "
                + KEY_AREA_VISITED + " TEXT , "
                + KEY_EXCLUSION_PERCENTAGE + " TEXT , "
                + KEY_DISTANCE_FROM_HOME + " TEXT , "
                + KEY_NUMBER_PEOPLE + " TEXT , "
                + KEY_GENERAL_REMARK + " TEXT , "
                + KEY_LOCATION + " TEXT , "
                + KEY_REASON_FOR_REJECTION + " TEXT , "
                + KEY_TIME + " TEXT , "
                + KEY_LINKAGE_DOC + " TEXT , "
                + KEY_LEGACY_DOC + " TEXT , "
                + KEY_PROBLEM + " TEXT , "
                + KEY_NAME_OF_CALLER + " TEXT , "
                + KEY_ADVOCATE_DETAILS + " TEXT)";
    }




    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
