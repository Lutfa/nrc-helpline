package com.raylabs.nrchelpline;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;

import com.raylabs.nrchelpline.Ui.RobotoTextView;

public class Training_MaterialActivity extends AppCompatActivity {
    RobotoTextView toolbar_title, text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_training__material);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            //  getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
            final Drawable upArrow = getResources().getDrawable(R.drawable.vector_back_white_icon);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("");
            toolbar_title = findViewById(R.id.toolbar_title);
            toolbar_title.setText(getString(R.string.training_material));

            text = findViewById(R.id.text);
            text.setText(Html.fromHtml(getString(R.string.aboutus_text)));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(Training_MaterialActivity.this,HomeScreenActivity.class));
    }
}
