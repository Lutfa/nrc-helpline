package com.raylabs.nrchelpline;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.raylabs.nrchelpline.Ui.MaterialDesignIconsTextView;
import com.raylabs.nrchelpline.helper.DbHelper;
import com.raylabs.nrchelpline.utils.DVoterEntity;
import com.raylabs.nrchelpline.utils.FormResponse;
import com.raylabs.nrchelpline.utils.Sliders;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeScreenActivity extends AppCompatActivity {
    RelativeLayout Loan_relativelayout, mySubmissionLayout, valuation_relativelayout, arservices_relativelayout,
            estimates_relativelayout, projects_relativelayout;
    public Toolbar toolbar;
    TextView toolbar_title;
    CharSequence mDrawerTitle;
    private CharSequence mTitle;
    ImageView profileImage;
    MaterialDesignIconsTextView activity_wizard_media_possition;


    MainActivity mainActivity = new MainActivity();
    Intent intent;
    int isMyhome = 0;
    ViewPager viewPager;
    int SliderImageCount = 0;
    Gson gson;
    static int i = 0;
    private ArrayList<String> sliders;
    SharedPreferencesActivity preferencesActivity;
    ImageView language, profile;
    DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);
        activity_wizard_media_possition = (MaterialDesignIconsTextView) findViewById(R.id.activity_wizard_media_possition);

        Loan_relativelayout = (RelativeLayout) findViewById(R.id.loanrelativeLayout);
        mySubmissionLayout = (RelativeLayout) findViewById(R.id.mySubmissionLayout);
        valuation_relativelayout = (RelativeLayout) findViewById(R.id.valuationrelativeLayout);
        arservices_relativelayout = (RelativeLayout) findViewById(R.id.arservicesrelativeLayout);
        estimates_relativelayout = (RelativeLayout) findViewById(R.id.estimatesrelativeLayout);
        projects_relativelayout = (RelativeLayout) findViewById(R.id.projectsrelativeLayout);
        // relative_enquiry = (RelativeLayout) findViewById(R.id.relative_enquiry);
        // relative_layout = (RelativeLayout) findViewById(R.id.relative_layout);
        viewPager = (ViewPager) findViewById(R.id.viewPager_home_image);
        preferencesActivity = SharedPreferencesActivity.getInstance(HomeScreenActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        language = findViewById(R.id.language);
        profile = findViewById(R.id.profile);
        String title = getResources().getString(R.string.app_name);
//        toolbar_title.setText(title);
        getSupportActionBar().setTitle("");
        gson = new Gson();
        dbHelper = new DbHelper(HomeScreenActivity.this);
        // mTitle = mDrawerTitle = getTitle();
        /*View headerView = getLayoutInflater().inflate(R.layout.header_navigation_drawer_media, mDrawerList, false);
        ImageView iv = (ImageView) headerView.findViewById(R.id.header_navigation_drawer_media_image);
        TextView ownerNameTextV = (TextView) headerView.findViewById(R.id.header_navigation_drawer_media_username);
        TextView moblileTextV = (TextView) headerView.findViewById(R.id.header_navigation_drawer_email);*/
       /* if (!StringUtils.isBlank(userKeyDetails.getprofilePicture())) {
            Picasso.with(HomeScreenActivity.this)
                    .load(userKeyDetails.getprofilePicture())
                    .error(R.drawable.default_image)
                    .transform(new CircleTransform())
                    .into(iv);
//                    viewImageCircle.SetProfileIconProfileView(MyProfileActivity.this, profileImage, userKeyDetails.getprofilePicture(), true);

        } else {
            ImageUtil.displayRoundImage(iv, getResources().getDrawable(R.drawable.default_image), null);
        }*/
        sliders = new ArrayList<>();


        Loan_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreenActivity.this, Training_MaterialActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mySubmissionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreenActivity.this, MySubmissionActivity.class);
                startActivity(intent);
                finish();
            }
        });
        valuation_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreenActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });
        arservices_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreenActivity.this, AboutUsActivity.class);
                startActivity(intent);
                finish();
            }
        });
        estimates_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreenActivity.this, FormListActivity.class);
                startActivity(intent);
                finish();
            }
        });
        projects_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreenActivity.this, ContactUsActivity.class);
                startActivity(intent);
                finish();
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreenActivity.this, MyProfile.class);
                startActivity(intent);
                finish();
            }
        });
        try {
            language.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPopup();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
      //getDataFromBackend();
        if (preferencesActivity.getIsLogIn()) {
            getDataFromBackend();
            preferencesActivity.setIsLogIn(false);
        }

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                try {
                    if (viewPager.getCurrentItem() == SliderImageCount - 1) {
                        i = 0;
                        viewPager.setCurrentItem(0);
                    }
                    viewPager.setCurrentItem(i, true);
                    i++;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 100, 5000);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int position) {
                // TODO Auto-generated method stub

                setNavigator();
            }
        });
        /*if (mainActivity.isInternetConnected(HomeScreenActivity.this)) {
            try {
                // new GetSlider().execute();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
          //  ArrayList<String> sliders = preferencesActivity.getsliderImages();
            ArrayList<String> sliders = new ArrayList<>();
            sliders.add("1");
            sliders.add("2");
            sliders.add("3");
            SetMainProductImage(sliders);
        }*/

        sliders.add("1");
        sliders.add("2");
        sliders.add("3");
        SetMainProductImage(sliders);
    }

    private void getDataFromBackend() {
        try {
            Rx2AndroidNetworking.get(MainActivity.SITE_URL + "dvoter/" + preferencesActivity.getUserId())
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(1500, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {

                                    FormResponse formResponse = gson.fromJson(response.toString(), FormResponse.class);
                                    for (int i = 0; i < formResponse.data.size(); i++) {

                                        DVoterEntity entity = formResponse.data.get(i);
                                        entity.docs = new ArrayList<>();
                                        entity.form_type = MainActivity.D_VOTER;
                                        if (entity.files.size() > 0) {
                                            for (int j = 0; j < entity.files.size(); j++) {

                                                entity.docs.add(entity.files.get(j).image);
                                            }
                                        }
                                        dbHelper.addUpdateFormTable(entity);
                                    }

                                } /*catch (JSONException e1) {
                                    e1.printStackTrace();
                                }*/ catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            Rx2AndroidNetworking.get(MainActivity.SITE_URL + "nrcreapplication/" + preferencesActivity.getUserId())
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(1500, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {

                                    FormResponse formResponse = gson.fromJson(response.toString(), FormResponse.class);
                                    for (int i = 0; i < formResponse.data.size(); i++) {

                                        DVoterEntity entity = formResponse.data.get(i);
                                        entity.docs = new ArrayList<>();
                                        entity.form_type = MainActivity.NRC_REAPPLICATION;
                                        if (entity.files.size() > 0) {
                                            for (int j = 0; j < entity.files.size(); j++) {
                                                entity.docs.add(entity.files.get(j).image);
                                            }
                                        }
                                        dbHelper.addUpdateFormTable(entity);
                                    }

                                } /*catch (JSONException e1) {
                                    e1.printStackTrace();
                                }*/ catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            Rx2AndroidNetworking.get(MainActivity.SITE_URL + "detentioncamp/" + preferencesActivity.getUserId())
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(1500, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {

                                    FormResponse formResponse = gson.fromJson(response.toString(), FormResponse.class);
                                    for (int i = 0; i < formResponse.data.size(); i++) {

                                        DVoterEntity entity = formResponse.data.get(i);
                                        entity.docs = new ArrayList<>();
                                        entity.form_type = MainActivity.DETENTION_CAMP;
                                        if (entity.files.size() > 0) {
                                            for (int j = 0; j < entity.files.size(); j++) {
                                                entity.docs.add(entity.files.get(j).image);
                                            }
                                        }
                                        dbHelper.addUpdateFormTable(entity);
                                    }

                                } /*catch (JSONException e1) {
                                    e1.printStackTrace();
                                }*/ catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            Rx2AndroidNetworking.get(MainActivity.SITE_URL + "courtcases/" + preferencesActivity.getUserId())
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(1500, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {

                                    FormResponse formResponse = gson.fromJson(response.toString(), FormResponse.class);
                                    for (int i = 0; i < formResponse.data.size(); i++) {

                                        DVoterEntity entity = formResponse.data.get(i);
                                        entity.form_type = MainActivity.COURT_CASE;
                                        entity.docs = new ArrayList<>();
                                        if (entity.files.size() > 0) {
                                            for (int j = 0; j < entity.files.size(); j++) {
                                                entity.docs.add(entity.files.get(j).image);
                                            }
                                        }
                                        dbHelper.addUpdateFormTable(entity);
                                    }

                                } /*catch (JSONException e1) {
                                    e1.printStackTrace();
                                }*/ catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            Rx2AndroidNetworking.get(MainActivity.SITE_URL + "dailyreports/" + preferencesActivity.getUserId())
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(1500, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {

                                    FormResponse formResponse = gson.fromJson(response.toString(), FormResponse.class);
                                    for (int i = 0; i < formResponse.data.size(); i++) {

                                        DVoterEntity entity = formResponse.data.get(i);
                                        entity.form_type = MainActivity.DAILY_REPORT;
                                    /*    entity.docs = new ArrayList<>();
                                        if (entity.files.size() > 0) {
                                            for (int j = 0; j < entity.files.size(); j++) {
                                                entity.docs.add(entity.files.get(j).image);
                                            }
                                        }*/
                                        dbHelper.addUpdateFormTable(entity);
                                    }

                                } /*catch (JSONException e1) {
                                    e1.printStackTrace();
                                }*/ catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        try {
            Rx2AndroidNetworking.get(MainActivity.SITE_URL + "logsheet/" + preferencesActivity.getUserId())
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(1500, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {

                                    FormResponse formResponse = gson.fromJson(response.toString(), FormResponse.class);
                                    for (int i = 0; i < formResponse.data.size(); i++) {

                                        DVoterEntity entity = formResponse.data.get(i);
                                        entity.form_type = MainActivity.LOG_SHEET;

                                    /*    entity.docs = new ArrayList<>();
                                        if (entity.files.size() > 0) {
                                            for (int j = 0; j < entity.files.size(); j++) {
                                                entity.docs.add(entity.files.get(j).image);
                                            }
                                        }*/
                                        dbHelper.addUpdateFormTable(entity);
                                    }

                                } /*catch (JSONException e1) {
                                    e1.printStackTrace();
                                }*/ catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem chaticon, notificationicon;
        try {
            getMenuInflater().inflate(R.menu.homescreen_menu, menu);
            chaticon = menu.findItem(R.id.chat);
            notificationicon = menu.findItem(R.id.notification);
            chaticon.setVisible(false);
            notificationicon.setVisible(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

   /* public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.notification:
                Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                startActivity(intent);
                return true;
            case R.id.chat:
                Intent intent1 = new Intent(getApplicationContext(), ChatActivity.class);
                startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }*/


    public void showPopup() {
        try {
            PopupMenu popup = new PopupMenu(this, language);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.language, popup.getMenu());

            Menu menu = popup.getMenu();

            Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Montserrat_Regular.otf");    //  THIS
            SpannableStringBuilder englishtitle = new SpannableStringBuilder(getResources().getString(R.string.english));
            // SpannableStringBuilder hindititle = new SpannableStringBuilder(getResources().getString(R.string.hindi));
            //  SpannableStringBuilder assamiesTitle = new SpannableStringBuilder(getResources().getString(R.string.assamies));
            SpannableStringBuilder bengailiTitle = new SpannableStringBuilder(getResources().getString(R.string.bengali));
            //  hindititle.setSpan(face, 0, hindititle.length(), 0);


            final MenuItem english = (MenuItem) menu.findItem(R.id.english);
            english.setTitle(englishtitle);
            // final MenuItem hindi = (MenuItem) menu.findItem(R.id.hindi);
            // hindi.setTitle(hindititle);
            // final MenuItem assamies = (MenuItem) menu.findItem(R.id.assamies);
            // assamies.setTitle(assamiesTitle);
            final MenuItem bengali = (MenuItem) menu.findItem(R.id.bengali);
            bengali.setTitle(bengailiTitle);

            if (preferencesActivity.getlanguage().equalsIgnoreCase("hi")) {
                english.setIcon(R.drawable.ic_done_transparent);
                // hindi.setIcon(R.drawable.ic_done_black_24dp);
                //  assamies.setIcon(R.drawable.ic_done_transparent);
                bengali.setIcon(R.drawable.ic_done_transparent);
            } else if (preferencesActivity.getlanguage().equalsIgnoreCase("as")) {
                english.setIcon(R.drawable.ic_done_transparent);
                // hindi.setIcon(R.drawable.ic_done_transparent);
                // assamies.setIcon(R.drawable.ic_done_black_24dp);
                bengali.setIcon(R.drawable.ic_done_transparent);

            } else if (preferencesActivity.getlanguage().equalsIgnoreCase("bn")) {
                english.setIcon(R.drawable.ic_done_transparent);
                //  hindi.setIcon(R.drawable.ic_done_transparent);
                //  assamies.setIcon(R.drawable.ic_done_transparent);
                bengali.setIcon(R.drawable.ic_done_black_24dp);


            } else {
                english.setIcon(R.drawable.ic_done_black_24dp);
                // hindi.setIcon(R.drawable.ic_done_transparent);
                //  assamies.setIcon(R.drawable.ic_done_transparent);
                bengali.setIcon(R.drawable.ic_done_transparent);

            }

            english.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    mainActivity.saveLanguage(HomeScreenActivity.this, preferencesActivity, "");
                    refreshResource();
                    english.setIcon(R.drawable.ic_done_black_24dp);
                    //  hindi.setIcon(R.drawable.ic_done_transparent);
                    //  assamies.setIcon(R.drawable.ic_done_transparent);
                    bengali.setIcon(R.drawable.ic_done_transparent);

                    return true;
                }
            });

            /*hindi.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    mainActivity.saveLanguage(HomeScreenActivity.this, preferencesActivity, "hi");
                    refreshResource();
                    english.setIcon(R.drawable.ic_done_transparent);
                    hindi.setIcon(R.drawable.ic_done_black_24dp);
                    assamies.setIcon(R.drawable.ic_done_transparent);
                    bengali.setIcon(R.drawable.ic_done_transparent);


                    return true;
                }
            });*/
           /* assamies.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    mainActivity.saveLanguage(HomeScreenActivity.this, preferencesActivity, "as");
                    refreshResource();
                    english.setIcon(R.drawable.ic_done_transparent);
                    hindi.setIcon(R.drawable.ic_done_transparent);
                    assamies.setIcon(R.drawable.ic_done_black_24dp);
                    bengali.setIcon(R.drawable.ic_done_transparent);

                    return true;
                }
            });*/

            bengali.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    mainActivity.saveLanguage(HomeScreenActivity.this, preferencesActivity, "bn");
                    refreshResource();
                    english.setIcon(R.drawable.ic_done_transparent);
                    //hindi.setIcon(R.drawable.ic_done_transparent);
                    // assamies.setIcon(R.drawable.ic_done_transparent);
                    bengali.setIcon(R.drawable.ic_done_black_24dp);

                    return true;
                }
            });
            mainActivity.setForceShowIconForPopup(popup);
            popup.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void refreshResource() {
        startActivity(new Intent(HomeScreenActivity.this, HomeScreenActivity.class));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //Sync the toogle state after onRestoreInstanceState has occured.
        // mDrawerToggle.syncState();
    }

    private class GetSlider extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... param) {

            String result = "";
            URL url;
            try {
                url = new URL(MainActivity.SITE_URL
                        + "slider/1");
                //Create a connection
                HttpURLConnection connection = (HttpURLConnection)
                        url.openConnection();
                //Set methods and timeouts
                connection.setRequestMethod("GET");
                connection.setReadTimeout(15000);
                connection.setConnectTimeout(15000);

                //Connect to our url
                connection.connect();
                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                } else {
                    result = "";

                }
            } catch (Exception e) {
                Log.d("GetError", "Error:" + e.getMessage());
                e.printStackTrace();
            }
            return result;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null && result != "null" && !result.equalsIgnoreCase("")) {
                gson = new GsonBuilder().disableHtmlEscaping().create();
                Sliders couponEntity = gson.fromJson(result, Sliders.class);
                Sliders slider = couponEntity.data;
                sliders.add(slider.slide1);
                sliders.add(slider.slide2);
                sliders.add(slider.slide3);
                try {
                    preferencesActivity.setSliderImages(sliders);
                    SetMainProductImage(sliders);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }
    }

    private void SetMainProductImage(List<String> sliders) {
        List<Sliders> imageList = null;
        try {
            imageList = new ArrayList<Sliders>();

            if (sliders != null) {
               /* for (int i = 0; i < sliders.size(); i++) {
                    if (!StringUtils.isBlank(sliders.get(i).ImageUrl)) {
                        try {
                            imageList.add(sliders.get(i));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }*/
                CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(HomeScreenActivity.this, sliders);
                viewPager.setAdapter(customPagerAdapter);
                SliderImageCount = sliders.size();
                setNavigator();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void setNavigator() {
        String navigation = "";
        for (int i = 0; i < SliderImageCount; i++) {
            if (i == viewPager.getCurrentItem()) {
                navigation += getString(R.string.material_icon_point_full)
                        + "  ";
            } else {
                navigation += getString(R.string.material_icon_point_empty)
                        + "  ";
            }
        }
        activity_wizard_media_possition.setText(navigation);
    }


    class CustomPagerAdapter extends PagerAdapter {

        Activity mContext;
        LayoutInflater mLayoutInflater;
        List<String> imageList;
        String imageModel;

        public CustomPagerAdapter(Activity context, List<String> imList) {
            mContext = context;
            imageList = imList;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return (imageList != null) ? imageList.size() : 0;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.image_view_pager, container, false);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.product_details_image);
            imageModel = imageList.get(position);
            TextView entityId = (TextView) itemView.findViewById(R.id.entity_id);
            try {
                if (position == 0) {
              /*  if (imageList.get(0) != null) {
                    try {
                        Picasso.with(mContext)
                                .load(imageList.get(0))
                                .into(imageView);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }*/
                    try {
                        imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.slider_image0));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (position == 1) {
                    try {
                        imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.slider_image1));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (position == 2) {

                    try {
                        imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.slider_image2));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            /*for (int i = 0; i < imageList.size(); i++) {


            }*/


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }

}
