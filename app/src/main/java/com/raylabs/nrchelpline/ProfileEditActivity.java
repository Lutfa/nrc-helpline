package com.raylabs.nrchelpline;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.raylabs.nrchelpline.Ui.RobotoEdittext;
import com.raylabs.nrchelpline.Ui.RobotoTextView;
import com.raylabs.nrchelpline.helper.CircleTransform;
import com.raylabs.nrchelpline.helper.GraphicsUtil;
import com.raylabs.nrchelpline.utils.ProfileEntity;
import com.raylabs.nrchelpline.utils.UserRegistrationEntity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.theartofdev.edmodo.cropper.CropImage;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.raylabs.nrchelpline.Form2Activity.PERMISSION_REQUEST_CODE;
import static com.raylabs.nrchelpline.Form2Activity.SELECT_FILE_CAMERA;
import static com.raylabs.nrchelpline.Form2Activity.SELECT_FILE_GALLERY;
import static com.raylabs.nrchelpline.MainActivity.MyPREFERENCES;

public class ProfileEditActivity extends AppCompatActivity /*implements OnMapReadyCallback*/ {

    private static final String TAG = "click";
    private static final int LOCATION_PERMISSION = 5;
    GoogleMap googleMap;
    Geocoder geocoder;
    MarkerOptions markerOptions;
    LatLng coordinate;
    private boolean addressDisplayed;
    private boolean isAddressEditClicked;
    private List<Address> addresses;
    private UserRegistrationEntity locationModel;
    TextView LocationDetails;
    RobotoEdittext edit_address, edit_pin, edit_landmark, edit_pharmacy_name, edit_email;
    String[] PERMISSIONS = {"android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION"};
    private Uri selectedImageUri, currentImageUri;
    private String imagePath = null;
    private AlertDialog alert;
    private boolean setBitmap, isRemoved;
    private String mCurrentPhotoPath;
    private byte[] imageInByte;
    String imageBase64String;
    Bitmap bitmap;
    RobotoTextView edit_dob;
    private ImageView profileImage;
    private SharedPreferences app_preference;
    RelativeLayout submit_layout;
    LinearLayout dobLayout;
    RelativeLayout back_layout;
    SharedPreferencesActivity preferencesActivity;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private boolean isRationaleDialogShown;
    private boolean isPermissionDeniedNever;
    String[] PERMISSIONS1 = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.CAMERA"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
      /*  MapFragment supportMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.googleMap);
        supportMapFragment.getMapAsync(ProfileEditActivity.this);*/
        LocationDetails = findViewById(R.id.LocationDetails);
        submit_layout = findViewById(R.id.submit_layout);
        back_layout = findViewById(R.id.back_layout);
        edit_pharmacy_name = findViewById(R.id.edit_pharmacy_name);
        edit_email = findViewById(R.id.edit_email);
        dobLayout = findViewById(R.id.dobLayout);
        locationModel = new UserRegistrationEntity();
        edit_address = findViewById(R.id.edit_address);
        edit_pin = findViewById(R.id.edit_pin);
        edit_landmark = findViewById(R.id.edit_landmark);
        profileImage = findViewById(R.id.profileimage);
        edit_dob = findViewById(R.id.edit_dob);
        preferencesActivity = SharedPreferencesActivity.getInstance(ProfileEditActivity.this);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                marshmallowDialog();
            }
        });
        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileEditActivity.this,MyProfile.class));
            }
        });
        dobLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCalender();
            }
        });
        edit_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dobLayout.performClick();
            }
        });
        inflatelayout();

        submit_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileEntity profileEntity = new ProfileEntity();
                if (!StringUtils.isBlank(edit_pharmacy_name.getText().toString()) && !StringUtils.isBlank(edit_email.getText().toString()) && !StringUtils.isBlank(edit_address.getText().toString()) && !StringUtils.isBlank(edit_address.getText().toString()) && !StringUtils.isBlank(edit_landmark.getText().toString())) {
                    profileEntity.name = edit_pharmacy_name.getText().toString();
                    profileEntity.email = edit_email.getText().toString();
                    profileEntity.avatar = imageBase64String;
                    profileEntity.location = edit_address.getText().toString();
                    profileEntity.area = edit_landmark.getText().toString();
                    profileEntity.age = edit_dob.getText().toString();


                    preferencesActivity.setImagePath(profileEntity.avatar);
                    preferencesActivity.setUserName(profileEntity.name);
                    preferencesActivity.setUserEmail(profileEntity.email);
                    preferencesActivity.setUserAddress(profileEntity.location);
                    preferencesActivity.setUserArea(profileEntity.area);
                    preferencesActivity.setDob(profileEntity.age);

                    //startActivity(new Intent(ProfileEditActivity.this, MyProfile.class));


                    longOperationUpdate(profileEntity);
                }
            }
        });

    }

    private void inflatelayout() {

        edit_pharmacy_name.setText(preferencesActivity.getUserName());
        edit_email.setText(preferencesActivity.getUserEmail());
        edit_address.setText(preferencesActivity.getUserAddress());
        edit_landmark.setText(preferencesActivity.getUserArea());
        edit_dob.setText(preferencesActivity.getDob());
        imageBase64String=preferencesActivity.getImagePath();
        if (preferencesActivity.getImagePath() != null && !preferencesActivity.getImagePath().equalsIgnoreCase("")) {

              /*  Glide.with(getApplicationContext())
                        .load(pharmacyDetailsEntity.Image)
                        .error(R.drawable.default_image)
                        .transform(new CircleTransformGlide(getApplicationContext()))
                        .diskCacheStrategy(DiskCacheStrategy.ALL).into(header_navigation_drawer_media_image);*/


            byte[] decodedString = Base64.decode(preferencesActivity.getImagePath(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            profileImage.setImageBitmap(new CircleTransform().transform(decodedByte));

        }
    }

    private void startCalender() {

        final Calendar c = Calendar.getInstance(Locale.ENGLISH);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        String startdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        edit_dob.setText(startdate);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();


    }

    private void marshmallowDialog() {
        try {
            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                ShowOptions();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /*@Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;
        ShowGoogleMapWithCurrentLocation();

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.d(TAG, "Map clicked");
                onLocationChanged(latLng);
            }
        });
    }*/

    private void ShowGoogleMapWithCurrentLocation() {
        double latitude = 0.0, longitude = 0.0;
        try {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.setMyLocationEnabled(true);
            geocoder = new Geocoder(ProfileEditActivity.this, Locale.getDefault());

            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location location = null;
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                onLocationChanged(new LatLng(location.getLatitude(), location.getLongitude()));
            } else {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    onLocationChanged(new LatLng(location.getLatitude(), location.getLongitude()));
                }
            }
            if (!isAddressEditClicked) {
                if (location != null) {
                    onLocationChanged(new LatLng(location.getLatitude(), location.getLongitude()));
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
            }
            coordinate = new LatLng(latitude, longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(coordinate));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 1));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            markerOptions.position(coordinate);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /*class DemoTask extends AsyncTask<String, String, ArrayList<String>> {
        ArrayList<String> resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        String input;

        public DemoTask(String inputtext) {
            input = inputtext;
        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {

            try {
                StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
                sb.append("?key=" + API_KEY);
                sb.append("&components=country:" + countryCode);
                sb.append("&input=" + URLEncoder.encode(input, "utf8"));

                URL url = new URL(sb.toString());

                System.out.println("URL: " + url);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(conn.getInputStream());

                // Load the results into a StringBuilder
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
            } catch (MalformedURLException e) {
                Log.e(LOG_TAG, "Error processing Places API URL", e);
                return resultList;
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error connecting to Places API", e);
                return resultList;
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }

            try {

                // Create a JSON object hierarchy from the results
                JSONObject jsonObj = new JSONObject(jsonResults.toString());
                JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

                // Extract the Place descriptions from the results
                resultList = new ArrayList<String>(predsJsonArray.length());
                for (int i = 0; i < predsJsonArray.length(); i++) {
                    System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                    System.out.println("============================================================");
                    resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                }
            } catch (JSONException e) {
                Log.e(TAG, "Cannot process JSON results", e);
            }
            return resultList;
        }

        protected void onPostExecute(Void result) {
            // TODO: do something with the feed
        }


    }

    public void GetCoordinatesFromPlace(String location) {
        try {
            SharedPreferences.Editor edit = app_preference.edit();
            Geocoder gc = new Geocoder(this);
            List<Address> addresses = gc.getFromLocationName(location, 5); // get the found Address Objects
            List<LatLng> ll = new ArrayList<LatLng>(addresses.size()); // A list to save the coordinates if they are available
            for (Address addr : addresses) {
                if (addr.hasLatitude() && addr.hasLongitude()) {
                    edit.putString("Longitude", String.valueOf(addr.getLongitude()));
                    edit.putString("Latitude", String.valueOf(addr.getLatitude()));
                    edit.commit();
                    onLocationChanged(new LatLng(addr.getLatitude(), addr.getLongitude()));
                }
            }
        } catch (IOException e) {
        }
    }*/
    public void onLocationChanged(LatLng latLng) {
        String detailAddressShowing = null;
        double latitude = latLng.latitude;
        double longitude = latLng.longitude;
        coordinate = new LatLng(latitude, longitude);
        googleMap.clear();
        String postalcode;
        markerOptions = new MarkerOptions();
        SharedPreferences.Editor edit;
        geocoder = new Geocoder(ProfileEditActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Log.v("log_tag", "addresses+)_+++" + addresses);

            locationModel.Landmark = addresses.get(0).getSubLocality();

            locationModel.DetailAddress = addresses.get(0).getAddressLine(0);
            postalcode = addresses.get(0).getPostalCode();
            locationModel.Pincode = postalcode;
            locationModel.Latitude = addresses.get(0).getLatitude();
            locationModel.Longitude = addresses.get(0).getLongitude();
            String countryCode = addresses.get(0).getCountryCode();

            edit_landmark.setText(locationModel.Landmark);
            edit_address.setText(locationModel.DetailAddress);
            edit_pin.setText(locationModel.Pincode);
            try {


              /*  if (postalcode != null) {

                    for (int i = 0; i < 10; i++) {
                        String addressDetails = addresses.get(0).getAddressLine(i);
                        if (addressDetails != null) {
                            postalcode = containsPinCode(addressDetails);
                            if (postalcode != null && !postalcode.equalsIgnoreCase("")) {
                                edit_pin.setText(locationModel.Pincode);
                                locationModel.Pincode = postalcode;
                                break;
                            }

                        } else
                            break;
                    }
                }*/
                if (!addressDisplayed) {
                    String localArea = addresses.get(0).getAddressLine(0);
                    LocationDetails.setText(localArea);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        markerOptions.position(coordinate);
        googleMap.addMarker(markerOptions);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(coordinate));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 1));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    protected void ShowOptions() {

        final CharSequence[] items = {getResources().getString(R.string.takeNewPhoto), getResources().getString(R.string.chooseFromGallery),
                getResources().getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                Boolean isSDPresent = Environment
                        .getExternalStorageState().equals(
                                Environment.MEDIA_MOUNTED);
                if (item == 0) {
                    if (isSDPresent)
                        getPhotoFromCamera();
                    else
                        Toast.makeText(
                                ProfileEditActivity.this,
                                "Please turn off USB storage or insert your SD card and try again",
                                Toast.LENGTH_SHORT).show();

                    return;
                } else if (item == 1) {
                    if (isSDPresent)
                        getPhotoFromGallery();
                    else
                        Toast.makeText(
                                ProfileEditActivity.this,
                                "Please turn off USB storage or insert your SD card and try again",
                                Toast.LENGTH_SHORT).show();
                    return;

                }/* else if (item == 2) {
                    imagePath = null;
                    profileImage.setImageBitmap(null);
                    setBitmap = false;
                    isRemoved = true;
                    return;

                }*/ else
                    alert.cancel();
            }

        });

        alert = builder.create();
        alert.show();

//        TextView textView = (TextView) alert.getWindow().findViewById(android.R.id.message);
//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat_Regular.otf");
//        textView.setTypeface(font);

    }

    protected void getPhotoFromCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                currentImageUri = createImageFile();
                SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();
                editor.putString("currentImageUri", String.valueOf(currentImageUri));
                editor.commit();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
            } else {
                File file = new File(currentImageUri.getPath());
                Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            }
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                startActivityForResult(intent, SELECT_FILE_CAMERA);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Uri createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        if (storageDir.exists()) {
            image = File.createTempFile(timeStamp, /* prefix */
                    ".jpg", /* suffix */
                    storageDir /* directory */
            );
        } else {
            storageDir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            if (storageDir.exists()) {
                image = File.createTempFile(timeStamp, /* prefix */
                        ".jpg", /* suffix */
                        storageDir /* directory */
                );
            }
        }
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return Uri.fromFile(image);
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(currentImageUri);
        this.sendBroadcast(mediaScanIntent);
    }

    protected void getPhotoFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select picture to upload "), SELECT_FILE_GALLERY);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_FILE_GALLERY) {
                try {
                    selectedImageUri = data.getData();
                    performCrop(selectedImageUri);
                  /*  if (isProfilePic) {
                        performCrop(selectedImageUri);
                    } else {
                        imagePath = getRealPathFromURI(selectedImageUri);
                        app_preference = getSharedPreferences(MyPREFERENCES,
                                Context.MODE_PRIVATE);
                        if (imagePath.equalsIgnoreCase("")) {
                            imagePath = ((String) selectedImageUri.toString()).substring(7, selectedImageUri.toString().length());
                        }

                        bitmap = decodeFile1(imagePath, selectedImageUri);
                    }*/
                } catch (Exception ex) {
                    Toast.makeText(ProfileEditActivity.this, "Internal error",
                            Toast.LENGTH_LONG).show();
                } catch (OutOfMemoryError ex) {
                    Toast.makeText(ProfileEditActivity.this, "Out of memory",
                            Toast.LENGTH_LONG).show();
                }
            } else if (requestCode == SELECT_FILE_CAMERA) {
                try {
                    app_preference = getSharedPreferences(MyPREFERENCES,
                            Context.MODE_PRIVATE);
                    String currentUri = app_preference.getString("currentImageUri", "");

                    currentImageUri = Uri.parse(currentUri);
                    galleryAddPic();
                    performCrop(selectedImageUri);
                   /* if (isProfilePic) {
                        performCrop(selectedImageUri);
                    } else {
                        if (mCurrentPhotoPath == null) {
                            imagePath = currentImageUri.getPath();
                        } else
                            imagePath = mCurrentPhotoPath;


                        bitmap = decodeFile1(imagePath, selectedImageUri);
                    }*/
                } catch (Exception e) {
                    Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.internal_error),
                            Toast.LENGTH_LONG).show();
                } catch (OutOfMemoryError ex) {
                    Toast.makeText(ProfileEditActivity.this, "Out of memory",
                            Toast.LENGTH_LONG).show();
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    GraphicsUtil graphicUtil = null;
                    Uri resultUri = result.getUri();
                    try {
                        Bitmap thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                        profileImage.setImageBitmap(thePic);
                        String filename = "profileImage.jpg";
                        // imagePath = Environment.getExternalStorageDirectory().toString() + "/Xampr/ProfileImages/" + filename;
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thePic.compress(Bitmap.CompressFormat.JPEG, 95, stream);
                        imageInByte = stream.toByteArray();
                        imageBase64String = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                        // mainActivity.SaveImageGallery(thePic, null, filename, null);
                        //SaveImage(thePic, imageInByte, null);
                        imagePath = Environment.getExternalStorageDirectory().toString() + "/NRC/ProfileImages/" + filename;
                        File file = new File(imagePath);
                        if (file.exists()) {
                            bitmap = BitmapFactory.decodeFile(imagePath);
                            profileImage.setImageBitmap(bitmap);
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }

            }
        }
    }


    private Bitmap rotateImage(int orint, Bitmap bitmap) {
        Bitmap bit = bitmap;
        Bitmap result = null;
        try {
            Matrix matrix = new Matrix();

            switch (orint) {
                case 1:
                    result = bitmap;
                    break;
                case 6:
                    matrix.postRotate(90);
                    break;
                case 3:
                    matrix.postRotate(180);
                    break;
                case 8:
                    matrix.postRotate(270);
                    break;
                default:
                    break;
            }
            result = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
        } catch (Exception e) {
            return bit;
        }
        return result;
    }


    private String getRealPathFromURI(Uri contentURI) {
        String filePath = "";
        Cursor cursor = null;
        try {
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
            String[] column = {MediaStore.Images.Media.DATA};
            if (isKitKat
                    && DocumentsContract.isDocumentUri(
                    ProfileEditActivity.this, contentURI)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(contentURI)) {
                    final String docId = DocumentsContract
                            .getDocumentId(contentURI);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        filePath = Environment.getExternalStorageDirectory()
                                + "/" + split[1];
                    } else {
                        String fileExtSDPath = System
                                .getenv("SECONDARY_STORAGE");
                        if ((null == fileExtSDPath)
                                || (fileExtSDPath.length() == 0)) {
                            fileExtSDPath = System
                                    .getenv("EXTERNAL_SDCARD_STORAGE");
                        }
                        filePath = fileExtSDPath + "/" + split[1];
                    }
                }
                // if (Build.VERSION.SDK_INT >= 19)
                else if (isDownloadsDocument(contentURI)) {
                    String wholeID = DocumentsContract
                            .getDocumentId(contentURI);
                    String[] id = wholeID.split(":");
                    final String type = id[0];
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"),
                            Long.valueOf(wholeID));
                    cursor = getContentResolver().query(contentUri, column,
                            null, null, null);
                    int columnIndex = cursor.getColumnIndex(column[0]);
                    if (cursor.moveToFirst()) {
                        filePath = cursor.getString(columnIndex);
                    }
                    cursor.close();
                } else if (isMediaDocument(contentURI)) {
                    final String docId = DocumentsContract
                            .getDocumentId(contentURI);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    String id = docId.split(":")[1];
                    String sel = MediaStore.Images.Media._ID + "=?";
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        cursor = getContentResolver().query(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                column, sel, new String[]{id}, null);
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        cursor = getContentResolver().query(contentUri, column,
                                null, null, null);
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        cursor = getContentResolver().query(contentUri, column,
                                null, null, null);
                    }
                    int columnIndex = cursor.getColumnIndex(column[0]);
                    if (cursor.moveToFirst()) {
                        filePath = cursor.getString(columnIndex);
                    }
                    cursor.close();
                }
            } else {
                cursor = getContentResolver().query(contentURI, column, null,
                        null, null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return filePath;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    private void performCrop(Uri tempUri) {
        try {
            CropImage.activity(tempUri)
                    .setAspectRatio(1, 1)
                    .start(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        }*/


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                try {
                    boolean isPhoneStateDone = false,
                            isCamera = false,
                            isShowRationaleReadExternal = false,
                            isShowRationaleReadCamera = false;
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];
                        if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            isShowRationaleReadExternal = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isPhoneStateDone = true;
                            } else {
                                isPhoneStateDone = false;
                            }
                        } else if (permission.equals(Manifest.permission.CAMERA)) {
                            isShowRationaleReadCamera = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isCamera = true;
                            } else {
                                isCamera = false;
                            }
                        }
                    }
                    if (isPhoneStateDone && isCamera) {
                        ShowOptions();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            case LOCATION_PERMISSION:
                try {
                    boolean isPhoneStateDone = false,
                            isCamera = false,
                            isShowRationaleReadExternal = false,
                            isShowRationaleReadCamera = false;
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];
                        if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                            isShowRationaleReadExternal = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                ShowGoogleMapWithCurrentLocation();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    private void requestPermission() {
        try {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                    ) {
                isRationaleDialogShown = true;
                ActivityCompat.requestPermissions(this, PERMISSIONS1, PERMISSION_REQUEST_CODE);
            } else {
                isPermissionDeniedNever = true;
                ActivityCompat.requestPermissions(this, PERMISSIONS1, PERMISSION_REQUEST_CODE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void longOperationUpdate(final ProfileEntity profileEntity) {
        final ProgressDialog progressBar = new ProgressDialog(ProfileEditActivity.this);
        progressBar.setTitle("Please wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressBar.show();
        progressBar.setCancelable(false);
        Gson gson = new Gson();
        String json = gson.toJson(profileEntity);

        try {
            Rx2AndroidNetworking.patch(MainActivity.SITE_URL + "profile_update/" + preferencesActivity.getUserId())
                    .addJSONObjectBody(new JSONObject(json))
                    .addHeaders("Content-type", "application/json")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(1300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {

                                    String Id = null;
                                    if (response.get("status").toString().equalsIgnoreCase("Profile Updated")) {
                                        progressBar.dismiss();

                                        preferencesActivity.setImagePath(profileEntity.avatar);
                                        preferencesActivity.setUserName(profileEntity.name);
                                        preferencesActivity.setUserEmail(profileEntity.email);
                                        preferencesActivity.setUserAddress(profileEntity.location);
                                        preferencesActivity.setUserArea(profileEntity.area);
                                        preferencesActivity.setDob(profileEntity.age);

                                        startActivity(new Intent(ProfileEditActivity.this, MyProfile.class));

                                    }


                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                    progressBar.cancel();
                                } catch (Exception e) {
                                    progressBar.cancel();

                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                            progressBar.dismiss();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


}
