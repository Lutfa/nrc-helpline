package com.raylabs.nrchelpline;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.raylabs.nrchelpline.Ui.RobotoTextView;

public class FormListActivity extends AppCompatActivity {
    CardView dvoter_iconView, nrc_iconView, relative_court_Case_iconView, relative_dtention_camp_iconView, relative_daily_report_iconView,relative_log_sheet_iconView;
    RobotoTextView toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //  getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        final Drawable upArrow = getResources().getDrawable(R.drawable.vector_back_white_icon);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(getString(R.string.new_submission));
        dvoter_iconView = findViewById(R.id.dvoter_iconView);
        nrc_iconView = findViewById(R.id.nrc_iconView);
        relative_court_Case_iconView = findViewById(R.id.relative_court_Case_iconView);
        relative_dtention_camp_iconView = findViewById(R.id.relative_dtention_camp_iconView);
        relative_daily_report_iconView = findViewById(R.id.relative_daily_report_iconView);
        relative_log_sheet_iconView = findViewById(R.id.relative_log_sheet_iconView);
        dvoter_iconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FormListActivity.this, Form1Activity.class));
            }
        });
        nrc_iconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FormListActivity.this, Form2Activity.class));
            }
        });
        relative_dtention_camp_iconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FormListActivity.this, Form4Activity.class));
            }
        });
        relative_court_Case_iconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FormListActivity.this, Form3Activity.class));
            }
        });
        relative_daily_report_iconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FormListActivity.this, DailyReportActivity.class));
            }
        }); relative_log_sheet_iconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FormListActivity.this, LogSheetActivity.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FormListActivity.this,HomeScreenActivity.class));
        finish();
    }

}
