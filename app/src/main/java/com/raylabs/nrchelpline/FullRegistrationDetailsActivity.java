package com.raylabs.nrchelpline;/*
package com.assam.amcr;

import android.*;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.zibew.distributor.Ui.RobotoTextView;
import com.zibew.distributor.Ui.SearchMedicineResponse;
import com.zibew.distributor.model.PharmacyDetailsEntity;

import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FullRegistrationDetailsActivity extends Baseactivity implements GCMRegistrationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private EditText edttxt_login_doorno, ownerAddressText, ownerlandmarkText, ownerCityText, ownerStateText, pincodeTxt;

    private Button registerBtn;
    String TAG = "FullRegistrationDetailsActivity";
    LocationManager locationManager;
    boolean isNetworkEnabled = false, isGPSEnabled = false;
    private static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;
    private boolean mRequestingLocationUpdates = false;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private static final int PERMISSION_REQUEST_CODE = 1;
    //    PharmacyRegistrationPost pharmacyRegistrationPost;
    private View view;
    Toolbar loginToolbar;
    private CoordinatorLayout coordinatorLayout;
    double longiTude = 0.0, latiTude = 0.0;
    String[] PERMISSIONS = {"android.permission.ACCESS_FINE_LOCATION"};


    String countryName = null, pharmacyAddress = null,
            pharmacyLandmark = null, pharmacyCity = null, pharmacyState = null, pharmacyPincode = null;
    SharedPreferences app_preference = null;
    private static final String MyPREFERENCES = null;
    MainActivity mainActivity;
    TextInputLayout pharmacyAddresHint, pharmacyPinCodeHint,

    pharmacyLandmarkHint, pharmacycityHint, pharmacyStateHint;
    int step = 1, max = 50, min = 1, progressValue = 1;
    String json;
    Gson gson;
    SearchMedicineResponse locationModel;
    double Latitude = 0, Longitude = 0, latFromMarker = 0.0, longfromamrker = 0.0;
    private SharedPreferencesActivity sharedPreferencesActivity;
    private DBHelper dbHelper;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReference();
        setHeader(loginToolbar);
        setHeader(registerBtn);

    }

    @Override
    public void setReference() {
        view = LayoutInflater.from(this).inflate(R.layout.activity_full_registration_details, container);
        try {
            loginToolbar = (Toolbar) view.findViewById(R.id.toolbar);
            setSupportActionBar(loginToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            gson = new Gson();
            locationModel = new SearchMedicineResponse();
            sharedPreferencesActivity = new SharedPreferencesActivity(getApplicationContext());
            Intent i = getIntent();
            json = i.getStringExtra("LocationJson");

            progressDialog = new ProgressDialog(FullRegistrationDetailsActivity.this);
            progressDialog.setMessage("Loading..");

            locationModel = gson.fromJson(json, SearchMedicineResponse.class);
            ownerlandmarkText = (EditText) view.findViewById(R.id.edttxt_login_landmak);
            ownerCityText = (EditText) view.findViewById(R.id.edttxt_login_city);
            ownerStateText = (EditText) view.findViewById(R.id.edttxt_login_state);
            pincodeTxt = (EditText) view.findViewById(R.id.edttxt_login_pincode);
            edttxt_login_doorno = (EditText) view.findViewById(R.id.edttxt_login_doorno);
            dbHelper = new DBHelper(getApplicationContext());
            getSupportActionBar().setTitle(getString(R.string.pharDetails));
            app_preference = this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            Latitude = Double.valueOf(app_preference.getString("Latitude", "0"));
            Longitude = Double.valueOf(app_preference.getString("Longitude", "0"));

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayout);
//            pharmacynameText = (EditText) findViewById(R.id.edttxt_pharmacy_name);
            ownerAddressText = (EditText) view.findViewById(R.id.edttxt_owner_addr);
            ownerlandmarkText = (EditText) view.findViewById(R.id.edttxt_login_landmak);
            ownerCityText = (EditText) view.findViewById(R.id.edttxt_login_city);
            ownerStateText = (EditText) view.findViewById(R.id.edttxt_login_state);
            pincodeTxt = (EditText) view.findViewById(R.id.edttxt_login_pincode);

            registerBtn = (Button) view.findViewById(R.id.btn_login_next);


            //TextInputLayout for setting hint dynamically
            pharmacyAddresHint = (TextInputLayout) view.findViewById(R.id.owner_addr_Hint);
            pharmacyPinCodeHint = (TextInputLayout) view.findViewById(R.id.login_pincode_Hint);
            pharmacyLandmarkHint = (TextInputLayout) view.findViewById(R.id.login_landmak_Hint);
            pharmacycityHint = (TextInputLayout) view.findViewById(R.id.login_city_Hint);
            pharmacyStateHint = (TextInputLayout) view.findViewById(R.id.login_state_Hint);
            //pharmacyDeliveryRadiusHint = (TextInputLayout) findViewById(R.id.login_landmak_Hint);


            ownerAddressText.setText(locationModel.DetailAddress);
            ownerlandmarkText.setText(locationModel.Landmark);
            ownerCityText.setText(locationModel.City);
            ownerStateText.setText(locationModel.State);
            pincodeTxt.setText(locationModel.Pincode);


            if (checkGooglePlayServices()) {
                buildGoogleApiClient();
            }
            registerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        PharmacyDetailsEntity pharmacyDetailsEntity = new PharmacyDetailsEntity();
                        pharmacyDetailsEntity.DoorNo = edttxt_login_doorno.getText().toString();
                        pharmacyDetailsEntity.Country = countryName;
                        pharmacyDetailsEntity.State = ownerStateText.getText().toString();
                        pharmacyDetailsEntity.City = ownerCityText.getText().toString();
                        pharmacyDetailsEntity.Address = ownerAddressText.getText().toString();
                        pharmacyDetailsEntity.LandMark = ownerlandmarkText.getText().toString();
                        pharmacyDetailsEntity.Pincode = pincodeTxt.getText().toString();
                        pharmacyDetailsEntity.PharmacyId = sharedPreferencesActivity.getUserId();
                        pharmacyDetailsEntity.DistributorId = "1";
                        longOperationConfirmAddress(pharmacyDetailsEntity);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setEditTextMaxLength() {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(25);
    }

    @Override
    public void registrationCallback(String regId) {
        String json = null, disCount = null, latLong = null, phoneNo = null, country_code = null, country_zip_code = null;
        String[] code1 = null;

        Intent intent = new Intent(getApplicationContext(), UploadLiscenceActivity.class);
        startActivity(intent);
        finish();
        sharedPreferencesActivity.AddressDetailsEntered(true);

      */
/*  SignInEntity loginEntity = new SignInEntity();
        try {
            disCount = pharmacydisCType + " " + pharmacyDiscount + "" + discount_Percent_Text.getText().toString();
            loginCredentials.saveMinimumOrderCost(pharmacyDelCost);
            loginCredentials.saveCoordinates(latLong);
            if (!starttime.getText().toString().equalsIgnoreCase(getString(R.string.opentime)) && !endTime.getText().toString().equalsIgnoreCase(getString(R.string.closetime))) {
                storeTime = starttime.getText().toString() + "^" + endTime.getText().toString();
            }
            if (pharmacyDelCost == null || pharmacyDelCost.equalsIgnoreCase(""))
                pharmacyDelCost = "50";

            try {
                String[] rl = this.getResources().getStringArray(R.array.country_code);
                for (int i = 0; i < rl.length; i++) {
                    String[] g = rl[i].split("\\(");
                    if (g[0].trim().equals(countryName)) {
                        country_code = g[1];
                        break;
                    }
                }

                if (country_code != null) {
                    String code = country_code;
                    code1 = code.split("\\)");
                    country_zip_code = code1[0];
                }else
                {
                    if(countryName.equalsIgnoreCase("India"))
                    {
                        country_zip_code="+91";
                    }else
                    {
                        country_zip_code="+62";
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            loginEntity.ChatId = Integer.valueOf(loginCredentials.getPharmacyId());
            loginEntity.DeviceId = loginCredentials.getDeviceGuid();
            loginEntity.Address = pharmacyAddress;
            loginEntity.PharmacyName = pharmacyName;
            loginEntity.Pincode = pharmacyPincode;
            loginEntity.State = pharmacyState;
            loginEntity.City = pharmacyCity;
            loginEntity.LandMark = pharmacyLandmark;
            loginEntity.MinOrder = pharmacyMinOrder;
            loginEntity.Latitude = String.valueOf(Latitude);
            loginEntity.Longitude = String.valueOf(Longitude);
            loginEntity.Country = countryName;
            loginEntity.StoreTime = storeTime;
            loginEntity.DeliveryCost = pharmacyDelCost;
            loginEntity.Discount = disCount;
            int delRadiusMitre = (Integer.valueOf(pharmacyDelRadius) * 1000);
            loginEntity.DeliveryRadius = String.valueOf(delRadiusMitre);
            loginEntity.APIFor = "PharmacyRegistration";
//            loginEntity.CountryCode = country_zip_code;
//            loginEntity.IsPhoneVerified =true;
//            loginEntity.PhoneNumber =loginCredentials.getPhoneNumber();
            loginCredentials.setCountryCode(country_zip_code);
            loginCredentials.setCountryName(countryName);
           *//*
*/
/* hsDetails.put("UserGuid", loginCredentials.getPharmacyId());
           // hsDetails.put("PhoneNumber", phoneNo);
            hsDetails.put("LandMark", pharmacynameText.getText().toString());
            hsDetails.put("Address", ownerAddressText.getText().toString());
            hsDetails.put("Pincode", pincodeTxt.getText().toString());
            //hsDetails.put("EmailAddress", emailText.getText().toString());
            hsDetails.put("State", disCount);
            hsDetails.put("City", minOrderCost);
            hsDetails.put("Country", latLong);
            hsDetails.put("MinOrder", minOrder.getText().toString());
            if (storeTime != null) {
                hsDetails.put("StoreTime", storeTime);
            }*//*
*/
/*
            Gson gson = new Gson();
            json = gson.toJson(loginEntity);

        } catch (Exception e) {
            Log.i(TAG, e.getLocalizedMessage());
        }
        uploadPharmacyIcon(json);*//*

    }


    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target)
                && Patterns.EMAIL_ADDRESS.matcher(target)
                .matches();
    }

    private boolean checkGooglePlayServices() {

        int checkGooglePlayServices = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            */
/*
            * google play services is missing or update is required
			*  return code could be
			* SUCCESS,
			* SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED,
			* SERVICE_DISABLED, SERVICE_INVALID.
			*//*

            GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                    this, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();

            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_RECOVER_PLAY_SERVICES) {

            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, getString(R.string.googleplayservice),
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(FullRegistrationDetailsActivity.this, getString(R.string.connectionfailed), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(FullRegistrationDetailsActivity.this, getString(R.string.connectionfailed), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        double longitude = 0.0, latitude = 0.0;
        try {
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                getCountryName(latitude, longitude);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void onConnected(Bundle bundle) {
        try {
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(20000);
            mLocationRequest.setFastestInterval(5000);
            mRequestingLocationUpdates = true;
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            if (Build.VERSION.SDK_INT >= 23 && !checkPermission()) {
                requestPermission();
            } else {
                if (!isGPSEnabled) {
                    buildAlertMessageNoGps();
                } else {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                    Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    if (mLastLocation != null) {
                        latiTude = mLastLocation.getLatitude();
                        longiTude = mLastLocation.getLongitude();
                        getCountryName(latiTude, longiTude);
                        //Toast.makeText(FullRegistrationDetailsActivity.this, "LatLongitude "+latiTude +","+longiTude, Toast.LENGTH_LONG).show();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private boolean checkPermission() {
        boolean isGranted = false;
        try {
            int result_location = ContextCompat.checkSelfPermission(FullRegistrationDetailsActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (result_location == PackageManager.PERMISSION_GRANTED) {
                isGranted = true;
            } else {
                isGranted = false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isGranted;
    }

    private void requestPermission() {
        try {
            if (ActivityCompat.shouldShowRequestPermissionRationale(FullRegistrationDetailsActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(FullRegistrationDetailsActivity.this, getString(R.string.gpsMessage), Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);
                //ActivityCompat.requestPermissions(FullRegistrationDetailsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                try {

                    break;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();

            if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        try {
            super.onPause();
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, (LocationListener) this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    protected void onStop() {
        try {
            super.onStop();
            if (mGoogleApiClient != null) {
                mGoogleApiClient.disconnect();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    protected void onStart() {
        boolean isGPSEnabled = false;
        try {
            super.onStart();
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
            */
/*if (isGPSEnabled) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    latiTude = mLastLocation.getLatitude();
                    longiTude = mLastLocation.getLongitude();
                    //Toast.makeText(FullRegistrationDetailsActivity.this, "LatLongitude "+latiTude +","+longiTude, Toast.LENGTH_LONG).show();
                }
            }*//*

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(
                getString(R.string.gpsMessage))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.statusYesBtn),
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    @SuppressWarnings("unused") final DialogInterface dialog,
                                    @SuppressWarnings("unused") final int id) {
                                startActivity(new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    private void getCountryName(double lat, double longi) {
        List<Address> addresses;
        Geocoder geocoder;
        geocoder = new Geocoder(FullRegistrationDetailsActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lat, longi, 1);
            countryName = addresses.get(0).getCountryName();

           */
/* String area = addresses.get(0).getSubLocality();
            String CityName = addresses.get(0).getLocality();
            String stateName = addresses.get(0).getAdminArea();
            String countrycode = addresses.get(0).getCountryCode();
            String postalCode = addresses.get(0).getPostalCode();
            double getlatitude = addresses.get(0).getLatitude();
            double getlongitude = addresses.get(0).getLongitude();
*//*


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void longOperationConfirmAddress(final PharmacyDetailsEntity logInEntity) {
        String json = gson.toJson(logInEntity);
        try {
            progressDialog.show();
            Rx2AndroidNetworking.post(AppConstant.SITE_URL + "ConfirmPharmacy")
                    .addJSONObjectBody(new JSONObject(json))
                    .addHeaders("Content-Type", "application/json")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {
                                    PharmacyDetailsEntity registrationResponse = gson.fromJson(response.toString(), PharmacyDetailsEntity.class);
                                    if (registrationResponse.Status.equalsIgnoreCase("Success")) {
                                        if (progressDialog != null) {
                                            progressDialog.dismiss();
                                        }
                                        dbHelper.AddUpdatePharmacyDetails(registrationResponse.Response);
                                        sharedPreferencesActivity.AddressDetailsEntered(true);
                                        Intent intent = new Intent(getApplicationContext(), UploadLiscenceActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (progressDialog != null) {
                                        progressDialog.dismiss();
                                    }
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
*/
