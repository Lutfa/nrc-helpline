package com.raylabs.nrchelpline;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.raylabs.nrchelpline.helper.LoadBitmapFromURL;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static final String NRC_REAPPLICATION = "nrc_reapplication";
    public static final String DETENTION_CAMP = "detention_camp";
    public static final String D_VOTER = "dvoter";
    public static final String COURT_CASE = "court_case";
    public static final String DAILY_REPORT = "daily_report";
    public static final String LOG_SHEET = "log_sheet";
    public static final String FORMID = "FORMID";
    public static final String LISTED = "listed";
    public static final String LEFTOUT = "leftout";
    Snackbar snackbar;
    private static MainActivity mainActivity;

    public static final String URL_SIGNUP = "signup";
    public static final String SITE_URL = "http://amcr-env.pn3bsvvimn.us-east-1.elasticbeanstalk.com/api/";
    public static String MyPREFERENCES = "";

    public boolean isInternetConnected(Context context) {
        boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        isConnected = true;
                    }
                }
            }
        }
        return isConnected;
    }

    public static MainActivity getInstance() {
        if (mainActivity == null) { //if there is no instance available... create new one
            mainActivity = new MainActivity();
        }
        return mainActivity;
    }

    public void show_snakbar(String message, final View view) {
        try {
            snackbar = Snackbar
                    .make(view, message, Snackbar.LENGTH_LONG)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Snackbar snackbar1 = Snackbar.make(view, "Message is restored!", Snackbar.LENGTH_SHORT);
                            snackbar1.dismiss();
                        }
                    });
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap GetBitmapFromURI(Context context, String picURI) {
        Bitmap bitmap = null;
        try {
            String actualFileName = picURI.substring(picURI.lastIndexOf("/") + 1);
            LoadBitmapFromURL loadBitmapFromURL = new LoadBitmapFromURL(picURI);
            boolean isLive = isInternetConnected(context);
            if (isLive) {
                bitmap = loadBitmapFromURL.execute().get();
                SaveImageGallery(bitmap, null, actualFileName, null);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bitmap;
    }

    public String SaveImageGallery(Bitmap bitmap, byte[] byteArrayData, String filename, String imgActualPath) {
        File file = null, videoDirectory, AudioDirectory, galleryDirectory, directory, documentDirectory, profileDirectory;
        String root;
        Bitmap localBitMap;
        FileOutputStream fos;
        boolean isGallery = false, isOthers = false;
        try {
            root = Environment.getExternalStorageDirectory().toString();
            directory = new File(root + "/NRC");
            if (!directory.exists())
                directory.mkdirs();
            videoDirectory = directory.getParentFile();
            AudioDirectory = directory.getParentFile();
            galleryDirectory = directory.getParentFile();
            videoDirectory = new File(directory + "/NRC Videos");
            AudioDirectory = new File(directory + "/NRC Audio");
            galleryDirectory = new File(directory + "/NRC Images");
            documentDirectory = new File(directory + "/NRC Documents");
            profileDirectory = new File(directory + "/ProfileImages");

            if (!videoDirectory.exists())
                videoDirectory.mkdirs();
            if (!AudioDirectory.exists())
                AudioDirectory.mkdirs();
            if (!galleryDirectory.exists())
                galleryDirectory.mkdirs();
            if (!documentDirectory.exists())
                documentDirectory.mkdirs();
            if (!profileDirectory.exists())
                profileDirectory.mkdirs();

            if (filename != null) {
                if (filename.toString().endsWith(".jpg")
                        || filename.toString().endsWith(".png")
                        || filename.toString().endsWith(".jpeg")) {

                    if (StringUtils.equalsIgnoreCase(filename, "profileImage.jpg"))
                        file = new File(profileDirectory, filename);
                    else
                        file = new File(galleryDirectory, filename);

                    isGallery = true;

                } else if (filename.toString().endsWith(".pdf")
                        || filename.toString().endsWith(".doc")
                        || filename.toString().endsWith(".docx")
                        || filename.toString().endsWith(".ppt")
                        || filename.toString().endsWith(".pptx")
                        || filename.toString().endsWith(".xls")
                        || filename.toString().endsWith(".xlsx")
                        || filename.toString().endsWith(".txt")) {
                    file = new File(documentDirectory, filename);
                    isOthers = true;
                } else if (filename.toString().endsWith(".mp3")
                        || filename.toString().endsWith(".ogg")
                        || filename.toString().endsWith(".m4a")
                        || filename.toString().endsWith(".amr")
                        || filename.toString().endsWith(".3gpp")) {
                    file = new File(AudioDirectory, filename);
                    isOthers = true;
                } else if (filename.toString().endsWith(".mp4")
                        || filename.toString().endsWith(".3gp")
                        || filename.toString().endsWith(".avi")
                        || filename.toString().endsWith(".mkv")) {
                    file = new File(videoDirectory, filename);
                    isOthers = true;
                }
            }
            if (file != null && file.exists())
                file.delete();
            try {
                if (file != null) {
                    fos = new FileOutputStream(file, false);
                    if (isGallery) {
                        if (imgActualPath != null) {
                            localBitMap = BitmapFactory.decodeFile(imgActualPath);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            localBitMap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            byteArrayData = stream.toByteArray();
                            fos.write(byteArrayData);
                        } else if (byteArrayData != null) {
                            // bitmap = BitmapFactory.decodeFile(filpath);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            // bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            // getByteArray = stream.toByteArray();
                            fos = new FileOutputStream(file, false);
                            fos.write(byteArrayData);
                            fos.flush();
                            fos.close();
                        } else
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, fos);
                    } else if (isOthers) {
                        fos.write(byteArrayData);
                    }
                    fos.flush();
                    fos.close();
                    return galleryDirectory + "/" + filename;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return "";
    }


    public static void ShowToast(Context context, String string) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).show();

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        try {
            if (height > reqHeight || width > reqWidth) {
                final int heightRatio = Math.round((float) height / (float) reqHeight);
                final int widthRatio = Math.round((float) width / (float) reqWidth);
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return inSampleSize;
    }

    public void saveLanguage(Context mContext, SharedPreferencesActivity app_preference, String language) {
        try {


            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            mContext.getResources().updateConfiguration(config,
                    mContext.getResources().getDisplayMetrics());

            app_preference.setlanguage(language);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void updateResources(Context context) {
        String language = null;
        SharedPreferencesActivity preferencesActivity = SharedPreferencesActivity.getInstance(context);
        try {
            language = preferencesActivity.getlanguage();

            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            Resources resources = context.getResources();
            Configuration configuration = resources.getConfiguration();
            configuration.locale = locale;
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static void setForceShowIconForPopup(PopupMenu popupMenu) {
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void showToast(Context context, String string) {
        try {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).show();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
