package com.raylabs.nrchelpline;/*
package com.assam.amcr;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.gson.Gson;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.zibew.distributor.Ui.RobotoTextView;
import com.zibew.distributor.model.LogInEntity;
import com.zibew.distributor.model.PharmacyDetailsEntity;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RegistrationDetails extends Baseactivity {

    RelativeLayout bottom_bar_next;
    RobotoTextView starttime, endTime, storeTimingTV, toTV;
    static final int TIME_DIALOG_ID = 1;
    static final int TIME_DIALOG_ID_1 = 2;
    int mhour, mmin, endmhour, endmmin;
    Boolean msec = false;
    boolean isStartTime = false;
    EditText pharmacynameText;
    SharedPreferencesActivity sharedPreferencesActivity;
    private Gson gson = new Gson();
    EditText et_name, et_email, edttxt_pharmacy_name;
    private DBHelper dbHelper;
    ProgressDialog progressDialog;
    private View view;
    TextView title;
    RelativeLayout toolbar_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReference();
        setTextColor(storeTimingTV);
        setTextColor(toTV);
        setHeader(toolbar_layout);
        setGradientBgButton(bottom_bar_next);

    }

    @Override
    public void setReference() {
        view = LayoutInflater.from(this).inflate(R.layout.activity_registration_details, container);
        bottom_bar_next = (RelativeLayout) view.findViewById(R.id.bottom_bar_next);
        toolbar_layout = (RelativeLayout) view.findViewById(R.id.toolbar_layout);
        starttime = (RobotoTextView) view.findViewById(R.id.startTime);
        endTime = (RobotoTextView) view.findViewById(R.id.endTime);
        title = (RobotoTextView) view.findViewById(R.id.title);
        et_name = (EditText) view.findViewById(R.id.et_name);
        et_email = (EditText) view.findViewById(R.id.et_email);
        edttxt_pharmacy_name = (EditText) view.findViewById(R.id.edttxt_pharmacy_name);
        storeTimingTV = (RobotoTextView) view.findViewById(R.id.storetimingTV);
        toTV = (RobotoTextView) view.findViewById(R.id.to_TV);
        dbHelper = new DBHelper(getApplicationContext());
        progressDialog = new ProgressDialog(RegistrationDetails.this);
        progressDialog.setMessage("Loading..");
        sharedPreferencesActivity = new SharedPreferencesActivity(getApplicationContext());
        starttime.setText("9:00 AM");
        starttime.setTextColor(getResources().getColor(R.color.black));
        endTime.setText("10:00 PM");
        title.setText(getString(R.string.app_name));
        endTime.setTextColor(getResources().getColor(R.color.black));
        bottom_bar_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!StringUtils.isBlank(et_name.getText().toString())) {

                    if (!StringUtils.isBlank(et_email.getText().toString())) {

                        if (!StringUtils.isBlank(edttxt_pharmacy_name.getText().toString())) {

                            PharmacyDetailsEntity pharmacyDetailsEntity = new PharmacyDetailsEntity();
                            pharmacyDetailsEntity.PhoneNumber = sharedPreferencesActivity.getPhoneNumber();
                            pharmacyDetailsEntity.PharmacyId = sharedPreferencesActivity.getUserId();
                            pharmacyDetailsEntity.DistributorId = "1";
                            pharmacyDetailsEntity.Name = et_name.getText().toString();
                            pharmacyDetailsEntity.Email = et_email.getText().toString();
                            pharmacyDetailsEntity.StoreName = edttxt_pharmacy_name.getText().toString();
                            pharmacyDetailsEntity.Timings = starttime.getText().toString() + "-" + endTime.getText().toString();
                            longOperationConfirmRegister(pharmacyDetailsEntity);
                        } else {
                            Toast.makeText(RegistrationDetails.this, "Enter Email ChatId", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        Toast.makeText(RegistrationDetails.this, "Enter Email ChatId", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RegistrationDetails.this, "Enter Name", Toast.LENGTH_SHORT).show();
                }

            }
        });
        starttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStartTime = true;
                if (starttime.getText().toString().equalsIgnoreCase(getString(R.string.opentime))) {
                    Calendar cal = Calendar.getInstance();
                    mhour = cal.get(Calendar.HOUR_OF_DAY);
                    mmin = cal.get(Calendar.MINUTE);
                } else {
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
                    try {
                        cal.setTime(sdf.parse(starttime.getText().toString()));
                        mhour = cal.get(Calendar.HOUR_OF_DAY);
                        mmin = cal.get(Calendar.MINUTE);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                showDialog(TIME_DIALOG_ID);

            }
        });
        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStartTime = false;

                if (endTime.getText().toString().equalsIgnoreCase(getString(R.string.closetime))) {
                    Calendar cal = Calendar.getInstance();
                    endmhour = cal.get(Calendar.HOUR_OF_DAY);
                    endmmin = cal.get(Calendar.MINUTE);
                } else {
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");

                    try {
                        cal.setTime(sdf.parse(endTime.getText().toString()));
                        endmhour = cal.get(Calendar.HOUR_OF_DAY);
                        endmmin = cal.get(Calendar.MINUTE);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                showDialog(TIME_DIALOG_ID_1);
            }
        });
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {

            case TIME_DIALOG_ID:
               */
/* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    return new TimePickerDialog(this, R.style.DialogTheme, mtimesetListener, mhour, mmin, msec);
                else*//*

                return new TimePickerDialog(this, mtimesetListener, mhour, mmin, msec);
            case TIME_DIALOG_ID_1:
              */
/*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    return new TimePickerDialog(this, R.style.DialogTheme, mtimesetListener, endmhour, endmmin, msec);
                else*//*

                return new TimePickerDialog(this, mtimesetListener, endmhour, endmmin, msec);


        }
        return null;
    }

    TimePickerDialog.OnTimeSetListener mtimesetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hour, int min) {
            // TODO Auto-generated method stub


            updateTime(hour, min);
        }


    };

    private void updateTime(int hours, int mins) {
        String timeSet = "";

        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";

        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";

        } else if (hours == 12)

            timeSet = "PM";

        else
            timeSet = "AM";
        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);
        String aTime = new StringBuilder().append(hours).append(":")
                .append(minutes).append(" ").append(timeSet).toString();
        if (isStartTime) {
            starttime.setText(aTime);
            starttime.setTextColor(getResources().getColor(R.color.black));
        } else {
            endTime.setText(aTime);
            endTime.setTextColor(getResources().getColor(R.color.black));
        }

    }

    private void longOperationConfirmRegister(final PharmacyDetailsEntity logInEntity) {
        String json = gson.toJson(logInEntity);
        try {
            progressDialog.show();
            Rx2AndroidNetworking.post(AppConstant.SITE_URL + "ConfirmPharmacy")
                    .addJSONObjectBody(new JSONObject(json))
                    .addHeaders("Content-Type", "application/json")
                    .build()
                    .getJSONObjectObservable()
                    .subscribeOn(Schedulers.io())
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JSONObject>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(JSONObject response) {

                            if (response != null) {
                                try {
                                    PharmacyDetailsEntity registrationResponse = gson.fromJson(response.toString(), PharmacyDetailsEntity.class);
                                    if (registrationResponse.Status.equalsIgnoreCase("Success")) {
                                        if (progressDialog != null) {
                                            progressDialog.dismiss();
                                        }
                                        dbHelper.AddUpdatePharmacyDetails(registrationResponse.Response);
                                        sharedPreferencesActivity.RegistrationDetailsEntered(true);
                                        Intent intent = new Intent(getApplicationContext(), PickAddressActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (progressDialog != null) {
                                        progressDialog.dismiss();
                                    }
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = e.getMessage();
                        }

                        @Override
                        public void onComplete() {

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
*/
